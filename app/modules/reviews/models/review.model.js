const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const reviewSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  meal_id: {
    type: Schema.Types.ObjectId,
    ref: 'Meal',
    default: null
  },
  title: {
    type: String,
    default: ''
  },
  comment: {
    type: String,
    default: '',
  },
  rating: {
    type: Number,
    default: ''
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  }

});

reviewSchema.plugin(beautifyUnique);
// For pagination
reviewSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Review', reviewSchema);