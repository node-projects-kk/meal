const mongoose = require('mongoose');
const Review = require('reviews/models/review.model');
const perPage = config.PAGINATION_PERPAGE;

const ReviewRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                // and_clauses.push({
                //     'title': Number(req.body.query.generalSearch)
                // });
                and_clauses.push({
                    $or: [{
                        'title': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    }]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Review.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allReview = await Review.aggregatePaginate(aggregate, options);
            return allReview;
        } catch (e) {
            throw (e);
        }
    },

    getZipCount: async (req) => {
        try {

            let reviews = await Review.find({
                isDeleted: false
            });
            return reviews;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let reviews = await Review.findById(id).exec();
            return reviews;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let reviews = await Review.findOne(params).exec();
            return reviews;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let reviews = await Review.find(params).exec();
            return reviews;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let reviews = await Review.findById(id);

            if (reviews) {
                let reviewsDelete = await Review.remove({
                    _id: id
                }).exec();
                return reviewsDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let reviews = await Review.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return reviews;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let reviews = await Review.create(data);
            if (!reviews) {
                return null;
            }
            return reviews;
        } catch (e) {
            throw e;
        }
    },


    getMealsReviewDetail: async (id) => {
        try {
            let reviews = await Review.aggregate([{
                    $match: {
                        'meal_id': mongoose.Types.ObjectId(id)
                    }
                },
                {
                    $lookup: {
                        from: "meals",
                        localField: "meal_id",
                        foreignField: "_id",
                        as: "meal_info"
                    }
                },
                {
                    $unwind: {
                        "path": "$meal_info",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "user_id",
                        foreignField: "_id",
                        as: "user_info"
                    }
                },
                {
                    $unwind: {
                        "path": "$user_info",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        comment: {
                            $first: "$comment"
                        },
                        rating: {
                            $first: "$rating"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                        meal_info: {
                            $first: "$meal_info"
                        },
                        user_info: {
                            $first: "$user_info"
                        },
                    }
                }
            ]);
            return reviews;
        } catch (e) {
            throw (e);
        }
    },
};

module.exports = ReviewRepository;