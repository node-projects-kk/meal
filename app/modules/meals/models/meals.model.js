const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const MealSchema = new Schema({
  title: {
    type: String,
    default: '',
    unique: 'Meal "{VALUE}" is already exist!'
  },
  meal_images: [{
    type: String,
    default: ''
  }],
  meal_type: {
    type: Schema.Types.ObjectId,
    ref: 'MealType'
  },
  topping_id: [{
    type: Schema.Types.ObjectId,
    ref: 'Topping',
    default: null
  }],
  category_id: [{
    type: Schema.Types.ObjectId,
    ref: 'Category',
    default: null
  }],
  like_id: [{
    type: Schema.Types.ObjectId,
    ref: 'Like',
    default: null
  }],
  taste_text: {
    type: String,
    default: ''
  },
  content: {
    type: String,
    default: ''
  },
  key_id: [{
    type: Schema.Types.ObjectId,
    ref: 'KeyBenefit',
    default: null
  }],
  dietary_id: [{
    type: Schema.Types.ObjectId,
    ref: 'Dietary',
    default: null
  }],
  contains_text: [{
    type: String,
    default: ''
  }],
  block_image: {
    type: String,
    default: ''
  },
  detail_blocks: [{
    heading: {
      type: String,
      default: ''
    },
    desc: {
      type: String,
      default: ''
    }
  }],
  price: {
    type: Schema.Types.Double,
    default: 0.00
  },
  slug: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  }
});

MealSchema.plugin(beautifyUnique);
// For pagination
MealSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Meal', MealSchema);