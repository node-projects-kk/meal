const mealRepo = require('meals/repositories/meals.repository');
const mealTypeRepo = require('meal_types/repositories/meal_types.repository');
const toppingRepo = require('toppings/repositories/toppings.repository');
const categoryRepo = require('category/repositories/category.repository');
const keyRepo = require('key_benefits/repositories/key.repository');
const dietaryRepo = require('dietary/repositories/dietary.repository');
const likeRepo = require('like/repositories/like.repository');
const reviewRepo = require('reviews/repositories/review.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');

const gm = require('gm').subClass({
    imageMagick: true
});

var async = require("async");

class MealController {

    /* @Method: create
    // @Description: create meals action
    */
    async create(req, res) {
        try {
            const activeMealType = await mealTypeRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            })
            const toppingDetails = await toppingRepo.getAllByField({
                'status': 'Active',
                "isDeleted": false,
            });
            const categoryDetails = await categoryRepo.getAllByField({
                'status': 'Active',
                "isDeleted": false,
            });
            const keyDetails = await keyRepo.getAllByField({
                'status': 'Active',
                "isDeleted": false,
            });
            const dietaryDetails = await dietaryRepo.getAllByField({
                'status': 'Active',
                "isDeleted": false,
            });
            const likeDetails = await likeRepo.getAllByField({
                'status': 'Active',
                "isDeleted": false,
            });

            res.render('meals/views/add.ejs', {
                page_name: 'meals-management',
                page_title: 'Create New Meal',
                user: req.user,
                mealtypes: activeMealType,
                toppings: toppingDetails,
                categories: categoryDetails,
                keybenefits: keyDetails,
                dietaryinfo: dietaryDetails,
                likeinfo: likeDetails
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save meals action
    */
    async insert(req, res) {
        try {
            var product_image = [];
            req.body.block_image = '';
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('meals.create'));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        req.files.forEach(function (file) {
                            if (file.fieldname == 'meal_images') {
                                product_image.push(file.filename);
                                gm('public/uploads/meal/' + file.filename)
                                    .resize(100)
                                    .write('public/uploads/meal/thumb/' + file.filename, function (err) {
                                        if (err) {
                                            req.flash('error', err.message);
                                            res.redirect(namedRouter.urlFor('meals.create'));
                                        }
                                    });
                            }
                            if (file.fieldname == 'block_image') {
                                gm('public/uploads/meal_block/' + file.filename)
                                    .resize(100)
                                    .write('public/uploads/meal_block/thumb/' + file.filename, function (err) {
                                        if (err) {
                                            req.flash('error', err.message);
                                            res.redirect(namedRouter.urlFor('meals.create'));
                                        }
                                    });
                                req.body.block_image = file.filename;
                            }
                        });
                        req.body.meal_images = product_image;
                    }
                }
                var containText = req.body.contains_text;
                var splittedText = containText.split(",");
                req.body.contains_text = splittedText;

                let mealDetailsArray = [];
                for (let i = 0; i < req.body.heading.length; i++) {
                    mealDetailsArray.push({
                        'heading': req.body.heading[i],
                        'desc': req.body.desc[i]
                    });
                }
                req.body.detail_blocks = mealDetailsArray;
                let newMeals = await mealRepo.save(req.body);
                if (!_.isEmpty(newMeals)) {
                    req.flash('success', 'Meal created succesfully.');
                    res.redirect(namedRouter.urlFor('meals.listing'));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('meals.create'));
                }
            }
        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('meals.create'));
        }
    };


    /*
    // @Method: edit
    // @Description:  meals edit page
    */
    async edit(req, res) {
        try {
            let result = {};
            const mealTypes = await mealTypeRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const getToppings = await toppingRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const categoryDetails = await categoryRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const GetKeyDetails = await keyRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const GetDietaryDetails = await dietaryRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            const GetLikeDetails = await likeRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });

            const meals = await mealRepo.getByToppingId(req.params.id);


            meals[0].topping_details =
                getToppings.map(item => {
                    item['selected'] = false;
                    _.filter(meals[0].topping_details, function (selected_item) {
                        if (selected_item._id.toString() == item._id.toString()) {
                            item['selected'] = true;
                        }
                    })
                    return item;
                })

            meals[0].key_details =
                GetKeyDetails.map(item => {
                    item['selected'] = false;
                    _.filter(meals[0].key_details, function (selected_item) {
                        if (selected_item._id.toString() == item._id.toString()) {
                            item['selected'] = true;
                        }
                    })
                    return item;
                })

            meals[0].dietary_details =
                GetDietaryDetails.map(item => {
                    item['selected'] = false;
                    _.filter(meals[0].dietary_details, function (selected_item) {
                        if (selected_item._id.toString() == item._id.toString()) {
                            item['selected'] = true;
                        }
                    })
                    return item;
                })


            meals[0].like_details =
                GetLikeDetails.map(item => {
                    item['selected'] = false;
                    _.filter(meals[0].like_details, function (selected_item) {
                        if (selected_item._id.toString() == item._id.toString()) {
                            item['selected'] = true;
                        }
                    })
                    return item;
                })

            // console.log(JSON.stringify(meals));

            if (!_.isEmpty(meals)) {
                result.meals_data = meals;
                result.meal_type = mealTypes;
                result.cat_data = categoryDetails;
                res.render('meals/views/edit.ejs', {
                    page_name: 'meals-management',
                    page_title: 'Update Meals',
                    user: req.user,
                    response: result,
                    toppings: getToppings,
                    keybenefits: GetKeyDetails,
                    dietaries: GetDietaryDetails,
                    likes: GetLikeDetails,
                    mealID: req.params.id
                });
            } else {
                req.flash('error', "Sorry meals not found!");
                res.redirect(namedRouter.urlFor('meals.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: meals update action
    */
    async update(req, res) {
        try {
            const MealId = req.body.mid;
            let oldData = await mealRepo.getById(MealId);
            let meal_gallery_image = [];
            meal_gallery_image = oldData.meal_images;

            if (_.has(req, 'files')) {
                if (req.files.length > 0) {
                    req.files.forEach(function (file) {
                        if (file.fieldname == 'meal_images') {
                            meal_gallery_image.push(file.filename);
                            gm('public/uploads/meal/' + file.filename)
                                .resize(100)
                                .write('public/uploads/meal/thumb/' + file.filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('meals.create'));
                                    }
                                });
                        }
                        if (file.fieldname == 'block_image') {
                            if (!_.isEmpty(oldData.block_image)) {
                                if (fs.existsSync('public/uploads/meal/' + oldData.block_image)) {
                                    const upl_img = fs.unlinkSync('public/uploads/meal/' + oldData.block_image);
                                }
                                if (fs.existsSync('public/uploads/meal/thumb/' + oldData.block_image)) {
                                    const upl_thumb_img = fs.unlinkSync('public/uploads/meal/thumb/' + oldData.block_image);
                                }
                            }
                            gm('public/uploads/meal_block/' + file.filename)
                                .resize(100)
                                .write('public/uploads/meal_block/thumb/' + file.filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('meals.create'));
                                    }
                                });
                            req.body.block_image = file.filename;
                        }
                    });
                    req.body.meal_images = meal_gallery_image;
                }
            }

            var newContainText = req.body.contains_text;
            var splittedText = newContainText.split(",");
            req.body.contains_text = splittedText;

            let mealDetailsArray = [];
            for (let i = 0; i < req.body.heading.length; i++) {
                mealDetailsArray.push({
                    'heading': req.body.heading[i],
                    'desc': req.body.desc[i]
                });
            }
            req.body.detail_blocks = mealDetailsArray;

            let MealUpdate = await mealRepo.updateById(req.body, MealId);

            if (MealUpdate) {
                req.flash('success', "Meal Updated Successfully");
                res.redirect(namedRouter.urlFor('meals.listing'));
            } else {
                res.redirect(namedRouter.urlFor('meals.edit', {
                    id: MealId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('meals.edit', {
                id: req.body.mid
            }));
        }

    };


    /* @Method: list
    // @Description: To list all the meals from DB
    */
    async list(req, res) {
        try {

            res.render('meals/views/list.ejs', {
                page_name: 'meals-management',
                page_title: 'Meal Lists',
                user: req.user,
                // mealTypes: ActiveMealTypes
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the meals from DB
    */
    async getAll(req, res) {
        try {
            const mealType = await mealRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": mealType.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": mealType.totalCount,
                "sort": sortOrder,
                "field": sortField
            };

            return {
                status: 200,
                meta: meta,
                data: mealType.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: meals delete
    */
    async delete(req, res) {
        try {
            let mealData = await mealRepo.getByField({
                '_id': req.params.id
            });
            let allMealImages = [];
            allMealImages = mealData.meal_images;
            let mealDelete = await mealRepo.delete(req.params.id)
            if (!_.isEmpty(mealDelete)) {
                if (!_.isEmpty(allMealImages)) {
                    for (let i = 0; i < allMealImages.length; i++) {
                        if (fs.existsSync('public/uploads/meal/' + allMealImages[i])) {
                            const upl_img = fs.unlinkSync('public/uploads/meal/' + allMealImages[i]);
                        }
                        if (fs.existsSync('public/uploads/meal/thumb/' + allMealImages[i])) {
                            const upl_thumb_img = fs.unlinkSync('public/uploads/meal/thumb/' + allMealImages[i]);
                        }
                    }
                }
                if (!_.isEmpty(mealData.block_image)) {
                    if (fs.existsSync('public/uploads/meal_block/' + mealData.block_image)) {
                        const upl_img = fs.unlinkSync('public/uploads/meal_block/' + mealData.block_image);
                    }
                    if (fs.existsSync('public/uploads/meal_block/thumb/' + mealData.block_image)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/meal_block/thumb/' + mealData.block_image);
                    }
                }
                req.flash('success', 'Meal Removed Successfully');
                res.redirect(namedRouter.urlFor('meals.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /*
    // @Method: deleteMealImages
    // @Description: Delete meal image action
    */
    async deleteMealImages(req, res) {
        try {
            const result = await mealRepo.getById(req.body.id);
            // let imageName = result.meal_images.indexOf(req.body.image);
            // result.meal_images.splice(imageName, 1);
            const deleteImg = await mealRepo.mealSingleImageDelete(req.body.id, req.body.image)
            // const deleteImg = await mealRepo.updateById({
            //     'meal_images': result.meal_images
            // }, req.body.id);
            if (!_.isEmpty(deleteImg)) {
                if (!_.isEmpty(req.body.image)) {
                    if (fs.existsSync('public/uploads/meal/' + req.body.image)) {
                        const upl_img = fs.unlinkSync('public/uploads/meal/' + req.body.image);
                    }
                    if (fs.existsSync('public/uploads/meal/thumb/' + req.body.image)) {
                        const upl_thumb_img = fs.unlinkSync('public/uploads/meal/thumb/' + req.body.image);
                    }
                }
            }
            return {
                status: 200,
            }
        } catch (error) {
            throw error;
        }
    };


    /* @Method: MealReviewPage
    // @Description: meals review page
    */
    async MealReviewPage(req, res) {
        try {
            res.render('reviews/views/list.ejs', {
                page_name: 'review-management',
                page_title: 'View Review',
                user: req.user,
                mealid: req.params.id,
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: MealReviewData
    // @Description: meals review data
    */
    async MealReviewData(req, res) {
        try {
            const mealReviews = await reviewRepo.getMealsReviewDetail(req.params.id);
            return {
                status: 200,
                data: mealReviews,
                message: 'Meal reviews fetched successfully.'
            }
        } catch (e) {
            throw (e);
        }
    };


}

module.exports = new MealController();