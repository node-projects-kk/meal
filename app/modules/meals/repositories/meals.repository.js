const mongoose = require('mongoose');
const Meals = require('meals/models/meals.model');
const Review = require('reviews/models/review.model');
const Toppings = require('toppings/models/toppings.model');
const perPage = config.PAGINATION_PERPAGE;

const MealsRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'price': parseFloat(req.body.query.generalSearch)
                        },
                        {
                            'meal_type_details.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'topping_info.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Meals.aggregate([{
                    "$lookup": {
                        "from": "mealtypes",
                        "localField": "meal_type",
                        "foreignField": "_id",
                        "as": "meal_type_details"
                    },
                },
                {
                    $unwind: {
                        path: "$meal_type_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                // {
                //     $unwind: '$topping_id'
                // },
                {
                    "$lookup": {
                        "from": "toppings",
                        "localField": "topping_id",
                        "foreignField": "_id",
                        "as": "topping_info"
                    },
                },
                {
                    $unwind: {
                        path: "$topping_info",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: conditions
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        price: {
                            $first: "$price"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                        meal_type: {
                            $first: "$meal_type"
                        },
                        meal_type_details: {
                            $first: "$meal_type_details"
                        },
                        status: {
                            $first: "$status"
                        },
                        topping_info: {
                            $push: "$topping_info"
                        },
                    }
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allMeals = await Meals.aggregatePaginate(aggregate, options);
            return allMeals;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let meals = await Meals.findById(id).exec();
            return meals;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let meals = await Meals.findOne(params).exec();
            return meals;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let meals = await Meals.find(params).exec();
            return meals;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let meals = await Meals.findById(id);
            if (meals) {
                let mealDelete = await Meals.remove({
                    _id: id
                }).exec();
                return mealDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let meal = await Meals.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return meal;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let meal = await Meals.create(data);
            if (!meal) {
                return null;
            }
            return meal;
        } catch (e) {
            throw e;
        }
    },

    getMealsCountOnTopping: async (id) => {
        try {
            let mealCountOfTopping = await Toppings.find({
                'meal': id
            }).count();
            return mealCountOfTopping;
        } catch (e) {
            throw (e);
        }
    },

    getByToppingId: async (id) => {
        try {
            return await Meals.aggregate([
                // {
                //     $unwind: "$topping_id"
                // },
                {
                    "$lookup": {
                        "from": "toppings",
                        "localField": "topping_id",
                        "foreignField": "_id",
                        "as": "topping_details"
                    }
                },
                {
                    $unwind: {
                        path: "$topping_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "keybenefits",
                        "localField": "key_id",
                        "foreignField": "_id",
                        "as": "key_details"
                    }
                },
                {
                    $unwind: {
                        path: "$key_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "dietaries",
                        "localField": "dietary_id",
                        "foreignField": "_id",
                        "as": "dietary_details"
                    }
                },
                {
                    $unwind: {
                        path: "$dietary_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$lookup": {
                        "from": "likes",
                        "localField": "like_id",
                        "foreignField": "_id",
                        "as": "like_details"
                    }
                },
                {
                    $unwind: {
                        path: "$like_details",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        "_id": mongoose.Types.ObjectId(id)
                    }
                },
                {
                    $group: {
                        _id: null,
                        meal_id: {
                            $first: "$_id"
                        },
                        title: {
                            $first: "$title"
                        },
                        meal_images: {
                            $first: "$meal_images"
                        },
                        topping_id: {
                            $addToSet: "$topping_details._id"
                        },
                        topping_details: {
                            $addToSet: "$topping_details"
                        },
                        category: {
                            $first: "$category_id"
                        },
                        like_id: {
                            $addToSet: "$like_details._id"
                        },
                        like_details: {
                            $addToSet: "$like_details"
                        },
                        key_id: {
                            $addToSet: "$key_details._id"
                        },
                        key_details: {
                            $addToSet: "$key_details"
                        },
                        dietary_id: {
                            $addToSet: "$dietary_details._id"
                        },
                        dietary_details: {
                            $addToSet: "$dietary_details"
                        },
                        taste_text: {
                            $first: "$taste_text"
                        },
                        content: {
                            $first: "$content"
                        },
                        contains_text: {
                            $first: "$contains_text"
                        },
                        block_image: {
                            $first: "$block_image"
                        },
                        mealtype: {
                            $first: "$meal_type"
                        },
                        detail_blocks: {
                            $first: "$detail_blocks"
                        },
                        slug: {
                            $first: "$slug"
                        },
                        price: {
                            $first: "$price"
                        },
                        status: {
                            $first: "$status"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                    }
                }
            ]).exec();
        } catch (error) {
            throw error;
        }
    },

    getMealCount: async (req) => {
        try {
            let mealCount = await Meals.find({
                isDeleted: false
            });
            return mealCount;
        } catch (e) {
            throw (e);
        }
    },

    getProductAll: async (_searchQuery) => {
        try {

            let query = [{
                "isDeleted": false,
                "status": "Active"
            }];

            if (_.has(_searchQuery, "category")) {
                if (_searchQuery.category != '') {
                    query.push({
                        'category_id': mongoose.Types.ObjectId(_searchQuery.category)
                    });
                }
            }

            if (_.has(_searchQuery, "like")) {

                if (_searchQuery.like != '') {
                    query.push({
                        'like_id': {
                            $in: _searchQuery.like.map(mongoose.Types.ObjectId)
                        }
                    });
                }
            }

            if (_.has(_searchQuery, "dislike")) {
                if (_searchQuery.dislike != '') {
                    query.push({
                        'like_id': {
                            $nin: _searchQuery.dislike.map(mongoose.Types.ObjectId)
                        }
                    });
                }
            }

            if (_.has(_searchQuery, "benefit")) {
                if (_searchQuery.benefit != '') {
                    query.push({
                        'key_id': {
                            $in: _searchQuery.benefit.map(mongoose.Types.ObjectId)
                        }
                    });
                }
            }

            if (_.has(_searchQuery, "dietary")) {
                if (_searchQuery.dietary != '') {
                    query.push({
                        'dietary_id': {
                            $in: _searchQuery.dietary.map(mongoose.Types.ObjectId)
                        }
                    });
                }
            }

            const searchQuery = {
                "$and": query
            };

            let meals = await Meals.aggregate([{
                    $lookup: {
                        from: "mealtypes",
                        localField: "meal_type",
                        foreignField: "_id",
                        as: "mealtype_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$mealtype_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "toppings",
                        localField: "topping_id",
                        foreignField: "_id",
                        as: "topping_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$topping_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "categories",
                        localField: "category_id",
                        foreignField: "_id",
                        as: "category_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$category_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "keybenefits",
                        localField: "key_id",
                        foreignField: "_id",
                        as: "key_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$key_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "likes",
                        localField: "like_id",
                        foreignField: "_id",
                        as: "like_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$like_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "dietaries",
                        localField: "dietary_id",
                        foreignField: "_id",
                        as: "dietary_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$dietary_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "reviews",
                        localField: "_id",
                        foreignField: "meal_id",
                        as: "review_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$review_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $match: searchQuery
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        meal_images: {
                            $first: "$meal_images"
                        },
                        content: {
                            $first: "$content"
                        },
                        contains_text: {
                            $first: "$contains_text"
                        },
                        detail_blocks: {
                            $first: "$detail_blocks"
                        },
                        price: {
                            $first: "$price"
                        },
                        slug: {
                            $first: "$slug"
                        },
                        status: {
                            $first: "$status"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                        mealtype_details: {
                            $first: "$mealtype_details"
                        },
                        topping_details: {
                            $addToSet: "$topping_details"
                        },
                        category_details: {
                            $first: "$category_details"
                        },
                        key_details: {
                            $addToSet: "$key_details"
                        },
                        like_details: {
                            $addToSet: "$like_details"
                        },
                        dietary_details: {
                            $addToSet: "$dietary_details"
                        },
                        review_details: {
                            $addToSet: "$review_details"
                        },
                    }
                }
            ]);
            return meals;
        } catch (e) {
            throw (e);
        }
    },

    getProductDetail: async (params) => {
        try {
            //console.log("425>>",params);
            let meals = await Meals.aggregate([{
                    $match: params
                },
                {
                    $lookup: {
                        from: "mealtypes",
                        localField: "meal_type",
                        foreignField: "_id",
                        as: "mealtype_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$mealtype_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "toppings",
                        localField: "topping_id",
                        foreignField: "_id",
                        as: "topping_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$topping_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "categories",
                        localField: "category_id",
                        foreignField: "_id",
                        as: "category_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$category_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "keybenefits",
                        localField: "key_id",
                        foreignField: "_id",
                        as: "key_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$key_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "likes",
                        localField: "like_id",
                        foreignField: "_id",
                        as: "like_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$like_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "reviews",
                        localField: "_id",
                        foreignField: "meal_id",
                        as: "review_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$review_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "review_details.user_id",
                        foreignField: "_id",
                        as: "review_details.user_id"
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        meal_images: {
                            $first: "$meal_images"
                        },
                        content: {
                            $first: "$content"
                        },
                        contains_text: {
                            $first: "$contains_text"
                        },
                        block_image: {
                            $first: "$block_image"
                        },
                        taste_text: {
                            $first: "$taste_text"
                        },
                        detail_blocks: {
                            $first: "$detail_blocks"
                        },
                        price: {
                            $first: "$price"
                        },
                        slug: {
                            $first: "$slug"
                        },
                        status: {
                            $first: "$status"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                        mealtype_details: {
                            $first: "$mealtype_details"
                        },
                        topping_details: {
                            $addToSet: "$topping_details"
                        },
                        category_details: {
                            $first: "$category_details"
                        },
                        key_details: {
                            $addToSet: "$key_details"
                        },
                        like_details: {
                            $addToSet: "$like_details"
                        },
                        review_details: {
                            $addToSet: "$review_details"
                        }
                    }
                }
            ]);
            return meals;
        } catch (e) {
            console.log('repo<><>', e);
            throw (e);
        }
    },

    // getProductReviewDetail: async (id) => {
    //     try {
    //         //console.log("425>>",params);
    //         let review = await Review.aggregate([{
    //                 $match: mongoose.Types.ObjectId(id)
    //             },
    //             {
    //                 $lookup: {
    //                     from: "users",
    //                     localField: "user_id",
    //                     foreignField: "_id",
    //                     as: "user_details"
    //                 }
    //             },
    //             {
    //                 $unwind: {
    //                     "path": "$user_details",
    //                     "preserveNullAndEmptyArrays": true
    //                 }
    //             },
    //         ]);
    //         return review;
    //     } catch (e) {
    //         throw (e);
    //     }
    // },

    mealSingleImageDelete: async (id, filename) => {
        try {
            return await Meals.update({
                _id: id
            }, {
                '$pull': {
                    "meal_images": filename
                }
            }).lean().exec();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    getMealDetailsOnMealType: async (id) => {
        try {
            let meals = await Meals.aggregate([

                {
                    $lookup: {
                        from: "mealtypes",
                        localField: "meal_type",
                        foreignField: "_id",
                        as: "mealtype_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$mealtype_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "toppings",
                        localField: "topping_id",
                        foreignField: "_id",
                        as: "topping_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$topping_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "categories",
                        localField: "category_id",
                        foreignField: "_id",
                        as: "category_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$category_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "likes",
                        localField: "like_id",
                        foreignField: "_id",
                        as: "like_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$like_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "keybenefits",
                        localField: "key_id",
                        foreignField: "_id",
                        as: "key_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$key_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "dietaries",
                        localField: "dietary_id",
                        foreignField: "_id",
                        as: "dietary_details"
                    }
                },
                {
                    $unwind: {
                        "path": "$dietary_details",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $match: {
                        'meal_type': mongoose.Types.ObjectId(id),
                        'status': 'Active',
                        'isDeleted': false
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        title: {
                            $first: "$title"
                        },
                        meal_images: {
                            $first: "$meal_images"
                        },
                        content: {
                            $first: "$content"
                        },
                        contains_text: {
                            $first: "$contains_text"
                        },
                        detail_blocks: {
                            $first: "$detail_blocks"
                        },
                        price: {
                            $first: "$price"
                        },
                        taste_text: {
                            $first: "$taste_text"
                        },
                        block_image: {
                            $first: "$block_image"
                        },
                        slug: {
                            $first: "$slug"
                        },
                        status: {
                            $first: "$status"
                        },
                        isDeleted: {
                            $first: "$isDeleted"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                        mealtype_details: {
                            $first: "$mealtype_details"
                        },
                        topping_details: {
                            $addToSet: "$topping_details"
                        },
                        category_details: {
                            $first: "$category_details"
                        },
                        like_details: {
                            $addToSet: "$like_details"
                        },
                        key_details: {
                            $addToSet: "$key_details"
                        },
                        dietary_details: {
                            $addToSet: "$dietary_details"
                        },

                    }
                }
            ]);

            return meals;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = MealsRepository;