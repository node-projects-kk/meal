const mealTypeRepo = require('meal_types/repositories/meal_types.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class MealTypeController {

    /* @Method: create
    // @Description: create meal type action
    */
    async create(req, res) {
        try {
            res.render('meal_types/views/add.ejs', {
                page_name: 'meal-type-management',
                page_title: 'Create New Meal Type',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save meal type action
    */
    async insert(req, res) {
        try {
            let newMealType = await mealTypeRepo.save(req.body);
            req.flash('success', 'Meal Type created succesfully.');
            res.redirect(namedRouter.urlFor('mealtypes.listing'));
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('mealtypes.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  meal type edit page
    */
    async edit(req, res) {
        try {
            let result = {};
            let mealType = await mealTypeRepo.getById(req.params.id);
            if (!_.isEmpty(mealType)) {
                result.mealtype_data = mealType;
                res.render('meal_types/views/edit.ejs', {
                    page_name: 'meal-type-management',
                    page_title: 'Update Meal Type',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry meal type not found!");
                res.redirect(namedRouter.urlFor('mealtypes.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: meal type update action
    */
    async update(req, res) {
        try {
            const MealTypeId = req.body.mtid;
            let MealTypeUpdate = await mealTypeRepo.updateById(req.body, MealTypeId);
            if (MealTypeUpdate) {
                req.flash('success', "Meal Type Updated Successfully");
                res.redirect(namedRouter.urlFor('mealtypes.listing'));
            } else {
                res.redirect(namedRouter.urlFor('mealtypes.edit', {
                    id: MealTypeId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('mealtypes.edit', {
                id: req.body.mtid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the meal type from DB
    */
    async list(req, res) {
        try {
            res.render('meal_types/views/list.ejs', {
                page_name: 'meal-type-management',
                page_title: 'Meal Type Lists',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the meal type from DB
    */
    async getAll(req, res) {
        try {
            let mealType = await mealTypeRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": mealType.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": mealType.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: mealType.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: meal type delete
    */
    async delete(req, res) {
        try {
            const mealExists = await mealTypeRepo.getMealsCountOnMealType(req.params.id);
            if (mealExists > 0) {
                req.flash('error', 'You can\'t delete this Meal Type, Beacuse this Meal Type contain many Meals.');
                res.redirect(namedRouter.urlFor('mealtypes.listing'));
            } else {
                let MealTypeDelete = await mealTypeRepo.updateById({
                    "isDeleted": true
                }, req.params.id);
                req.flash('success', 'Meal Type Removed Successfully');
                res.redirect(namedRouter.urlFor('mealtypes.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

}

module.exports = new MealTypeController();