const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const MealTypeSchema = new Schema({
  title: {
    type: String,
    default: '',
    unique: 'Meal Type "{VALUE}" is already exist!'
  },
  slug: {
    type: String,
    default: ''
  },
  sort_order: {
    type: Number,
    default: 0
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  }
});

MealTypeSchema.plugin(beautifyUnique);
// For pagination
MealTypeSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('MealType', MealTypeSchema);