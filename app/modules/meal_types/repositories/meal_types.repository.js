const mongoose = require('mongoose');
const MealTypes = require('meal_types/models/meal_types.model');
const Meals = require('meals/models/meals.model');
const perPage = config.PAGINATION_PERPAGE;

const MealTypesRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = MealTypes.aggregate([{
                $match: conditions
            },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allMealTypes = await MealTypes.aggregatePaginate(aggregate, options);
            return allMealTypes;
        } catch (e) {
            throw (e);
        }
    },

    getMealTypeCount: async (req) => {
        try {

            let mealtypes = await MealTypes.find({
                isDeleted: false
            });
            return mealtypes;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let mealtypes = await MealTypes.findById(id).exec();
            return mealtypes;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let mealtypes = await MealTypes.findOne(params).exec();
            return mealtypes;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let mealtypes = await MealTypes.find(params).sort({ sort_order: 1 }).exec();
            return mealtypes;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let mealtypes = await MealTypes.findById(id);

            if (mealtypes) {
                let mealtypesDelete = await MealTypes.remove({
                    _id: id
                }).exec();
                return mealtypesDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let mealtypes = await MealTypes.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return mealtypes;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let mealtypes = await MealTypes.create(data);
            if (!mealtypes) {
                return null;
            }
            return mealtypes;
        } catch (e) {
            throw e;
        }
    },

    getMealsCountOnMealType: async (id) => {
        try {
            let mealCountOfMealTypes = await Meals.find({
                'meal_type': id
            }).count();
            return mealCountOfMealTypes;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = MealTypesRepository;