const mongoose = require('mongoose');
const Dietary = require('dietary/models/dietary.model');
const Meals = require('meals/models/meals.model');
const perPage = config.PAGINATION_PERPAGE;

const dietaryRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} });
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Dietary.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allDietary = await Dietary.aggregatePaginate(aggregate, options);
            return allDietary;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let dietary = await Dietary.findById(id).exec();
        try {
            if (!dietary) {
                return null;
            }
            return dietary;
        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let dietary = await Dietary.findOne(params).exec();
        try {
            if (!dietary) {
                return null;
            }
            return dietary;
        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let dietary = await Dietary.find(params).exec();
        try {
            if (!dietary) {
                return null;
            }
            return dietary;
        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let dietary = await Dietary.findById(id);
            if (dietary) {
                let dietaryDelete = await Dietary.remove({
                    _id: id
                }).exec();
                if (!dietaryDelete) {
                    return null;
                }
                return dietaryDelete;
            }
        } catch (e) {
            return e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },

    updateById: async (data, id) => {
        try {
            let dietary = await Dietary.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!dietary) {
                return null;
            }
            return dietary;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let dietary = await Dietary.create(data);
            if (!dietary) {
                return null;
            }
            return dietary;
        } catch (e) {
            throw e;
        }
    },

    getDietaryCountOnMeals: async (id) => {
        try {
            let dietaryCountOfMeals = await Meals.find({
                'dietary_id': id
            }).count();
            return dietaryCountOfMeals;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = dietaryRepository;