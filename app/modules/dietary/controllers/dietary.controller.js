const mongoose = require('mongoose');
const dietaryRepo = require('dietary/repositories/dietary.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class dietaryController {

    async create(req, res) {
        try {
            res.render('dietary/views/add.ejs', {
                page_name: 'dietary-management',
                page_title: 'Create New Dietary',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
            req.body.dietary_icon = '';
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('dietary.create'));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        if (req.files[0].fieldname == 'dietary_icon') {
                            gm('public/uploads/dietary/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/dietary/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('dietary.create'));
                                    }
                                });
                            req.body.dietary_icon = req.files[0].filename;
                        }
                    }
                }
                let newdietary = await dietaryRepo.save(req.body);
                if (!_.isEmpty(newdietary)) {
                    req.flash('success', 'Dietary created succesfully.');
                    res.redirect(namedRouter.urlFor('dietary.listing'));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('dietary.create'));
                }
            }
        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('dietary.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  dietary edit page
    */
    async edit(req, res) {
        try {
            let dietary = await dietaryRepo.getById(req.params.id);
            if (!_.isEmpty(dietary)) {
                res.render('dietary/views/edit.ejs', {
                    page_name: 'dietary-management',
                    page_title: 'Update Dietary',
                    user: req.user,
                    response: dietary
                });
            } else {
                req.flash('error', "Sorry Dietary not found!");
                res.redirect(namedRouter.urlFor('dietary.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: dietary update action
    */
    async update(req, res) {
        try {
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('dietary.edit', {
                    id: req.body.did
                }));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        let dietary = await dietaryRepo.getByField({
                            '_id': req.body.did
                        });
                        if (!_.isEmpty(dietary.dietary_icon)) {
                            if (fs.existsSync('public/uploads/dietary/' + dietary.dietary_icon)) {
                                const upl_img = fs.unlinkSync('public/uploads/dietary/' + dietary.dietary_icon);
                            }
                            if (fs.existsSync('public/uploads/dietary/thumb/' + dietary.dietary_icon)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/dietary/thumb/' + dietary.dietary_icon);
                            }
                        }
                        if (req.files[0].fieldname == 'dietary_icon') {
                            gm('public/uploads/dietary/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/dietary/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('dietary.edit', {
                                            id: req.body.did
                                        }));
                                    }
                                });
                            req.body.dietary_icon = req.files[0].filename;
                        }
                    }
                }
                let dietaryUpdate = await dietaryRepo.updateById(req.body, req.body.did);
                if (dietaryUpdate) {
                    req.flash('success', 'Dietary updated succesfully.');
                    res.redirect(namedRouter.urlFor('dietary.listing'));
                } else {
                    res.redirect(namedRouter.urlFor('dietary.edit', {
                        id: req.body.did
                    }));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the dietary from DB
    */
    async list(req, res) {
        try {
            res.render('dietary/views/list.ejs', {
                page_name: 'dietary-management',
                page_title: 'Dietary List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAll(req, res) {
        try {
            let dietaryData = await dietaryRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": dietaryData.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": dietaryData.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: dietaryData.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: key Delete
    */
    async delete(req, res) {
        try {
            let dietary = await dietaryRepo.getByField({
                '_id': req.params.id
            });
            const dietaryExists = await dietaryRepo.getDietaryCountOnMeals(req.params.id);
            if (dietaryExists > 0) {
                req.flash('error', 'You can\'t delete this Dietary, Beacuse this Dietary added in Meals.');
                res.redirect(namedRouter.urlFor('dietary.listing'));
            } else {
                let dietaryDelete = await dietaryRepo.delete(req.params.id)
                if (!_.isEmpty(dietaryDelete)) {
                    if (!_.isEmpty(dietary.dietary_icon)) {
                        if (fs.existsSync('public/uploads/dietary/' + dietary.dietary_icon)) {
                            const upl_img = fs.unlinkSync('public/uploads/dietary/' + dietary.dietary_icon);
                        }
                        if (fs.existsSync('public/uploads/dietary/thumb/' + dietary.dietary_icon)) {
                            const upl_thumb_img = fs.unlinkSync('public/uploads/dietary/thumb/' + dietary.dietary_icon);
                        }
                    }
                    req.flash('success', 'Dietary Removed Successfully');
                    res.redirect(namedRouter.urlFor('dietary.listing'));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


}

module.exports = new dietaryController();