const coupon = require('coupon/repositories/coupon.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');
const mongoose = require('mongoose');

class CouponController {
    async create(req, res) {
        try {
            res.render('coupon/views/add.ejs', {
                page_name: 'coupon-management',
                page_title: 'Create New Coupon',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
                let couponSave = await coupon.save(req.body);
                if (!_.isEmpty(couponSave)) {
                    req.flash('success', 'Coupon created succesfully.');
                    res.redirect(namedRouter.urlFor('coupon.listing'));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('coupon.create'));
                }
        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('coupon.create'));
        }
    };
  /*
    // @Method: status_change
    // @Description: coupon status change action
    */
   async statusChange (req, res){
    try {
        let coupons = await coupon.getById(req.params.id);
        if(!_.isEmpty(coupons)){
            let couponStatus = (coupons.status == "Active") ? "Inactive" : "Active";
            let couponUpdate = await coupon.updateById({ 'status': couponStatus }, req.params.id);
            req.flash('success', "Coupon status has changed successfully" );
            res.redirect(namedRouter.urlFor('coupon.listing'));
        } else {
            req.flash('error', "sorry coupon not found");
            res.redirect(namedRouter.urlFor('coupon.listing')); 
        }
    } catch(e){
        console.log(e);
        return res.status(500).send({message: e.message}); 
    }
};

    /*
    // @Method: view
    // @Description:  orders edit page
    */
    async view(req, res) {
        try {
                res.render('coupons/views/view.ejs', {
                    page_name: 'coupon-management',
                    page_title: 'View Coupons',
                    user: req.user,
                });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
   async edit(req, res) {
    try {
        let result = await coupon.getById(req.params.id);
        if (!_.isEmpty(result)) {
            res.render('coupon/views/edit.ejs', {
                page_name: 'coupon-edit',
                page_title: 'Update coupon',
                user: req.user,
                response: result
            });
        } else {
            req.flash('error', "Sorry coupon not found!");
            res.redirect(namedRouter.urlFor('coupon.listing'));
        }
    } catch (e) {
        return res.status(500).send({ message: e.message });
    }
};

    /* @Method: update
    // @Description: orders update action
    */
    async update(req, res) {
        try {
            let couponUpdate = await coupon.updateById(req.body, req.body.id);
            if (couponUpdate) {
                req.flash('success', "Coupon Updated Successfully");
                res.redirect(namedRouter.urlFor('coupon.listing'));
            } else {
                res.redirect(namedRouter.urlFor('coupon.edit', {
                    id: req.body.id
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('coupon.edit', {
                id: req.body.id
            }));
        }
    };

    /* @Method: list
    // @Description: To list all the orders from DB
    */
    async list(req, res) {
        try {

            res.render('coupon/views/list.ejs', {
                page_name: 'coupon-management',
                page_title: 'Coupon List',
                user: req.user,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async destroy(req, res) {
        try {
            let Delete = await coupon.delete(mongoose.Types.ObjectId(req.params.id))
            if (!_.isEmpty(Delete)) {
                req.flash('success', 'Coupon Removed Successfully');
                res.redirect(namedRouter.urlFor('coupon.listing'));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };
    /* @Method: getAll
    // @Description: To get all the orders from DB
    */
    async getAll(req, res) {
        try {
            let coupons = await coupon.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": coupons.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": coupons.totalCount,
                "sort": sortOrder,
                "field": sortField
            };

            return {
                status: 200,
                meta: meta,
                data: coupons.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    };

    async verifyCoupon(req, res) {
        try {
            let coupons = await coupon.getByField({"coupon_code":req.body.code});

            if(!coupons)
        {
            return {
                "status": 200,
            };
        }
        else {
            return {
                "status": 200,
                "message": "Coupon already exists"
            };
        }
        } catch (e) {
            throw e;
        }
    };


}

module.exports = new CouponController();