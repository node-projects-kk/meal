const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["Active", "Inactive"];
const deleted = [true, false];
const dtype = ["flat", "percent"];

const CouponSchema = new Schema({
    coupon_code: { type: String, default: '' },
    coupon_description: { type: String, default: '' },
    expiry_date: { type: String, default: ''},
    amount: { type: Schema.Types.Double, default: 0.00 },
    discount_type: {  type: String, default: 'flat', enum: dtype },
    coupon_count: { type: Number, default: '' },
    status: { type: String, default: 'Active', enum: status},
    isDeleted: { type: Boolean, default: false, enum: deleted },
    createdAt: { type: Date, default: Date.now() }
});

// For pagination
CouponSchema.plugin(mongooseAggregatePaginate);

module.exports = mongoose.model('Coupon', CouponSchema);