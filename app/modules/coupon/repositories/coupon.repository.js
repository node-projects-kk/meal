const Coupon = require('coupon/models/coupon.model');
const perPage = config.PAGINATION_PERPAGE;

class CouponRepository {
    constructor() { }

  async getAll(req) {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                // "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'coupon_code': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'amount': parseFloat(req.body.query.generalSearch)
                        },
                        {
                            'coupon_description': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Coupon.aggregate([
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let coupons = await Coupon.aggregatePaginate(aggregate, options);
            return coupons;
        } catch (e) {
            throw (e);
        }
    }

    async getById(id) {
        try {
            return await Coupon.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = Object.assign(params, {
                "isDeleted": false,
                "status": "Active",
            })
            return await Coupon.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }


    async getAllByField(params) {
        try {
            const _params = Object.assign(params, {
                "isDeleted": false,
            });
            return await Coupon.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getCount(params) {
        try {
            return await Coupon.count(params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            
                let productDelete = await Coupon.remove({_id:id}).exec();
                if (!productDelete) {
                    return null;
                }
                return productDelete;
        } catch (e) {
            throw e;
        }
    }

    async updateById(data, id) {
        try {
            return await Coupon.findByIdAndUpdate(id, data).exec();
        } catch (error) {
            return error;
        }
    }

    async updateByField(field, fieldValue, data) {
        //todo: update by field
    }

    async save(obj) {
        try {
            const new_coupon = new Coupon(obj);
            return await new_coupon.save();
        } catch (error) {
            return error;
        }
    }

/* 
    async getByField(params) {
        try {
            const _params = Object.assign(params, {
                "isDeleted": false,
                "status": "Active",
            })
            return await Coupon.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    } */

};

module.exports = new CouponRepository();