const mealRepo = require('meals/repositories/meals.repository');
const toppingRepo = require('toppings/repositories/toppings.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class ToppingController {

    /* @Method: create
    // @Description: create meals action
    */
    async create(req, res) {
        try {
            const activeMeals = await mealRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            })
            res.render('toppings/views/add.ejs', {
                page_name: 'toppings-management',
                page_title: 'Create New Topping',
                user: req.user,
                meals: activeMeals
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save meals action
    */
    async insert(req, res) {
        try {
            let newToppings = await toppingRepo.save(req.body);
            req.flash('success', 'Topping created succesfully.');
            res.redirect(namedRouter.urlFor('toppings.listing'));
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('toppings.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  meals edit page
    */
    async edit(req, res) {
        try {
            let result = {};
            const meals = await mealRepo.getAllByField({
                "isDeleted": false,
                "status": "Active"
            });
            let toppings = await toppingRepo.getById(req.params.id);
            if (!_.isEmpty(toppings)) {
                result.topping_data = toppings;
                result.meal_data = meals;

                res.render('toppings/views/edit.ejs', {
                    page_name: 'toppings-management',
                    page_title: 'Update Topping',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry topping not found!");
                res.redirect(namedRouter.urlFor('toppings.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: meals update action
    */
    async update(req, res) {
        try {
            const toppingId = req.body.tid;
            let toppingUpdate = await toppingRepo.updateById(req.body, toppingId);
            if (toppingUpdate) {
                req.flash('success', "Topping Updated Successfully");
                res.redirect(namedRouter.urlFor('toppings.listing'));
            } else {
                res.redirect(namedRouter.urlFor('toppings.edit', {
                    id: toppingId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('toppings.edit', {
                id: req.body.tid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the meals from DB
    */
    async list(req, res) {
        try {

            res.render('toppings/views/list.ejs', {
                page_name: 'toppings-management',
                page_title: 'Toppings List',
                user: req.user,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the meals from DB
    */
    async getAll(req, res) {
        try {
            let toppings = await toppingRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": toppings.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": toppings.totalCount,
                "sort": sortOrder,
                "field": sortField
            };

            return {
                status: 200,
                meta: meta,
                data: toppings.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: meals delete
    */
    async delete(req, res) {
        try {
            const toppingExists = await toppingRepo.getToppingCountOnMeals(req.params.id);
            if (toppingExists > 0) {
                req.flash('error', 'You can\'t delete this Topping, Beacuse this Topping contain Meals.');
                res.redirect(namedRouter.urlFor('toppings.listing'));
            } else {
                let toppingDelete = await toppingRepo.updateById({
                    "isDeleted": true
                }, req.params.id)
                req.flash('success', 'Topping Removed Successfully');
                res.redirect(namedRouter.urlFor('toppings.listing'));
            }

        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


}

module.exports = new ToppingController();