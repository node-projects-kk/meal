const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const ToppingSchema = new Schema({
  title: {
    type: String,
    default: '',
    unique: 'Topping "{VALUE}" is already exist!'
  },
  meal: {
    type: Schema.Types.ObjectId,
    ref: 'Meal'
  },
  price: {
    type: Schema.Types.Double,
    default: 0.00
  },
  slug: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  }
});

ToppingSchema.plugin(beautifyUnique);
// For pagination
ToppingSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Topping', ToppingSchema);