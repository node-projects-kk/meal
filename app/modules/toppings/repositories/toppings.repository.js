const mongoose = require('mongoose');
const Meals = require('meals/models/meals.model');
const Toppings = require('toppings/models/toppings.model');
const perPage = config.PAGINATION_PERPAGE;

const ToppingsRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'price': parseFloat(req.body.query.generalSearch)
                        },
                        {
                            'meal_details.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Toppings.aggregate([{
                    $lookup: {
                        from: 'meals',
                        localField: 'meal',
                        foreignField: '_id',
                        as: 'meal_details'
                    }
                },
                {
                    $unwind: {
                        path: '$meal_details',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allToppings = await Toppings.aggregatePaginate(aggregate, options);
            return allToppings;
        } catch (e) {
            throw (e);
        }
    },

    getToppingCount: async (req) => {
        try {

            let toppings = await Toppings.find({
                isDeleted: false
            });
            return toppings;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let toppings = await Toppings.findById(id).exec();
            return toppings;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let toppings = await Toppings.findOne(params).exec();
            return toppings;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let toppings = await Toppings.find(params).exec();
            return toppings;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let toppings = await Toppings.findById(id);
            if (toppings) {
                let toppingDelete = await Toppings.remove({
                    _id: id
                }).exec();
                return toppingDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let topping = await Toppings.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return topping;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let topping = await Toppings.create(data);
            if (!topping) {
                return null;
            }
            return topping;
        } catch (e) {
            throw e;
        }
    },

    getToppingCountOnMeals: async (id) => {
        try {
            let toppingCountOfMeals = await Meals.find({
                'topping_id': id
            }).count();
            return toppingCountOfMeals;
        } catch (e) {
            throw (e);
        }
    },

    getToppingCount: async (req) => {
        try {
            let toppingCount = await Toppings.find({
                isDeleted: false
            });
            return toppingCount;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = ToppingsRepository;