const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const CategorySchema = new Schema({
  title: {
    type: String,
    default: '',
    unique: 'Category "{VALUE}" already exist!'
  },
  slug: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  }
});

CategorySchema.plugin(beautifyUnique);
// For pagination
CategorySchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Category', CategorySchema);