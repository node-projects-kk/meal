const categoryRepo = require('category/repositories/category.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class categoryController {

    /* @Method: create
    // @Description: create category action
    */
    async create(req, res) {
        try {
            res.render('category/views/add.ejs', {
                page_name: 'category-management',
                page_title: 'Create New Category',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save category action
    */
    async insert(req, res) {
        try {
            let SaveCategory = await categoryRepo.save(req.body);
            req.flash('success', 'Category created succesfully.');
            res.redirect(namedRouter.urlFor('category.listing'));
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('category.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  category edit page
    */
    async edit(req, res) {
        try {
            let categoryValue = await categoryRepo.getById(req.params.id);
            if (!_.isEmpty(categoryValue)) {
                res.render('category/views/edit.ejs', {
                    page_name: 'category-management',
                    page_title: 'Update Category',
                    user: req.user,
                    response: categoryValue
                });
            } else {
                req.flash('error', "Sorry Category not found!");
                res.redirect(namedRouter.urlFor('category.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: category update action
    */
    async update(req, res) {
        try {
            const CategoryId = req.body.cid;
            let CategoryUpdate = await categoryRepo.updateById(req.body, CategoryId);
            if (CategoryUpdate) {
                req.flash('success', "Category Updated Successfully");
                res.redirect(namedRouter.urlFor('category.listing'));
            } else {
                res.redirect(namedRouter.urlFor('category.edit', {
                    id: CategoryUpdate
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('category.edit', {
                id: req.body.cid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the category from DB
    */
    async list(req, res) {
        try {
            res.render('category/views/list.ejs', {
                page_name: 'category-management',
                page_title: 'Category Lists',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the category from DB
    */
    async getAll(req, res) {
        try {
            let categoryDetails = await categoryRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": categoryDetails.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": categoryDetails.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: categoryDetails.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: category delete
    */
    async delete(req, res) {
        try {
            let CategoryDelete = await categoryRepo.updateById({
                "isDeleted": true
            }, req.params.id);
            req.flash('success', 'Category Removed Successfully');
            res.redirect(namedRouter.urlFor('category.listing'));
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

}

module.exports = new categoryController();