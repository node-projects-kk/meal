const mongoose = require('mongoose');
const Category = require('category/models/category.model');
const Toppings = require('toppings/models/toppings.model');

const perPage = config.PAGINATION_PERPAGE;

const CategoryRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Category.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allCategory = await Category.aggregatePaginate(aggregate, options);
            return allCategory;
        } catch (e) {
            throw (e);
        }
    },

    getZipCount: async (req) => {
        try {

            let category = await Category.find({
                isDeleted: false
            });
            return category;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let category = await Category.findById(id).exec();
            return category;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let category = await Category.findOne(params).exec();
            return category;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let category = await Category.find(params).exec();
            return category;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let category = await Category.findById(id);

            if (category) {
                let categoryDelete = await Category.remove({
                    _id: id
                }).exec();
                return categoryDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let category = await Category.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return category;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let category = await Category.create(data);
            if (!category) {
                return null;
            }
            return category;
        } catch (e) {
            throw e;
        }
    },



};

module.exports = CategoryRepository;