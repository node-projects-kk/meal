const mongoose = require('mongoose');
const keyRepo = require('key_benefits/repositories/key.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class keyController {

    async create(req, res) {
        try {
            res.render('key_benefits/views/add.ejs', {
                page_name: 'key-management',
                page_title: 'Create New Key Benefits',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
            req.body.key_icon = '';
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('key.create'));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        if (req.files[0].fieldname == 'key_icon') {
                            gm('public/uploads/key/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/key/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('key.create'));
                                    }
                                });
                            req.body.key_icon = req.files[0].filename;
                        }
                    }
                }
                let newkey = await keyRepo.save(req.body);
                if (!_.isEmpty(newkey)) {
                    req.flash('success', 'Key Benefits created succesfully.');
                    res.redirect(namedRouter.urlFor('key.listing'));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('key.create'));
                }

            }
        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('key.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  key update page
    */
    async edit(req, res) {
        try {
            let keyBenefits = await keyRepo.getById(req.params.id);
            if (!_.isEmpty(keyBenefits)) {
                res.render('key_benefits/views/edit.ejs', {
                    page_name: 'key-management',
                    page_title: 'Update Key Benefits',
                    user: req.user,
                    response: keyBenefits
                });
            } else {
                req.flash('error', "Sorry Key Benefits not found!");
                res.redirect(namedRouter.urlFor('key.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: key update action
    */
    async update(req, res) {
        try {
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('key.edit', {
                    id: req.body.kid
                }));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        let key = await keyRepo.getByField({
                            '_id': req.body.kid
                        });
                        if (!_.isEmpty(key.key_icon)) {
                            if (fs.existsSync('public/uploads/key/' + key.key_icon)) {
                                const upl_img = fs.unlinkSync('public/uploads/key/' + key.key_icon);
                            }
                            if (fs.existsSync('public/uploads/key/thumb/' + key.key_icon)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/key/thumb/' + key.key_icon);
                            }
                        }
                        if (req.files[0].fieldname == 'key_icon') {
                            gm('public/uploads/key/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/key/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('key.edit', {
                                            id: req.body.kid
                                        }));
                                    }
                                });
                            req.body.key_icon = req.files[0].filename;
                        }
                    }
                }
                let keyUpdate = keyRepo.updateById(req.body, req.body.kid);
                if (keyUpdate) {
                    req.flash('success', 'Key Benefits updated succesfully.');
                    res.redirect(namedRouter.urlFor('key.listing'));
                } else {
                    res.redirect(namedRouter.urlFor('key.edit', {
                        id: req.body.kid
                    }));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the key from DB
    */
    async list(req, res) {
        try {
            res.render('key_benefits/views/list.ejs', {
                page_name: 'key-management',
                page_title: 'Key Benefits List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAll(req, res) {
        try {
            let keyData = await keyRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": keyData.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": keyData.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: keyData.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }

    /* @Method: delete
    // @Description: key Delete
    */
    async delete(req, res) {
        try {
            let key = await keyRepo.getByField({
                '_id': req.params.id
            });
            const keyExists = await keyRepo.getKeyCountOnMeals(req.params.id);
            if (keyExists > 0) {
                req.flash('error', 'You can\'t delete this Key Benefits, Beacuse this Key Benefits added in Meals.');
                res.redirect(namedRouter.urlFor('key.listing'));
            } else {
                let keyDelete = await keyRepo.delete(req.params.id)
                if (!_.isEmpty(keyDelete)) {
                    if (!_.isEmpty(key.key_icon)) {
                        if (fs.existsSync('public/uploads/key/' + key.key_icon)) {
                            const upl_img = fs.unlinkSync('public/uploads/key/' + key.key_icon);
                        }
                        if (fs.existsSync('public/uploads/key/thumb/' + key.key_icon)) {
                            const upl_thumb_img = fs.unlinkSync('public/uploads/key/thumb/' + key.key_icon);
                        }
                    }
                    req.flash('success', 'Key Benefits Removed Successfully');
                    res.redirect(namedRouter.urlFor('key.listing'));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };
}

module.exports = new keyController();