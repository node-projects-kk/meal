const mongoose = require('mongoose');
const Key = require('key_benefits/models/key.model');
const Meals = require('meals/models/meals.model');
const perPage = config.PAGINATION_PERPAGE;

const keyRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} });
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Key.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allKey = await Key.aggregatePaginate(aggregate, options);
            return allKey;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let key = await Key.findById(id).exec();
        try {
            if (!key) {
                return null;
            }
            return key;
        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let key = await Key.findOne(params).exec();
        try {
            if (!key) {
                return null;
            }
            return key;
        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let key = await Key.find(params).exec();
        try {
            if (!key) {
                return null;
            }
            return key;
        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let key = await Key.findById(id);
            if (key) {
                let keyDelete = await Key.remove({
                    _id: id
                }).exec();
                if (!keyDelete) {
                    return null;
                }
                return keyDelete;
            }
        } catch (e) {
            return e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },

    updateById: async (data, id) => {
        try {
            let key = await Key.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!key) {
                return null;
            }
            return key;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let key = await Key.create(data);
            if (!key) {
                return null;
            }
            return key;
        } catch (e) {
            throw e;
        }
    },

    getKeyCountOnMeals: async (id) => {
        try {
            let keyCountOfMeals = await Meals.find({
                'key_id': id
            }).count();
            return keyCountOfMeals;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = keyRepository;