var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];

var UserSchema = new Schema({
  first_name: {
    type: String,
    default: ''
  },
  last_name: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    default: ''
  },
  phone: {
    type: String,
    default: ''
  },
  password: {
    type: String,
    default: ''
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: 'Role'
  },
  isNewUser: {
    type: Boolean,
    default: true,
    enum: [true, false]
  },
  customer_id: {
    type: String,
    default: ''
  },
  stripe_token: {
    type: String,
    default: ''
  },
  isVerified: {
    type: String,
    default: "No",
    enum: ['Yes', 'No']
  },
  verifyToken: {
    type: Number,
    default: null
  },
  deviceToken: {
    type: String,
    default: ''
  },
  deviceType: {
    type: String,
    default: ''
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: deleted
  },
  isActive: {
    type: Boolean,
    default: true,
    enum: [true, false]
  },
}, {
  timestamps: true
});

// generating a hash
UserSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function (password, checkPassword) {
  return bcrypt.compareSync(password, checkPassword);
  //bcrypt.compare(jsonData.password, result[0].pass
};


// For pagination
UserSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('User', UserSchema);