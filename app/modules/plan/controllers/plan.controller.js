const mealRepo = require('meals/repositories/meals.repository');
const toppingRepo = require('toppings/repositories/toppings.repository');
const planRepo = require('plan/repositories/plan.repository');
const packageTypeRepo = require('packageType/repositories/packageType.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class PlanController {

    /* @Method: create
    // @Description: create plan action
    */
    async create(req, res) {
        let packageType = await packageTypeRepo.getAllByField({ 'isDeleted': false });
        const planData = planRepo.getAllByField({ 'isDeleted': false, 'status': 'Active' });
        try {
            res.render('plan/views/add.ejs', {
                page_name: 'package-management',
                page_title: 'Create New Package',
                user: req.user,
                response: planData,
                packageType: packageType
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save plan action
    */
    async insert(req, res) {
        try {
            let newPlans = await planRepo.save(req.body);
            req.flash('success', 'Package created succesfully.');
            res.redirect(namedRouter.urlFor('plan.listing'));
        } catch (e) {
            // console.log('e', e);
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('plan.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  plan edit page
    */
    async edit(req, res) {
        try {
            let packageType = await packageTypeRepo.getAllByField({ 'isDeleted': false });
            let plans = await planRepo.getById(req.params.id);
            if (!_.isEmpty(plans)) {
                res.render('plan/views/edit.ejs', {
                    page_name: 'package-management',
                    page_title: 'Update Package',
                    user: req.user,
                    response: plans,
                    packageType: packageType
                });
            } else {
                req.flash('error', "Sorry package not found!");
                res.redirect(namedRouter.urlFor('plan.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: plan update action
    */
    async update(req, res) {
        try {
            const planId = req.body.pid;
            let planUpdate = await planRepo.updateById(req.body, planId);
            if (planUpdate) {
                req.flash('success', "Package Updated Successfully");
                res.redirect(namedRouter.urlFor('plan.listing'));
            } else {
                res.redirect(namedRouter.urlFor('plan.edit', {
                    id: planId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('plan.edit', {
                id: req.body.pid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the plan from DB
    */
    async list(req, res) {
        try {

            res.render('plan/views/list.ejs', {
                page_name: 'package-management',
                page_title: 'Package List',
                user: req.user,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the plan from DB
    */
    async getAll(req, res) {
        try {
            //let plans = await planRepo.getAll(req);
            let plans = await planRepo.getAllAdmin(req);

            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": plans.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": plans.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: plans.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: plan delete
    */
    async delete(req, res) {
        try {
            let planDelete = await planRepo.updateById({
                "isDeleted": true
            }, req.params.id)
            req.flash('success', 'Package Removed Successfully');
            res.redirect(namedRouter.urlFor('plan.listing'));
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


}

module.exports = new PlanController();