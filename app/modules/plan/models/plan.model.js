const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const PlanSchema = new Schema({
  packageType_id: {
    type: Schema.Types.ObjectId,
    ref: 'PackageType'
  },
  box: {
    type: Number,
    default: ''
  },
  price: {
    type: Schema.Types.Double,
    default: 0.00
  },
  tag: {
    type: String,
    default: 'MostPopular',
    enum: ['MostPopular', 'BestValue']
  },
  
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  }
});

// For pagination
PlanSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Plan', PlanSchema);