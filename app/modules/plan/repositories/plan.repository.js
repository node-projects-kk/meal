const mongoose = require('mongoose');
const Meals = require('meals/models/meals.model');
const Toppings = require('toppings/models/toppings.model');
const Plans = require('plan/models/plan.model');
const PackageType = require('packageType/models/packageType.model');
const perPage = config.PAGINATION_PERPAGE;

const PlanRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'price': parseFloat(req.body.query.generalSearch)
                        },
                        {
                            'box': parseInt(req.body.query.generalSearch)
                        },
                        {
                            'tag': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Plans.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allPlans = await Plans.aggregatePaginate(aggregate, options);
            return allPlans;
        } catch (e) {
            throw (e);
        }
    },

    getAllAdmin: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                and_clauses.push({
                    $or: [{
                            'packagetype_details.title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'price': parseFloat(req.body.query.generalSearch)
                        },
                        {
                            'box': parseInt(req.body.query.generalSearch)
                        },
                        {
                            'tag': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Plans.aggregate([{
                    $lookup: {
                        "from": "packagetypes",
                        "localField": "packageType_id",
                        "foreignField": "_id",
                        "as": "packagetype_details"
                    }
                },
                {
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allPlans = await Plans.aggregatePaginate(aggregate, options);
            return allPlans;
        } catch (e) {
            throw (e);
        }
    },

    getPlanCount: async (req) => {
        try {

            let plans = await Plans.find({
                isDeleted: false
            });
            return plans;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let plans = await Plans.findById(id).exec();
            return plans;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let plans = await Plans.findOne(params).exec();
            return plans;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let plans = await Plans.find(params).exec();
            return plans;
        } catch (e) {
            throw (e);
        }
    },


    getAllPlan: async (params) => {
        try {
            var plist = await Plans.aggregate([{
                    $match: params
                },
                {
                    $sort: {
                        '_id': -1
                    }
                },
                {
                    $group: {
                        _id: "$title",
                        plan_details: {
                            $addToSet: "$$ROOT"
                        }
                    }
                }
            ]);
            return plist;
        } catch (e) {
            throw e;
        }
    },

    getAllPlansAPI: async () => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "package_details.isDeleted": false
            });
            conditions['$and'] = and_clauses;
            var plist = await PackageType.aggregate([{
                    $lookup: {
                        "from": "plans",
                        "localField": "_id",
                        "foreignField": "packageType_id",
                        "as": "package_details"
                    }
                },
                {
                    $unwind: "$package_details"
                },
                {
                    $match: conditions
                },
                {
                    $sort: {
                        '_id': -1,
                        'package_details.box': -1
                    }
                },
                {
                    $group: {
                        _id: {
                            _id: "$_id",
                            title: "$title"
                        },
                        title: {
                            $first: "$title"
                        },
                        description: {
                            $first: "$description"
                        },
                        createdAt: {
                            $first: "$createdAt"
                        },
                        package_details: {
                            $addToSet: "$package_details"
                        }
                    }
                }
            ]);
            return plist;
        } catch (e) {
            console.log('ererere', e);
            throw e;
        }
    },

    delete: async (id) => {
        try {
            let plans = await Plans.findById(id);
            if (plans) {
                let planDelete = await Plans.remove({
                    _id: id
                }).exec();
                return planDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let plan = await Plans.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return plan;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        console.log(data);
        try {
            let plan = await Plans.create(data);
            if (!plan) {
                return null;
            }
            return plan;
        } catch (e) {
            throw e;
        }
    },

    // getToppingCountOnMeals: async (id) => {
    //     try {
    //         let toppingCountOfMeals = await Plans.find({
    //             'topping_id': id
    //         }).count();
    //         return toppingCountOfMeals;
    //     } catch (e) {
    //         throw (e);
    //     }
    // },

    // getToppingCount: async (req) => {
    //     try {
    //         let toppingCount = await Toppings.find({
    //             isDeleted: false
    //         });
    //         return toppingCount;
    //     } catch (e) {
    //         throw (e);
    //     }
    // },

};

module.exports = PlanRepository;