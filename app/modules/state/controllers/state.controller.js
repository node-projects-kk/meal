const stateRepo = require('state/repositories/state.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class stateController {

    /* @Method: create
    // @Description: create state action
    */
    async create(req, res) {
        try {
            res.render('state/views/add.ejs', {
                page_name: 'state-management',
                page_title: 'Create New State',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save state action
    */
    async insert(req, res) {
        try {
            let SaveState = await stateRepo.save(req.body);
            req.flash('success', 'State created succesfully.');
            res.redirect(namedRouter.urlFor('state.listing'));
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('state.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  state edit page
    */
    async edit(req, res) {
        try {
            let stateValue = await stateRepo.getById(req.params.id);
            if (!_.isEmpty(stateValue)) {
                res.render('state/views/edit.ejs', {
                    page_name: 'state-management',
                    page_title: 'Update State',
                    user: req.user,
                    response: stateValue
                });
            } else {
                req.flash('error', "Sorry State not found!");
                res.redirect(namedRouter.urlFor('state.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: state update action
    */
    async update(req, res) {
        try {
            const stateId = req.body.sid;
            let stateUpdate = await stateRepo.updateById(req.body, stateId);
            if (stateUpdate) {
                req.flash('success', "State Updated Successfully");
                res.redirect(namedRouter.urlFor('state.listing'));
            } else {
                res.redirect(namedRouter.urlFor('state.edit', {
                    id: stateId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('state.edit', {
                id: req.body.sid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the state from DB
    */
    async list(req, res) {
        try {
            res.render('state/views/list.ejs', {
                page_name: 'state-management',
                page_title: 'State Lists',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the state from DB
    */
    async getAll(req, res) {
        try {
            let stateDetails = await stateRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = 1;
                var sortField = 'name';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": stateDetails.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": stateDetails.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: stateDetails.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: state delete
    */
    async delete(req, res) {
        try {
            let stateDelete = await stateRepo.updateById({
                "isDeleted": true
            }, req.params.id);
            req.flash('success', 'State Removed Successfully');
            res.redirect(namedRouter.urlFor('state.listing'));
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

}

module.exports = new stateController();