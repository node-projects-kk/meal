const mongoose = require('mongoose');
const State = require('state/models/state.model');
const perPage = config.PAGINATION_PERPAGE;

const StateRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'short_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['name'] = 1;
            }

            var aggregate = State.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allState = await State.aggregatePaginate(aggregate, options);
            return allState;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let state = await State.findById(id).exec();
            return state;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let state = await State.findOne(params).exec();
            return state;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let state = await State.find(params).exec();
            return state;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let state = await State.findById(id);

            if (state) {
                let stateDelete = await State.remove({
                    _id: id
                }).exec();
                return stateDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let state = await State.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return state;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let state = await State.create(data);
            if (!state) {
                return null;
            }
            return state;
        } catch (e) {
            throw e;
        }
    },

};

module.exports = StateRepository;