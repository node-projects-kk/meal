const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const StateSchema = new Schema({
  name: {
    type: String,
    default: '',
    unique: 'State "{VALUE}" already exist!'
  },
  short_name: {
    type: String,
    default: '',
    unique: 'Code "{VALUE}" already exist!'
  },
  status: {
    type: String,
    default: "Active",
    enum: ["Active", "Inactive"]
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  }
});

StateSchema.plugin(beautifyUnique);
// For pagination
StateSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('State', StateSchema);