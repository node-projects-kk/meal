const mongoose = require('mongoose');
const Meals = require('meals/models/meals.model');
const Toppings = require('toppings/models/toppings.model');
const Orders = require('order_management/models/order.model');
const Users = require('user/models/user.model');
const perPage = config.PAGINATION_PERPAGE;
// const perPage = 1

const OrderRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                // "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                        'order_no': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    },
                    {
                        'total_price': parseFloat(req.body.query.generalSearch)
                    },
                    {
                        'order_status': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    },
                    {
                        'payment_status': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    },
                    {
                        'user_info.first_name': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    },
                    {
                        'user_info.last_name': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Orders.aggregate([{
                $lookup: {
                    from: 'users',
                    localField: 'user_id',
                    foreignField: '_id',
                    as: 'user_info'
                }
            },
            {
                $unwind: {
                    path: '$user_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: conditions
            },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allOrders = await Orders.aggregatePaginate(aggregate, options);
            return allOrders;
        } catch (e) {
            throw (e);
        }
    },

    getOrderByCoupon: async (coupon_id) => {
        try {
            const query = [{
                "isDeleted": false,
                "coupons.status": "Active",
                "coupon.coupon_id": mongoose.Types.ObjectId(coupon_id),
                "order_status": {
                    $in: ["Pending", "Completed"]
                }
            }];

            var searchQuery = {
                "$and": query
            };

            return await Orders.aggregate([{
                $lookup: {
                    from: 'coupons',
                    localField: 'coupon.coupon_id',
                    foreignField: '_id',
                    as: 'coupons'
                }
            },
            {
                $unwind: '$coupons'
            },
            {
                $match: searchQuery
            }
            ]).exec();
        } catch (e) {
            throw (e);
        }
    },

    getOrderCount: async (req) => {
        try {

            let orders = await Orders.find({
                isDeleted: false
            });
            return orders;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let orders = await Orders.findById(id).lean().exec();
            return orders;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let orders = await Orders.findOne(params).exec();
            return orders;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let orders = await Orders.find(params).exec();
            return orders;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let orders = await Orders.findById(id);
            if (orders) {
                let orderDelete = await Orders.remove({
                    _id: id
                }).exec();
                return orderDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let order = await Orders.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return order;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let order = await Orders.create(data);
            if (!order) {
                return null;
            }
            return order;
        } catch (e) {
            throw e;
        }
    },

    orderViewDetail: async (id) => {
        try {
            return await Orders.aggregate([{
                $match: {
                    '_id': mongoose.Types.ObjectId(id)
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'user_id',
                    foreignField: '_id',
                    as: 'user_info'
                }
            },
            {
                $unwind: {
                    path: '$user_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: '$order_details'
            },
            {
                $lookup: {
                    from: 'meals',
                    localField: 'order_details.meal_id',
                    foreignField: '_id',
                    as: 'order_details.meal_info'
                }
            },
            {
                $unwind: {
                    path: '$order_details.meal_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$order_details.topping_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'toppings',
                    localField: 'order_details.topping_details.topping_id',
                    foreignField: '_id',
                    as: 'order_details.topping_details'
                }
            },
            {
                $unwind: {
                    path: '$order_details.topping_details',
                    preserveNullAndEmptyArrays: true
                }
            },

            {
                $group: {
                    "_id": {
                        "id": "$_id",
                        "meal_id": "$order_details.meal_id"
                    },
                    user_id: {
                        $first: "$user_info"
                    },
                    discounted_price: {
                        $first: "$discounted_price"
                    },
                    coupon: {
                        $first: "$coupon"
                    },
                    order_details: {
                        $addToSet: "$order_details"
                    },
                    shipping_address: {
                        $first: "$shipping_address"
                    },
                    deliver_arrive_on: {
                        $first: "$deliver_arrive_on"
                    },
                    deliver_day: {
                        $first: "$deliver_day"
                    },
                    order_no: {
                        $first: "$order_no"
                    },
                    total_price: {
                        $first: "$total_price"
                    },
                    order_status: {
                        $first: "$order_status"
                    },
                    next_order_status: {
                        $first: "$next_order_status"
                    },
                    payment_status: {
                        $first: "$payment_status"
                    },
                    status: {
                        $first: "$status"
                    },
                    isDeleted: {
                        $first: "$isDeleted"
                    },
                    createdAt: {
                        $first: "$createdAt"
                    }
                }
            },
            {
                $group: {
                    _id: "$_id.id",
                    user_id: {
                        $first: "$user_id"
                    },
                    order_no: {
                        $first: "$order_no"
                    },
                    deliver_arrive_on: {
                        $first: "$deliver_arrive_on"
                    },
                    deliver_day: {
                        $first: "$deliver_day"
                    },
                    total_price: {
                        $first: "$total_price"
                    },
                    order_status: {
                        $first: "$order_status"
                    },
                    next_order_status: {
                        $first: "$next_order_status"
                    },
                    payment_status: {
                        $first: "$payment_status"
                    },
                    status: {
                        $first: "$status"
                    },
                    isDeleted: {
                        $first: "$isDeleted"
                    },
                    createdAt: {
                        $first: "$createdAt"
                    },
                    order_details: {
                        $addToSet: "$order_details"
                    },
                    shipping_address: {
                        $first: "$shipping_address"
                    },
                    discounted_price: {
                        $first: "$discounted_price"
                    },
                    coupon: {
                        $first: "$coupon"
                    },
                }
            }
            ]).exec();
        } catch (e) {
            throw (e);
        }
    },

    getBestSellerProduct: async (params) => {
        try {
            let meals = await Orders.aggregate([{
                $unwind: "$order_details"
            },
            {
                $match: params
            },
            {
                $lookup: {
                    from: "meals",
                    localField: "order_details.meal_id",
                    foreignField: "_id",
                    as: "meal_details"
                }
            },
            {
                $unwind: {
                    "path": "$meal_details",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "categories",
                    localField: "meal_details.category_id",
                    foreignField: "_id",
                    as: "category_details"
                }
            },
            {
                $unwind: {
                    "path": "$category_details",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $group: {
                    _id: "$order_details.meal_id",
                    meals: {
                        $first: "$meal_details"
                    },
                    category: {
                        $first: "$category_details.title"
                    },
                    itemsale: {
                        $sum: 1
                    }
                }
            },
            {
                $sort: {
                    itemsale: -1
                }
            },
            {
                $limit: 12
            }
            ]).exec();

            return meals;
        } catch (e) {
            throw (e);
        }
    },

    userOrderHistoryList: async (id, page) => {
        try {
            let aggregate = Orders.aggregate([{
                $match: {
                    'user_id': mongoose.Types.ObjectId(id),
                    'isDeleted': false
                }
            },
            {
                $unwind: '$order_details'
            },
            {
                $lookup: {
                    from: 'meals',
                    localField: 'order_details.meal_id',
                    foreignField: '_id',
                    as: 'order_details.meal_info'
                }
            },
            {
                $unwind: {
                    path: '$order_details.meal_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'order_details.meal_info.category_id',
                    foreignField: '_id',
                    as: 'order_details.category_info'
                }
            },
            {
                $unwind: {
                    path: '$order_details.category_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$order_details.topping_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'toppings',
                    localField: 'order_details.topping_details.topping_id',
                    foreignField: '_id',
                    as: 'order_details.topping_details'
                }
            },
            {
                $unwind: {
                    path: '$order_details.topping_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    "_id": "$_id",
                    order_no: {
                        $first: '$order_no'
                    },
                    shipping_address: {
                        $first: '$shipping_address'
                    },
                    coupon: {
                        $first: '$coupon'
                    },
                    deliver_arrive_on: {
                        $first: '$deliver_arrive_on'
                    },
                    deliver_day: {
                        $first: '$deliver_day'
                    },
                    total_price: {
                        $first: '$total_price'
                    },
                    discounted_price: {
                        $first: '$discounted_price'
                    },
                    createdAt: {
                        $first: '$createdAt'
                    },
                    order_status: {
                        $first: '$order_status'
                    },
                    order_details: {
                        $addToSet: "$order_details"
                    },
                }
            },
            {
                $sort: {
                    createdAt: -1,
                }
            }
            ]);

            let options = {
                page: page,
                limit: perPage
            };
            return await Orders.aggregatePaginate(aggregate, options);
        } catch (e) {
            throw (e);
        }
    },

    userOrderHistoryDetails: async (id) => {
        try {
            return await Orders.aggregate([{
                $match: {
                    '_id': mongoose.Types.ObjectId(id),
                }
            },
            {
                $unwind: '$order_details'
            },
            {
                $lookup: {
                    from: 'meals',
                    localField: 'order_details.meal_id',
                    foreignField: '_id',
                    as: 'order_details.meal_info'
                }
            },
            {
                $unwind: {
                    path: '$order_details.meal_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'order_details.meal_info.category_id',
                    foreignField: '_id',
                    as: 'order_details.category_info'
                }
            },
            {
                $unwind: {
                    path: '$order_details.category_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$order_details.topping_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'toppings',
                    localField: 'order_details.topping_details.topping_id',
                    foreignField: '_id',
                    as: 'order_details.topping_details'
                }
            },
            {
                $unwind: {
                    path: '$order_details.topping_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    "_id": "$_id",
                    order_no: {
                        $first: '$order_no'
                    },
                    shipping_address: {
                        $first: '$shipping_address'
                    },
                    coupon: {
                        $first: '$coupon'
                    },
                    deliver_arrive_on: {
                        $first: '$deliver_arrive_on'
                    },
                    deliver_day: {
                        $first: '$deliver_day'
                    },
                    post_code: {
                        $first: '$post_code'
                    },
                    total_price: {
                        $first: '$total_price'
                    },
                    discounted_price: {
                        $first: '$discounted_price'
                    },
                    createdAt: {
                        $first: '$createdAt'
                    },
                    order_status: {
                        $first: '$order_status'
                    },
                    order_details: {
                        $addToSet: "$order_details"
                    },
                }
            }
            ]).exec();
        } catch (e) {
            throw (e);
        }
    },


    getPendingOrdersAndUserDetailsBackup: async (params) => {
        try {
            return await Orders.aggregate([{
                $match: params
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'user_id',
                    foreignField: '_id',
                    as: 'user_info'
                }
            },
            {
                $unwind: {
                    path: '$user_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            ]).exec();
        } catch (e) {
            throw (e);
        }
    },

    async getPendingOrdersAndUserDetails(params) {
        try {
            console.log('params <><>702<><>', params);
            return await Orders.aggregate([{
                $lookup: {
                    from: 'users',
                    localField: 'user_id',
                    foreignField: '_id',
                    as: 'user_info'
                }
            },
            {
                $unwind: {
                    path: '$user_info',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    'delivery_date': {
                        $dateToString: {
                            format: "%Y-%m-%d",
                            date: "$deliver_arrive_on"
                        }
                    },
                    '_id': 1,
                    'order_no': 1,
                    'total_price': 1,
                    'deliver_arrive_on': 1,
                    'deliver_day': 1,
                    'user_info': 1,
                    'order_status': 1,
                    'payment_status': 1,
                    'status': 1,
                    'isDeleted': 1
                }
            },
            {
                '$match': {
                    'delivery_date': params,
                    'order_status': 'Pending',
                    'payment_status': 'Pending',
                    'status': 'Active',
                    'isDeleted': false
                }
            },
            {
                $group: {
                    '_id': {
                        delivery_date: "$delivery_date",
                    },
                    'order_id': {
                        $first: '$_id'
                    },
                    'order_no': {
                        $first: '$order_no'
                    },
                    'total_price': {
                        $first: '$total_price'
                    },
                    'deliver_arrive_on': {
                        $first: '$deliver_arrive_on'
                    },
                    'deliver_day': {
                        $first: '$deliver_day'
                    },
                    'order_status': {
                        $first: '$order_status'
                    },
                    'payment_status': {
                        $first: '$payment_status'
                    },
                    'user_info': {
                        $first: '$user_info'
                    },
                }
            },
            {
                $sort: {
                    '_id': -1
                }
            }

            ]).exec();
        } catch (error) {
            console.log('ererererere <><><>repo<><><>', error);
            return error;
        }
    }
};

module.exports = OrderRepository;