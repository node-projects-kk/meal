const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const dtype = ["flat", "percent"];
const order = ['Pending', 'Completed', 'Cancel'];
const nextOrderStatus = [true, false];

const OrderSchema = new Schema({
	user_id: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	order_no: {
		type: String,
		default: ''
	},
	total_price: {
		type: Schema.Types.Double,
		default: 0.00
	},
	discounted_price: {
		type: Schema.Types.Double,
		default: 0.00
	},
	coupon: {
		coupon_id: {
			type: Schema.Types.ObjectId,
			ref: 'Coupon'
		},
		coupon_code: {
			type: String,
			default: ''
		},
		coupon_description: {
			type: String,
			default: ''
		},
		amount: {
			type: Schema.Types.Double,
			default: 0.00
		},
		discount_type: {
			type: String,
			default: 'flat',
			enum: dtype
		},
	},
	order_details: [{
		_id: false,
		meal_id: {
			type: Schema.Types.ObjectId,
			ref: 'Meal'
		},
		quantity: {
			type: Number,
			default: ''
		},
		meal_price: {
			type: Schema.Types.Double,
			default: 0.00
		},
		topping_details: [{
			topping_id: {
				type: Schema.Types.ObjectId,
				ref: 'Topping',
				default: null
			},
			price: {
				type: Schema.Types.Double,
				default: 0.00
			},
			_id: false
		}]
	}],
	stripe_charge_id: {
		type: String,
		default: ''
	},
	is_refunded: {
		type: Boolean,
		default: false,
		enum: [true, false]
	},
	refund_details: [],
	shipping_address: {
		first_name: {
			type: String,
			default: ''
		},
		last_name: {
			type: String,
			default: ''
		},
		address: {
			type: String,
			default: ''
		},
		apt_suite: {
			type: String,
			default: ''
		},
		city: {
			type: String,
			default: ''
		},
		state: {
			type: String,
			default: ''
		},
		phone: {
			type: String,
			default: ''
		},
		company_name: {
			type: String,
			default: ''
		},
	},
	post_code: {
		type: String,
		default: ''
	},
	deliver_arrive_on: {
		type: Date,
		default: Date.now()
	},
	deliver_day: {
		type: String,
		default: ''
	},
	order_status: {
		type: String,
		default: 'Pending',
		enum: order
	},
	payment_status: {
		type: String,
		default: 'Pending',
		enum: order
	},
	next_order_status: {
		type: Boolean,
		default: false,
		enum: nextOrderStatus
	},
	status: {
		type: String,
		default: "Active",
		enum: ["Active", "Inactive"]
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: [true, false]
	},
	createdAt: {
		type: Date,
		default: Date.now(),
	}
});

OrderSchema.plugin(beautifyUnique);
// For pagination
OrderSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Order', OrderSchema);