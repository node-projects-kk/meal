const mealRepo = require('meals/repositories/meals.repository');
const toppingRepo = require('toppings/repositories/toppings.repository');
const userRepo = require('user/repositories/user.repository');
const orderRepo = require('order_management/repositories/order.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');
var moment = require('moment');
const mongoose = require('mongoose');
var stripe = require("stripe")(config.stripe_secret_key);
const settingRepo = require('setting/repositories/setting.repository');
const mailer = require('../../../helper/mailer.js');
class OrderController {

    /*
    // @Method: view
    // @Description:  orders edit page
    */
    async view(req, res) {
        try {
            let orderDetails = await orderRepo.orderViewDetail(req.params.id);
            // console.log(JSON.stringify(orderDetails));
            if (!_.isEmpty(orderDetails)) {
                res.render('order_management/views/view.ejs', {
                    page_name: 'order-management',
                    page_title: 'Update Order',
                    user: req.user,
                    response: orderDetails[0],
                    oid: req.params.id
                });
            } else {
                req.flash('error', "Sorry order not found!");
                res.redirect(namedRouter.urlFor('orders.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: orders update action
    */
    async update(req, res) {
        try {
            const orderId = req.body.orderid;
            let orderUpdate = await orderRepo.updateById(req.body, orderId);
            if (orderUpdate) {
                req.flash('success', "Order Status Updated Successfully");
                res.redirect(namedRouter.urlFor('orders.listing'));
            } else {
                res.redirect(namedRouter.urlFor('orders.view', {
                    id: req.body.orderid
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('orders.view', {
                id: req.body.orderid
            }));
        }
    };

    /* @Method: list
    // @Description: To list all the orders from DB
    */
    async list(req, res) {
        try {

            res.render('order_management/views/list.ejs', {
                page_name: 'order-management',
                page_title: 'Order List',
                user: req.user,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the orders from DB
    */
    async getAll(req, res) {
        try {
            let orders = await orderRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": orders.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": orders.totalCount,
                "sort": sortOrder,
                "field": sortField
            };

            return {
                status: 200,
                meta: meta,
                data: orders.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    async autoRenewUserOrder(req, res) {
        try {
            let getOrderDetails = await orderRepo.getById(mongoose.Types.ObjectId(req.body.id));
            if (req.body.status == 'Completed') {
                if ((req.body.next_order_status !== null) && (req.body.next_order_status == 'true')) {
                    let nextDeliveryDate = moment(getOrderDetails.deliver_arrive_on).add(7, 'days');
                    let onlyDay = moment(nextDeliveryDate).format('dddd');

                    let uniqueOrdNo = await generateUniqueOrderNumber();
                    getOrderDetails.order_no = uniqueOrdNo;
                    getOrderDetails.deliver_arrive_on = nextDeliveryDate;
                    getOrderDetails.stripe_charge_id = '';
                    getOrderDetails.deliver_day = onlyDay;
                    getOrderDetails.isNewUser = false;
                    getOrderDetails.order_status = 'Pending';
                    getOrderDetails.payment_status = 'Pending';
                    var cloneObj = _.omit(getOrderDetails, '_id');
                    const RepeatTheOrder = await orderRepo.save(cloneObj);
                    let updateOldOrderStatus = await orderRepo.updateById({
                        'order_status': req.body.status
                    }, req.body.id);

                    const setting_data = await settingRepo.getAllSetting();
                    var settingObj = {};
                    if (!_.isEmpty(setting_data)) {
                        setting_data.forEach(function (element) {
                            settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                        });
                    }

                    let orderDetailsForMail = await orderRepo.orderViewDetail(RepeatTheOrder._id);

                    //send email to admin
                    let localsForAdmin = {
                        response: orderDetailsForMail[0],
                    };

                    let isMailSendAdmin = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, settingObj.Site_Email, 'Order Placed Successfully | Mass Meal', 'order-admin', localsForAdmin);

                    return {
                        status: 200,
                        data: RepeatTheOrder,
                        message: 'You order has been placed Successfully.'
                    }
                } else {
                    let updateOldOrderStatus = await orderRepo.updateById({
                        'order_status': req.body.status
                    }, req.body.id);

                    return {
                        status: 200,
                        data: updateOldOrderStatus,
                        message: 'Status changed successfully.'
                    }
                }
            }
        } catch (error) {
            console.log('erererererer', error);
            throw error;
        }
    }

    async orderAutoPayment(req, res) {
        try {
            let currentDate = moment().format();
            let nextDate = moment(currentDate).add(1, 'days').format();
            let formatDate = moment(nextDate).format('YYYY-MM-DD');
            let getAllPendingOrders = await orderRepo.getPendingOrdersAndUserDetails(formatDate);

            const setting_data = await settingRepo.getAllSetting();
            var settingObj = {};
            if (!_.isEmpty(setting_data)) {
                setting_data.forEach(function (element) {
                    settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
                });
            }

            if (!_.isEmpty(getAllPendingOrders)) {
                for (var key in getAllPendingOrders) {
                    const stripeCharge = await stripe.charges.create({
                        amount: getAllPendingOrders[key].total_price * 100,
                        currency: 'usd',
                        description: 'order charge',
                        customer: getAllPendingOrders[key].user_info.customer_id
                    });

                    if (stripeCharge) {
                        const updateUser = await userRepo.updateById({
                            'isNewUser': false,
                        }, getAllPendingOrders[key].user_info._id);

                        const orderUpdate = await orderRepo.updateById({
                            'stripe_charge_id': stripeCharge.id,
                            'payment_status': 'Completed'
                        }, getAllPendingOrders[key].order_id);

                        //send email to user for successfull payment
                        let localsForUserRepeatOrder = {
                            response: getAllPendingOrders[key],
                            logo_url: `${process.env.EMAIL_TEMPLATE_IMAGE_LINK}`
                        };

                        let isMailSendForUserRepeatOrder = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, getAllPendingOrders[key].user_info.email, 'Repeat Order Placed Successfully | Mass Meal', 'repeat-order-customer', localsForUserRepeatOrder);

                        //send email to admin
                        let localsForAdminRepeatOrder = {
                            response: getAllPendingOrders[key],
                            logo_url: `${process.env.EMAIL_TEMPLATE_IMAGE_LINK}`
                        };

                        let isMailSendForAdminRepeatOrder = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, settingObj.Site_Email, 'Repeat Order Placed Successfully | Mass Meal', 'repeat-order-admin', localsForAdminRepeatOrder);

                        return {
                            "status": 200,
                            data: order,
                            "message": "Order placed successfully."
                        };
                    } else {
                        return {
                            status: 201,
                            data: [],
                            message: 'Payment Failed, We are unable to placed the Order, Please try again.'
                        }
                    }
                }
            } else {
                return {
                    status: 201,
                    data: [],
                    message: 'There are no Pending Orders at this moment.'
                }
            }
        } catch (error) {
            throw error;
        }
    }

    async generateUniqueOrderNumber(req) {
        const obj = {
            'length': 10,
            'chars': '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        };
        const uniqueNumber = randomString(obj);
        const orderDet = orderRepo.getByField({
            'order_no': uniqueNumber.toString()
        });
        if (_.isEmpty(orderDet)) {
            return uniqueNumber;
        } else {
            return generateUniqueOrderNumber();
        }
    };

    async randomString(req) {
        var result = '';
        for (var i = req.length; i > 0; --i)
            result += req.chars[Math.round(Math.random() * (req.chars.length - 1))];
        return result;
    };

}

module.exports = new OrderController();