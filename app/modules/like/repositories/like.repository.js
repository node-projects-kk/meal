const mongoose = require('mongoose');
const Like = require('like/models/like.model');
const Meals = require('meals/models/meals.model');
const perPage = config.PAGINATION_PERPAGE;

const likeRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} });
                and_clauses.push({
                    'title': {
                        $regex: req.body.query.generalSearch,
                        $options: 'i'
                    }
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))
            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Like.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allLike = await Like.aggregatePaginate(aggregate, options);
            return allLike;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let like = await Like.findById(id).exec();
        try {
            if (!like) {
                return null;
            }
            return like;
        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let like = await Like.findOne(params).exec();
        try {
            if (!like) {
                return null;
            }
            return like;
        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let like = await Like.find(params).exec();
        try {
            if (!like) {
                return null;
            }
            return like;
        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let like = await Like.findById(id);
            if (like) {
                let likeDelete = await Like.remove({
                    _id: id
                }).exec();
                if (!likeDelete) {
                    return null;
                }
                return likeDelete;
            }
        } catch (e) {
            return e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },

    updateById: async (data, id) => {
        try {
            let like = await Like.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!like) {
                return null;
            }
            return like;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let like = await Like.create(data);
            if (!like) {
                return null;
            }
            return like;
        } catch (e) {
            throw e;
        }
    },

    getLikeCountOnMeals: async (id) => {
        try {
            let likeCountOfMeals = await Meals.find({
                'like_id': id
            }).count();
            return likeCountOfMeals;
        } catch (e) {
            throw (e);
        }
    },

};

module.exports = likeRepository;