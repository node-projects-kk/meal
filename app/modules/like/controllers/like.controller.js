const mongoose = require('mongoose');
const likeRepo = require('like/repositories/like.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var gm = require('gm').subClass({
    imageMagick: true
});
const errorHandler = require('../../../errorHandler');

class likeController {

    async create(req, res) {
        try {
            res.render('like/views/add.ejs', {
                page_name: 'like-management',
                page_title: 'Create New Like',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    async insert(req, res) {
        try {
            req.body.like_icon = '';
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('like.create'));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        if (req.files[0].fieldname == 'like_icon') {
                            gm('public/uploads/like/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/like/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('like.create'));
                                    }
                                });
                            req.body.like_icon = req.files[0].filename;
                        }
                    }
                }
                let newlike = await likeRepo.save(req.body);
                if (!_.isEmpty(newlike)) {
                    req.flash('success', 'Like created succesfully.');
                    res.redirect(namedRouter.urlFor('like.listing'));
                } else {
                    req.flash('error', "Sorry some error occur!");
                    res.redirect(namedRouter.urlFor('like.create'));
                }
            }
        } catch (e) {
            req.flash('error', e.message);
            res.redirect(namedRouter.urlFor('like.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  like update page
    */
    async edit(req, res) {
        try {
            let likeBenefits = await likeRepo.getById(req.params.id);
            if (!_.isEmpty(likeBenefits)) {
                res.render('like/views/edit.ejs', {
                    page_name: 'like-management',
                    page_title: 'Update Like',
                    user: req.user,
                    response: likeBenefits
                });
            } else {
                req.flash('error', "Sorry Like not found!");
                res.redirect(namedRouter.urlFor('like.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: like update action
    */
    async update(req, res) {
        try {
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('like.edit', {
                    id: req.body.kid
                }));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        let like = await likeRepo.getByField({
                            '_id': req.body.kid
                        });
                        if (!_.isEmpty(like.like_icon)) {
                            if (fs.existsSync('public/uploads/like/' + like.like_icon)) {
                                const upl_img = fs.unlinkSync('public/uploads/like/' + like.like_icon);
                            }
                            if (fs.existsSync('public/uploads/like/thumb/' + like.like_icon)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/like/thumb/' + like.like_icon);
                            }
                        }
                        if (req.files[0].fieldname == 'like_icon') {
                            gm('public/uploads/like/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/like/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('like.edit', {
                                            id: req.body.kid
                                        }));
                                    }
                                });
                            req.body.like_icon = req.files[0].filename;
                        }
                    }
                }
                let likeUpdate = likeRepo.updateById(req.body, req.body.kid);
                if (likeUpdate) {
                    req.flash('success', "Like updated succesfully.");
                    res.redirect(namedRouter.urlFor('like.listing'));
                } else {
                    res.redirect(namedRouter.urlFor('like.edit', {
                        id: req.body.kid
                    }));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: list
    // @Description: To get all the like from DB
    */
    async list(req, res) {
        try {
            res.render('like/views/list.ejs', {
                page_name: 'like-management',
                page_title: 'Like List',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    async getAll(req, res) {
        try {
            let likeData = await likeRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": likeData.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": likeData.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: likeData.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            return {
                status: 500,
                data: [],
                message: e.message
            };
        }
    }

    /* @Method: delete
    // @Description: like Delete
    */
    async delete(req, res) {
        try {
            let like = await likeRepo.getByField({
                '_id': req.params.id
            });
            const likeExists = await likeRepo.getLikeCountOnMeals(req.params.id);
            if (likeExists > 0) {
                req.flash('error', 'You can\'t delete this Like, Beacuse this Like added in Meals.');
                res.redirect(namedRouter.urlFor('like.listing'));
            } else {
                let likeDelete = await likeRepo.delete(req.params.id)
                if (!_.isEmpty(likeDelete)) {
                    if (!_.isEmpty(like.like_icon)) {
                        if (fs.existsSync('public/uploads/like/' + like.like_icon)) {
                            const upl_img = fs.unlinkSync('public/uploads/like/' + like.like_icon);
                        }
                        if (fs.existsSync('public/uploads/like/thumb/' + like.like_icon)) {
                            const upl_thumb_img = fs.unlinkSync('public/uploads/like/thumb/' + like.like_icon);
                        }
                    }
                    req.flash('success', 'Like Removed Successfully');
                    res.redirect(namedRouter.urlFor('like.listing'));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


}

module.exports = new likeController();