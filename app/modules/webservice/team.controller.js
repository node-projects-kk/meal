const teamRepo = require('team/repositories/team.repository');
const aboutRepo = require('about/repositories/about.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();

class teamController {
  constructor() {}

  async list(req) {
    try {
      let teamlist = await teamRepo.getAllByField({
        'status': 'Active'
      });
      if (!_.isEmpty(teamlist) && !_.isNull(teamlist)) {
        return {
          "status": 200,
          data: teamlist,
          "message": "Team fetched successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No record found."
        };
      }
    } catch (error) {
      throw error;
    }
  }

  async aboutContent(req) {
    try {
      let aboutlist = await aboutRepo.getByField({
        'status': 'Active'
      });
      if (!_.isEmpty(aboutlist) && !_.isNull(aboutlist)) {
        return {
          "status": 200,
          data: aboutlist,
          "message": "About page fetched successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No record found."
        };
      }
    } catch (error) {
      throw error;
    }
  }

}

module.exports = new teamController();