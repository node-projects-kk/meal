const categoryRepo = require('category/repositories/category.repository');
const dietaryRepo = require('dietary/repositories/dietary.repository');
const key_benefitsRepo = require('key_benefits/repositories/key.repository');
const likeRepo = require('like/repositories/like.repository');
const stateRepo = require('state/repositories/state.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();


class categoryController {
	constructor() {}

	async list(req) {
		try {
			let params = {
				"isDeleted": false,
				"status": "Active"
			};
			let categorylist = await categoryRepo.getAllByField(params);

			if (!_.isEmpty(categorylist) && !_.isNull(categorylist)) {
				return {
					"status": 200,
					data: categorylist,
					"message": "Category fetched successfully."
				};
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, No record found."
				};
			}
		} catch (error) {
			throw error;
		}
	}

	async typeList(req) {
		try {
			let typelist;
			let type = req.params.type;
			// console.log("49>>", type);
			let params = {
				"isDeleted": false,
				"status": "Active"
			};
			if (type == "like") {
				typelist = await likeRepo.getAllByField(params);
			}
			if (type == "dietary") {
				typelist = await dietaryRepo.getAllByField(params);
			}
			if (type == "key_benefits") {
				typelist = await key_benefitsRepo.getAllByField(params);
			}
			if (type == "state") {
				typelist = await stateRepo.getAllByField(params);
			}

			if (!_.isEmpty(typelist) && !_.isNull(typelist)) {
				return {
					"status": 200,
					data: typelist,
					"message": +type + " fetched successfully."
				};
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, No record found."
				};
			}
		} catch (error) {
			// console.log('controller error', error.message);
			throw error;
		}
	}




}

module.exports = new categoryController();