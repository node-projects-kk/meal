const planRepo = require('plan/repositories/plan.repository');
const express = require('express');
const routeLabel = require('route-label');
const helper = require('../../helper/helper.js');
const mailer = require('../../helper/mailer.js');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");

//mail send 
const {
	join
} = require('path');
const ejs = require('ejs');
const {
	readFile
} = require('fs');
const {
	promisify
} = require('util');
const readFileAsync = promisify(readFile);

class planController {
	constructor() {}

	async list(req) {
		try {
			let planlist = await planRepo.getAllPlansAPI();
			if (!_.isEmpty(planlist)) {
				return {
					"status": 200,
					data: planlist,
					"message": "Plan fetched successfully."
				};
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, No record found."
				};
			}
		} catch (error) {
			// console.log('controller error', error.message);
			return {
				status: 500,
				message: error.message
			}
		}
	}

}

module.exports = new planController();