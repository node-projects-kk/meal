const stateRepo = require('state/repositories/state.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


class stateController {
  constructor() {}


  async list(req) {
    try {
      let params = {
        "isDeleted": false,
        "status": "Active"
      };
      let statelist = await stateRepo.getAllByField(params);

      if (!_.isEmpty(statelist) && !_.isNull(statelist)) {
        return {
          "status": 200,
          data: statelist,
          "message": "States fetched successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No record found."
        };
      }
    } catch (error) {
      // console.log('controller error', error.message);
      throw error;
    }
  }


}

module.exports = new stateController();