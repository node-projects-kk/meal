const categoryRepo = require('category/repositories/category.repository');
const dietaryRepo = require('dietary/repositories/dietary.repository');
const key_benefitsRepo = require('key_benefits/repositories/key.repository');
const likeRepo = require('like/repositories/like.repository');
const productRepo = require('meals/repositories/meals.repository');
const orderRepo = require('order_management/repositories/order.repository');
const reviewRepo = require('reviews/repositories/review.repository');
const express = require('express');
const routeLabel = require('route-label');
const helper = require('../../helper/helper.js');
const mailer = require('../../helper/mailer.js');
const router = express.Router();
const namedRouter = routeLabel(router);
const mongoose = require('mongoose');
const querystring = require('querystring');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");

//mail send 
const {
	join
} = require('path');
const ejs = require('ejs');
const {
	readFile
} = require('fs');
const {
	promisify
} = require('util');
const readFileAsync = promisify(readFile);

class productController {
	constructor() {}

	async list(req) {
		try {
			let params = {
				"isDeleted": false,
				"status": "Active"
			};
			const currentPage = req.query.page || 1;
			let productlist = await productRepo.getProductAll(req.body, currentPage);
			if (!_.isEmpty(productlist) && !_.isNull(productlist)) {
				return {
					"status": 200,
					data: productlist,
					"message": "Product fetched successfully."
				};
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, No record found."
				};
			}
		} catch (error) {
			throw error;
		}
	}

	async details(req) {
		try {
			let productID = req.params.id; //console.log("49>>",productID);
			let params = {
				"isDeleted": false,
				"status": "Active",
				"_id": mongoose.Types.ObjectId(productID)
			};
			let products = await productRepo.getProductDetail(params);
			if (!_.isEmpty(products) && !_.isNull(products)) {
				return {
					"status": 200,
					data: products,
					"message": "product details fetched successfully."
				};
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, No record found."
				};
			}
		} catch (error) {
			throw error;
		}
	}

	async bestsellerList(req) {
		try {
			let params = {
				"isDeleted": false,
				"status": "Active"
			};
			let products = await orderRepo.getBestSellerProduct(params);
			if (!_.isEmpty(products) && !_.isNull(products)) {
				return {
					"status": 200,
					data: products,
					"message": "Best sell product list fetched successfully."
				};
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, No record found."
				};
			}
		} catch (error) {
			// console.log('controller error', error.message);
			throw error;
		}
	}

	async productReview(req) {
		try {
			req.body.user_id = req.user._id;
			const saveReview = await reviewRepo.save(req.body);
			if (!_.isEmpty(saveReview) && !_.isNull(saveReview)) {
				return {
					status: 200,
					data: saveReview,
					message: 'Thank you, We have received your review on this Meal.'
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'Sorry, We haven\'t received any review, Please try again.'
				}
			}
		} catch (error) {
			throw error;
		}
	}




}

module.exports = new productController();