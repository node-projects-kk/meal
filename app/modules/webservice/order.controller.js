const categoryRepo = require('category/repositories/category.repository');
const dietaryRepo = require('dietary/repositories/dietary.repository');
const key_benefitsRepo = require('key_benefits/repositories/key.repository');
const likeRepo = require('like/repositories/like.repository');
const roleRepo = require('role/repositories/role.repository');
const userRepo = require('user/repositories/user.repository');
const settingRepo = require('setting/repositories/setting.repository');
const productRepo = require('meals/repositories/meals.repository');
const orderRepo = require('order_management/repositories/order.repository');
const userModel = require('user/models/user.model');
const User = new userModel();
const express = require('express');
const routeLabel = require('route-label');
const helper = require('../../helper/helper.js');
const mailer = require('../../helper/mailer.js');
const router = express.Router();
const namedRouter = routeLabel(router);
const mongoose = require('mongoose');
const querystring = require('querystring');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const async = require('async');
var stripe = require("stripe")(config.stripe_secret_key);
const couponRepo = require('coupon/repositories/coupon.repository');

//mail send 
const {
	join
} = require('path');
const ejs = require('ejs');
const {
	readFile
} = require('fs');
const {
	promisify
} = require('util');
const readFileAsync = promisify(readFile);
var moment = require('moment');

exports.placeOrder = async req => {
	try {
		let ship_address = {};
		let meals = [];
		let coupon = {};
		let newArr = [];
		let order_details = [];

		if (req.body.coupon_code) {
			let Coupon = await couponRepo.getByField({
				"coupon_code": req.body.coupon_code
			});
			coupon = {
				coupon_id: Coupon._id,
				coupon_code: Coupon.coupon_code,
				coupon_description: Coupon.coupon_description,
				amount: Coupon.amount,
				discount_type: Coupon.discount_type
			}
		}

		req.body.coupon = coupon;
		const uniqueOrdNo = await generateUniqueOrderNumber();
		req.body.order_no = uniqueOrdNo;
		let total_price = req.body.total_price;
		let user_id;

		ship_address.first_name = req.body.first_name;
		ship_address.last_name = req.body.last_name;
		ship_address.address = req.body.address;
		ship_address.apt_suite = req.body.apt_suite;
		ship_address.city = req.body.city;
		ship_address.state = req.body.state;
		ship_address.phone = req.body.phone;
		ship_address.company_name = req.body.company_name;
		req.body.shipping_address = ship_address;
		meals = req.body.order_details;
		let meal_id;
		let qty;
		let mealprice = 0.00;
		let toppingArr = [];
		let tid;
		let tprice = 0.00;
		let topArr = [];

		const setting_data = await settingRepo.getAllSetting();
		var settingObj = {};
		if (!_.isEmpty(setting_data)) {
			setting_data.forEach(function (element) {
				settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
			});
		}

		async.forEach(meals, function (item, cb) {
				meal_id = item.meal_id;
				qty = item.quantity;
				mealprice = item.meal_price;
				toppingArr = item.topping_details;
				async.forEach(toppingArr, function (element, callback) {
						tid = element.topping_id;
						tprice = element.price;
						topArr.push({
							"topping_id": tid,
							"price": tprice
						});
						callback();
					},
					function (e) {});
				newArr.push({
					"meal_id": meal_id,
					"quantity": qty,
					"meal_price": mealprice,
					"topping_details": item.topping_details
				});
				cb();
			},
			function (error) {
				req.body.order_details = newArr;
			});

		if (req.body.email != '' && req.body.email != null) {
			const roleDetails = await roleRepo.getByField({
				'role': 'user'
			});

			if (!_.isEmpty(roleDetails)) {
				const userDetails = await userRepo.getByField({
					'email': req.body.email,
					'isDeleted': false
				});
				if (_.isEmpty(userDetails) || _.isNull(userDetails)) {
					req.body.role = roleDetails._id;
					req.body.password = User.generateHash(req.body.password);
					req.body.isVerified = "Yes";
					/************************************************************/
					var card_number = req.body.card_number;
					var expiry_month = parseInt(req.body.exp_month);
					var expiry_year = parseInt(req.body.exp_year);
					var cvv = req.body.cvc;

					var stripeCardResponse = await stripe.tokens.create({
						card: {
							number: card_number,
							exp_month: expiry_month,
							exp_year: expiry_year,
							cvc: cvv,
						}
					});

					var stripe_token = stripeCardResponse.id;
					var customer = await stripe.customers.create({
						// source: stripe_token
						email: req.body.email,
					});

					var source = await stripe.customers.createSource(customer.id, {
						source: stripe_token,
					});

					const stripeCharge = await stripe.charges.create({
						amount: total_price * 100,
						currency: 'usd',
						description: 'order charge',
						customer: source.customer
					});

					req.body.stripe_charge_id = stripeCharge.id;
					req.body.customer_id = customer.id;
					req.body.isVerified = 'Yes';
					req.body.stripe_token = stripe_token;
					moment(req.body.deliver_arrive_on).toISOString();
					if (stripeCharge) {
						const saveUser = await userRepo.save(req.body);
						user_id = saveUser._id;
						req.body.user_id = user_id;

						//send email to user for profile
						let localsForUserProfileCreate = {
							response: saveUser,
						};

						let isMailSendForUserProfile = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, saveUser.email, "Welcome to " + settingObj.Site_Title, 'user-profile', localsForUserProfileCreate);

						let order = await orderRepo.save(req.body);
						const updateOrder = await orderRepo.updateById({
							'payment_status': 'Completed'
						}, order._id);
						let OrderedUserDetails = await userRepo.getById(order.user_id);
						if (!_.isEmpty(order) && !_.isNull(order)) {
							//send email to user
							let orderDetailsForMail = await orderRepo.orderViewDetail(order._id);

							let localsForUser = {
								response: orderDetailsForMail[0],
								logo_url: `${process.env.EMAIL_TEMPLATE_IMAGE_LINK}`
							};

							let isMailSendUser = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, OrderedUserDetails.email, 'Order Placed Successfully | Mass Meal', 'order-customer', localsForUser);

							let localsForAdmin = {
								response: orderDetailsForMail[0],
								logo_url: `${process.env.EMAIL_TEMPLATE_IMAGE_LINK}`
							};

							let isMailSendAdmin = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, settingObj.Site_Email, 'Order Placed Successfully | Mass Meal', 'order-admin', localsForAdmin);

							return {
								"status": 200,
								data: order,
								"message": "Order placed successfully."
							};
						} else {
							return {
								"status": 201,
								data: order,
								"message": "Payment Failed, We are unable to placed the Order, Please try again."
							};
						}
					} else {
						return {
							"status": 201,
							data: [],
							"message": "Sorry, Payment failed. Please try again."
						};
					}
				} else {
					return {
						"status": 201,
						data: [],
						"message": "Email already Exists. Please login or choose another email."
					}
				}
			}
		} else {
			let getUserDetails = await userRepo.getById(req.body.user_id);
			if (!_.isEmpty(getUserDetails) && !_.isNull(getUserDetails)) {
				var card_number = req.body.card_number;
				var expiry_month = parseInt(req.body.exp_month);
				var expiry_year = parseInt(req.body.exp_year);
				var cvv = req.body.cvc;
				var stripeCardResponse = await stripe.tokens.create({
					card: {
						number: card_number,
						exp_month: expiry_month,
						exp_year: expiry_year,
						cvc: cvv,
					}
				});
				if (stripeCardResponse) {
					var stripe_token = stripeCardResponse.id;
					let updateUserToken = await userRepo.updateById({
						'stripe_token': stripe_token
					}, getUserDetails._id);
				}

				if (getUserDetails.customer_id != '') {
					var source = await stripe.customers.createSource(getUserDetails.customer_id, {
						source: stripe_token,
					});
				}

				const stripeCharge = await stripe.charges.create({
					amount: total_price * 100,
					currency: 'usd',
					description: 'order charge',
					customer: source.customer
				});

				req.body.stripe_charge_id = stripeCharge.id;
				moment(req.body.deliver_arrive_on).toISOString();
				if (stripeCharge) {
					const updateUser = await userRepo.updateById({
						'isNewUser': false,
					}, getUserDetails._id);

					let order = await orderRepo.save(req.body);
					let orderUpdate = await orderRepo.updateById({
						'payment_status': 'Completed'
					}, order._id);
					let OrderedUserDetails = await userRepo.getById(order.user_id);
					if (!_.isEmpty(order) && !_.isNull(order)) {

						let orderDetailsForMail = await orderRepo.orderViewDetail(order._id);

						//send email to user
						let localsForUser = {
							response: orderDetailsForMail[0],
							logo_url: `${process.env.EMAIL_TEMPLATE_IMAGE_LINK}`
						};

						let isMailSendUser = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, OrderedUserDetails.email, 'Order Placed Successfully | Mass Meal', 'order-customer', localsForUser);

						//send email to admin
						let localsForAdmin = {
							response: orderDetailsForMail[0],
							logo_url: `${process.env.EMAIL_TEMPLATE_IMAGE_LINK}`
						};

						let isMailSendAdmin = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, settingObj.Site_Email, 'Order Placed Successfully | Mass Meal', 'order-admin', localsForAdmin);

						return {
							"status": 200,
							data: order,
							"message": "Order placed successfully."
						};
					} else {
						return {
							"status": 201,
							data: order,
							"message": "We are unable to placed the Order, Please try again."
						};
					}
				} else {
					return {
						"status": 201,
						data: [],
						"message": "Sorry, Payment failed. Please try again."
					};
				}
			} else {
				return {
					"status": 201,
					data: [],
					"message": "We are unable to find the User. Please login with valid email id."
				}
			}
		}
	} catch (error) {
		console.log('ererererererere', error);
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};

exports.orderHistory = async req => {
	try {
		const currentPage = req.query.page || 1;
		const {
			data,
			pageCount,
			totalCount
		} = await orderRepo.userOrderHistoryList(req.user._id, currentPage);
		if (!_.isEmpty(data) && !_.isNull(data)) {
			return {
				status: 200,
				data,
				current: parseInt(currentPage),
				pages: pageCount,
				total: totalCount,
				// pages: 24,
				message: 'Your orders fetched successfully.'
			}
		} else {
			return {
				status: 201,
				data: [],
				message: 'There are no orders at this moment.'
			}
		}
	} catch (error) {
		throw error;
	}
};

exports.orderHistoryDetails = async req => {
	try {
		const orderDetails = await orderRepo.userOrderHistoryDetails(req.params.id);
		if (!_.isEmpty(orderDetails) && !_.isNull(orderDetails)) {
			return {
				status: 200,
				data: orderDetails,
				message: 'Order details fetched successfully.'
			}
		} else {
			return {
				status: 201,
				data: [],
				message: 'There are no data at this moment.'
			}
		}
	} catch (error) {
		throw error;
	}
};

exports.orderCancel = async req => {
	try {
		const getOrderInfo = await orderRepo.getById(req.params.id);
		const getUserDetails = await userRepo.getById(getOrderInfo.user_id);
		const setting_data = await settingRepo.getAllSetting();
		var settingObj = {};
		if (!_.isEmpty(setting_data)) {
			setting_data.forEach(function (element) {
				settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
			});
		}
		if (!_.isEmpty(getOrderInfo) || !_.isNull(getOrderInfo)) {
			if (getOrderInfo.payment_status == 'Completed') {
				//If payment made for this order
				const refund = await stripe.refunds.create({
					charge: getOrderInfo.stripe_charge_id
				});

				if (!_.isEmpty(refund) && !_.isNull(refund)) {
					const refundStatusUpdate = await orderRepo.updateById({
						'is_refunded': true,
						'order_status': 'Cancel',
						'refund_details': refund
					}, getOrderInfo._id);

					let localsForUser = {
						response: getUserDetails,
					};

					let isMailSendUser = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, getUserDetails.email, 'Order Refund Process Initiated | Mass Meal', 'order-refund-user', localsForUser);

					let localsForAdmin = {
						user_info: getUserDetails,
						order_info: getOrderInfo
					};

					let isMailSendAdmin = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, settingObj.Site_Email, 'Order Refund Process Initiated | Mass Meal', 'order-refund-admin', localsForAdmin);

					return {
						status: 200,
						data: refund,
						message: 'Refund process initiated. Please wait sometime, you will receive an email from the Payment Gateway.'
					}
				} else {
					return {
						status: 201,
						data: [],
						message: 'Sorry, we are unable to initiate your Refund process. '
					}
				}
			} else {
				//If no payment made for this order
				const refundStatusUpdate = await orderRepo.updateById({
					'order_status': 'Cancel'
				}, getOrderInfo._id);

				let localsForUserOrder = {
					userInfo: getUserDetails,
					orderInfo: getOrderInfo
				};

				let isMailSendToUser = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, getUserDetails.email, 'Order Cancelled | Mass Meal', 'order-cancel-user', localsForUserOrder);
			}
		}
	} catch (error) {
		return {
			status: 500,
			data: [],
			message: error.message
		}
	}
};

generateUniqueOrderNumber = async req => {
	const obj = {
		'length': 10,
		'chars': '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
	};
	const uniqueNumber = randomString(obj);
	const orderDet = orderRepo.getByField({
		'order_no': uniqueNumber.toString()
	});
	if (_.isEmpty(orderDet)) {
		return uniqueNumber;
	} else {
		return generateUniqueOrderNumber();
	}
};

randomString = async req => {
	var result = '';
	for (var i = req.length; i > 0; --i)
		result += req.chars[Math.round(Math.random() * (req.chars.length - 1))];
	return result;
};