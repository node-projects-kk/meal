const couponRepo = require('coupon/repositories/coupon.repository');
const orderRepo = require('order_management/repositories/order.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const moment = require('moment'); 

class couponController {
  constructor() {}

  async applyCoupon(req){
      try {
          let Coupon = await couponRepo.getByField({"coupon_code":req.body.coupon_code});
          if(Coupon)
          {
              var dateOne=moment().format("YYYY-MM-DD");              
              if(dateOne<=Coupon.expiry_date)
              {
                  var orders = await orderRepo.getOrderByCoupon(Coupon._id);                  
                  if(Coupon.coupon_count>orders.length)
                  {
                      var discounted_price=0;
                      var total=req.body.order_total;
                      if(Coupon.discount_type=="flat")
                      {
                          discounted_price=total-Coupon.amount;
                      }
                      else
                      {
                          discounted_price=total-((total*Coupon.amount)/100);
                      }
                      var data={
                          total:total,
                          discounted_amount:discounted_price
                      }
                      
                      return { "status": 200, data: data, "message": "Coupon applied successfully." };
                  }
                  else
                  {
                      return { "status": 201, "message": "Coupon usage limit has been reached." };
                  }
              }
              else
              {
                  return { "status": 201, "message": "Coupon Expired." };
              }
          } else {
              return { "status": 201, "message": "Coupon not found." };
          } 
      } catch (error) {
          throw error;
      }
  }

  async getCoupon(req) {
    try {
      let Coupon = await couponRepo.getByField({"coupon_code":req.params.coupon_code});
      if(Coupon)
      {
        var dateOne=moment().format("YYYY-MM-DD");
        if(dateOne<=Coupon.expiry_date)
        {
            if(Coupon.coupon_count>0)
            {
                var discounted_price=0;
                var total=req.params.total;
                if(Coupon.discount_type=="flat")
                {
                    discounted_price=total-Coupon.amount;
                }
                else
                {
                    discounted_price=total-((total*Coupon.amount)/100);
                }
                var data={
                    total:total,
                    discounted_amount:discounted_price
                }
                return {
                    "status": 200,
                    data: data,
                    "message": "Coupon applied Successfully."
                  };
            }
            else
            {
                return {
                    "status": 201,
                    "message": "Coupon Usage maximum reached."
                  };
            }
        }
        else
        {
            return {
                "status": 201,
                "message": "Coupon Expired."
              };
        }
      } else {
        return {
          "status": 201,
          "message": "Coupon not found."
        };
      } 
    } catch (error) {
      console.log('controller error', error.message);
      throw error;
    }
  }



}

module.exports = new couponController();