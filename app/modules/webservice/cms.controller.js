const cmsRepo = require('cms/repositories/cms.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();


class cmsController {
  constructor() {}

  /* @Method: store cms
  // @Description: To store cms in  DB
  */
  async list(req, res) {
    try {
      let cms = await cmsRepo.getByField({
        "slug": req.params.slug
      });
      if (cms) {
        return {
          "status": 200,
          data: cms,
          "message": "Data Fetched Successfully."
        };
      } else {
        return {
          status: 201,
          data: {},
          message: `There are no data at this moment.`
        };
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  };

}

module.exports = new cmsController();