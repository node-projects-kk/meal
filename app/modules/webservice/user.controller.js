const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const zipRepo = require('zip_management/repositories/zip.repository');
const userModel = require('user/models/user.model.js');
const express = require('express');
const routeLabel = require('route-label');
const helper = require('../../helper/helper.js');
const mailer = require('../../helper/mailer.js');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");

//mail send 
const {
	join
} = require('path');
const ejs = require('ejs');
const {
	readFile
} = require('fs');
const {
	promisify
} = require('util');
const readFileAsync = promisify(readFile);

class userController {
	constructor() {}

	async checkZipCode(req) {
		try {
			let zipcode = req.body.zip;
			let email = req.body.email;
			let checkExist = await zipRepo.getByField({
				"title": parseInt(zipcode),
				"isDeleted": false,
				"status": "Active"
			});

			let emailExists = await userRepo.getByField({
				'email': email
			});

			if (!_.isEmpty(checkExist) && !_.isNull(checkExist)) {
				if (_.isEmpty(emailExists)) {
					return {
						"status": 200,
						data: checkExist,
						"message": "Zipcode fetched successfully."
					};
				} else {
					return {
						status: 201,
						data: [],
						message: 'User already exist, please try again.'
					}
				}
			} else {
				return {
					"status": 201,
					data: [],
					"message": "Sorry, we don't ship to your area yet, but we'll be in touch."
				};
			}
		} catch (error) {
			throw error;
		}
	}

	/* @Method: login
	// @Description: Vehicle Owner Login
	*/
	async login(req, res) {
		try {
			let userData = await userRepo.getUserByField({
				email: req.body.email,
				isDeleted: false
			});
			if (userData) {
				if (userData.role.role == 'user') {
					if (userData.isActive == true) {
						if (!userData.validPassword(req.body.password, userData.password)) {
							return {
								status: 201,
								data: [],
								message: 'Authentication failed, Wrong Password.'
							};
						} else {
							if (userData.isVerified == 'Yes') {
								const payload = {
									id: userData._id
								};
								const token = jwt.sign(payload, config.jwtSecret, {
									expiresIn: 86400
								});
								return {
									"status": 200,
									token: token,
									data: userData,
									"message": "You have successfully logged in."
								};
							} else {
								return {
									status: 201,
									data: userData,
									message: "Your Account is not verified."
								};
							}
						}
					} else {
						return {
							status: 201,
							data: {},
							message: "Your Account is not active."
						};
					}
				} else {
					return {
						status: 201,
						data: {},
						message: "You are not a valid user."
					};
				}
			} else {
				return {
					status: 201,
					data: {},
					message: "No user found."
				};
			}
		} catch (e) {
			return {
				status: 500,
				data: {},
				message: e.message
			};
		}
	}

	/* @Method: forgotPassword
	// @Description: To forgotPassword
	*/
	async forgotPassword(req, res) {
		try {
			let user = await userRepo.getByField({
				email: req.body.email
			});
			if (user) {
				let random_pass = Math.random().toString(36).substr(2, 9);
				const readable_pass = random_pass;
				random_pass = user.generateHash(random_pass);
				let locals = {
					firstName: user.first_name,
					password: readable_pass
				};
				let isMailSend = await mailer.sendMail(`Admin<${process.env.MAIL_USERNAME}>`, req.body.email, 'Reset Password | Mass Meal', 'forgot-password', locals);

				if (isMailSend) {
					let updatePassword = await userRepo.updateById({
						password: random_pass
					}, user._id);
					return {
						status: 200,
						data: {},
						"message": 'A email with new password has been sent to your email address.'
					};
				}
			} else {
				return {
					status: 201,
					data: {},
					message: "No user found."
				};
			}
		} catch (e) {
			return res.status(500).send({
				message: e.message
			});
		}
	};

	/* @Method: logout user
	// @Description: To logout user
	*/
	async logout(req, res) {
		try {
			let user = await userRepo.getById(req.params.id);
			if (user) {
				await userRepo.updateById({
					device_token: '',
					device_type: ''
				}, user._id);
				return {
					status: 200,
					data: {},
					"message": 'You have successfully logout.'
				};
			} else {
				return {
					status: 201,
					data: {},
					message: "No user found."
				};
			}
		} catch (e) {
			return {
				status: 500,
				data: {},
				message: e.message
			};
		}
	};

	/* @Method: removeProfile
	// @Description: To remove user profile
	*/
	async removeProfile(req, res) {
		try {
			let user = await userRepo.getById(req.user._id);
			if (user) {
				await userRepo.updateById({
					'isDeleted': true
				}, req.user._id);
				return {
					status: 200,
					data: {},
					"message": 'Your profile has been removed successfully.'
				};
			} else {
				return {
					status: 201,
					data: {},
					message: "No user found."
				};
			}
		} catch (e) {

			return {
				status: 500,
				data: {},
				message: e.message
			};
		}
	};

	/* @Method: changePassword
	// @Description: Change Password
	*/
	async changePassword(req, res) {
		try {
			let userData = await userRepo.getById(req.user._id);
			if (!_.isEmpty(userData) && userData.isActive == true && userData.isDeleted == false) {
				if (userData.validPassword(req.body.old_password, userData.password)) {
					let newPassword = userData.generateHash(req.body.new_password);
					let updatedUser = await userRepo.updateById({
						password: newPassword
					}, req.user._id);
					if (updatedUser) {
						return {
							status: 200,
							data: updatedUser,
							message: 'Password Changed Successfully.'
						}
					}
				} else {
					return {
						status: 201,
						data: [],
						message: 'You have entered Wrong Old Password.'
					};
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'User not Found.'
				};
			}
		} catch (e) {
			return res.status(500).send({
				message: e.message
			});
		}

	}

	async UpdateProfile(req, res) {
		try {
			let user_id = req.user._id;
			const userProfileUpdate = await userRepo.updateById(req.body, user_id);
			if (!_.isEmpty(userProfileUpdate) && !_.isNull(userProfileUpdate)) {
				return {
					status: 200,
					data: userProfileUpdate,
					message: 'Profile updated successfully.'
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'We are unable to update the profile, Please try again.'
				}
			}
		} catch (error) {
			throw error;
		}
	}

}

module.exports = new userController();