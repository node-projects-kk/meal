const contactRepo = require('contact/repositories/contact.repository');
const settingRepo = require('setting/repositories/setting.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


class contactController {
  constructor() {}


  async contactSubmit(req) {
    try {
      let Savecontact = await contactRepo.save(req.body);

      const setting_data = await settingRepo.getAllSetting();
      var settingObj = {};
      if (!_.isEmpty(setting_data)) {
        setting_data.forEach(function (element) {
          settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
        });
      }

      if (!_.isEmpty(Savecontact) && !_.isNull(Savecontact)) {
        //send mail to user
        var mailOptions = {
          from: settingObj.Site_Title + '<belasmith00@gmail.com>',
          to: Savecontact.email_id,
          subject: "Contact Us",
          html: 'Hello ' + Savecontact.name + ',<br><br>We have received your message. <br><br>Thank you.'
        };
        await transporter.sendMail(mailOptions);

        //send mail to admin
        var mailOptions = {
          from: settingObj.Site_Title + '<belasmith00@gmail.com>',
          to: settingObj.Site_Email,
          subject: "New Message Submitted",
          html: 'Hello ' + settingObj.Site_Email + '<br><br>Name: ' + Savecontact.name + '<br><br> Email: ' + Savecontact.email_id + '<br><br> Phone: ' + Savecontact.phone_number + '<br><br> Message: ' + Savecontact.message,
        };
        await transporter.sendMail(mailOptions);

        return {
          "status": 200,
          data: Savecontact,
          "message": "Thank you. Your message has been posted successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No record found."
        };
      }
    } catch (error) {
      // console.log('controller error', error.message);
      throw error;
    }
  }

  async contactInfo(req) {
    try {
      let getContactInfo = await settingRepo.getAllByField({
        'status': 'Active'
      });

      if (!_.isEmpty(getContactInfo) && !_.isNull(getContactInfo)) {
        return {
          "status": 200,
          data: getContactInfo,
          "message": "Contact information fetched successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No record found."
        };
      }
    } catch (error) {
      // console.log('controller error', error.message);
      throw error;
    }
  }


}

module.exports = new contactController();