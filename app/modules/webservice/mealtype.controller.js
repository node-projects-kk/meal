const mealtypeRepo = require('meal_types/repositories/meal_types.repository');
const mealRepo = require('meals/repositories/meals.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);


class mealTypeController {
  constructor() {}


  async mealTypes(req) {
    try {
      let getAllMealTypes = await mealtypeRepo.getAllByField({
        'status': 'Active',
        'isDeleted': false
      });

      if (!_.isEmpty(getAllMealTypes) && !_.isNull(getAllMealTypes)) {
        return {
          "status": 200,
          data: getAllMealTypes,
          "message": "Meal Types fetched successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No record found."
        };
      }
    } catch (error) {
      throw error;
    }
  }

  async getMealsOnType(req) {
    try {
      let getMeals = await mealRepo.getMealDetailsOnMealType(req.params.id);

      if (!_.isEmpty(getMeals) && !_.isNull(getMeals)) {
        return {
          "status": 200,
          data: getMeals,
          "message": "Meals fetched successfully."
        };
      } else {
        return {
          "status": 201,
          data: [],
          "message": "Sorry, No Data found."
        };
      }
    } catch (error) {
      throw error;
    }
  }





}

module.exports = new mealTypeController();