const packageTypeRepo = require('packageType/repositories/packageType.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class packageTypeController {

    /*
    // @Method: edit
    // @Description:  plan edit page
    */
    async edit(req, res) {
        try {
            let packageType = await packageTypeRepo.getById(req.params.id);
            
            if (!_.isEmpty(packageType)) {
                res.render('packageType/views/edit.ejs', {
                    page_name: 'packageType-management',
                    page_title: 'Update Package Type',
                    user: req.user,
                    response: packageType
                });
            } else {
                req.flash('error', "Sorry package type not found!");
                res.redirect(namedRouter.urlFor('packageType.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: plan update action
    */
    async update(req, res) {
        try {
            const packageTypeId = req.body.pid;
            let packageTypeUpdate = await packageTypeRepo.updateById(req.body,packageTypeId);
            if (packageTypeUpdate) {
                req.flash('success', "Package Type Updated Successfully");
                res.redirect(namedRouter.urlFor('packageType.listing'));
            } else {
                res.redirect(namedRouter.urlFor('packageType.edit', {
                    id: packageTypeId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('packageType.edit', {
                id: req.body.pid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the plan from DB
    */
    async list(req, res) {
        try {

            res.render('packageType/views/list.ejs', {
                page_name: 'packageType-management',
                page_title: 'Package Type List',
                user: req.user,
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the plan from DB
    */
    async getAll(req, res) {
        try {
            let packageType = await packageTypeRepo.getAll(req);

            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": packageType.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": packageType.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: packageType.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            
            throw e;
        }
    }

    /* @Method: delete
    // @Description: plan delete
    */
    async delete(req, res) {
        try {
            let planDelete = await planRepo.updateById({
                "isDeleted": true
            }, req.params.id)
            req.flash('success', 'Package Removed Successfully');
            res.redirect(namedRouter.urlFor('plan.listing'));
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


}

module.exports = new packageTypeController();