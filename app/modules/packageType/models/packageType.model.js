const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const packageTypeSchema = new Schema({
  title: {type: String,default: '',},
  description:{type: String,default:''},
  isDeleted: {type: Boolean,default: false,enum: [true, false]},
  createdAt: {type: Date,default: Date.now(),}
});

// For pagination
packageTypeSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('PackageType', packageTypeSchema);