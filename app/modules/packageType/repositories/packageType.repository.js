const mongoose = require('mongoose');
const PackageType = require('packageType/models/packageType.model');
const perPage = config.PAGINATION_PERPAGE;

const packageTypeRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'title': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'price': parseFloat(req.body.query.generalSearch)
                        },
                        {
                            'box': parseInt(req.body.query.generalSearch)
                        },
                        {
                            'tag': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = PackageType.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allPlans = await PackageType.aggregatePaginate(aggregate, options);
            return allPlans;
        } catch (e) {
            throw (e);
        }
    },

    getPlanCount: async (req) => {
        try {

            let plans = await Plans.find({
                isDeleted: false
            });
            return plans;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let packageType = await PackageType.findById(id).exec();
            return packageType;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let packageType = await PackageType.findOne(params).exec();
            return packageType;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let packageType = await PackageType.find(params).exec();
            return packageType;
        } catch (e) {
            throw (e);
        }
    },


    getAllPlan: async (params) => {
        try {
            var plist = await Plans.aggregate([{
                    $match: params
                },
                {
                    $sort: {
                        '_id': -1
                    }
                },
                {
                    $group: {
                        _id: "$title",
                        plan_details: {
                            $addToSet: "$$ROOT"
                        }
                    }
                }
            ]);
            return plist;
        } catch (e) {
            throw e;
        }
    },

    delete: async (id) => {
        try {
            let plans = await Plans.findById(id);
            if (plans) {
                let planDelete = await Plans.remove({
                    _id: id
                }).exec();
                return planDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
        //     console.log(data);
        // console.log('168',id);
            let packageType = await PackageType.findByIdAndUpdate(id, data, {
                new: true,
            }).exec();
            return packageType;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let packageType = await PackageType.create(data);
            if (!packageType) {
                return null;
            }
            return packageType;
        } catch (e) {
            throw e;
        }
    },

    // getToppingCountOnMeals: async (id) => {
    //     try {
    //         let toppingCountOfMeals = await Plans.find({
    //             'topping_id': id
    //         }).count();
    //         return toppingCountOfMeals;
    //     } catch (e) {
    //         throw (e);
    //     }
    // },

    // getToppingCount: async (req) => {
    //     try {
    //         let toppingCount = await Toppings.find({
    //             isDeleted: false
    //         });
    //         return toppingCount;
    //     } catch (e) {
    //         throw (e);
    //     }
    // },

};

module.exports = packageTypeRepository;