const mongoose = require('mongoose');
const Zips = require('zip_management/models/zip.model');
const Toppings = require('toppings/models/toppings.model');

const perPage = config.PAGINATION_PERPAGE;

const ZipRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                // and_clauses.push({
                //     'title': Number(req.body.query.generalSearch)
                // });
                and_clauses.push({
                    $or: [{
                        'title': {
                            $regex: req.body.query.generalSearch,
                            $options: 'i'
                        }
                    }]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Zips.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allZip = await Zips.aggregatePaginate(aggregate, options);
            return allZip;
        } catch (e) {
            throw (e);
        }
    },

    getZipCount: async (req) => {
        try {

            let zips = await Zips.find({
                isDeleted: false
            });
            return zips;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        try {
            let zips = await Zips.findById(id).exec();
            return zips;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let zips = await Zips.findOne(params).exec();
            return zips;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let zips = await Zips.find(params).exec();
            return zips;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let zips = await Zips.findById(id);

            if (zips) {
                let zipsDelete = await Zips.remove({
                    _id: id
                }).exec();
                return zipsDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let zips = await Zips.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return zips;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let zips = await Zips.create(data);
            if (!zips) {
                return null;
            }
            return zips;
        } catch (e) {
            throw e;
        }
    },

    // getMealsCountOnMealType: async (id) => {
    //     try {
    //         let mealCountOfMealTypes = await Meals.find({
    //             'meal_type': id
    //         }).count();
    //         return mealCountOfMealTypes;
    //     } catch (e) {
    //         throw (e);
    //     }
    // },

};

module.exports = ZipRepository;