const zipRepo = require('zip_management/repositories/zip.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');


class zipController {

    /* @Method: create
    // @Description: create meal type action
    */
    async create(req, res) {
        try {
            res.render('zip_management/views/add.ejs', {
                page_name: 'zip-management',
                page_title: 'Create New Zip',
                user: req.user
            });
        } catch (e) {
            throw (e);
        }
    };

    /* @Method: insert
    // @Description: save zip type action
    */
    async insert(req, res) {
        try {
            let SaveZip = await zipRepo.save(req.body);
            req.flash('success', 'Zip created succesfully.');
            res.redirect(namedRouter.urlFor('zip.listing'));
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            //res.status(500).send({message: error.message});
            res.redirect(namedRouter.urlFor('zip.create'));
        }
    };

    /*
    // @Method: edit
    // @Description:  zip edit page
    */
    async edit(req, res) {
        try {
            let zipValue = await zipRepo.getById(req.params.id);
            if (!_.isEmpty(zipValue)) {
                res.render('zip_management/views/edit.ejs', {
                    page_name: 'zip-management',
                    page_title: 'Update Zip Code',
                    user: req.user,
                    response: zipValue
                });
            } else {
                req.flash('error', "Sorry Zip Code not found!");
                res.redirect(namedRouter.urlFor('zip.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: update
    // @Description: zip update action
    */
    async update(req, res) {
        try {
            const ZipId = req.body.zid;
            let ZipUpdate = await zipRepo.updateById(req.body, ZipId);
            if (ZipUpdate) {
                req.flash('success', "Zip Updated Successfully");
                res.redirect(namedRouter.urlFor('zip.listing'));
            } else {
                res.redirect(namedRouter.urlFor('zip.edit', {
                    id: ZipId
                }));
            }
        } catch (e) {
            const error = errorHandler(e);
            req.flash('error', error.message);
            res.redirect(namedRouter.urlFor('zip.edit', {
                id: req.body.zid
            }));
        }

    };

    /* @Method: list
    // @Description: To list all the zip from DB
    */
    async list(req, res) {
        try {
            res.render('zip_management/views/list.ejs', {
                page_name: 'zip-management',
                page_title: 'Zip Lists',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

    /* @Method: getAll
    // @Description: To get all the zip from DB
    */
    async getAll(req, res) {
        try {
            let zipDetails = await zipRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": zipDetails.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": zipDetails.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            // console.log('zipDetails', zipDetails);
            return {
                status: 200,
                meta: meta,
                data: zipDetails.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /* @Method: delete
    // @Description: meal type delete
    */
    async delete(req, res) {
        try {
            let zipDelete = await zipRepo.updateById({
                "isDeleted": true
            }, req.params.id);
            if (zipDelete) {
                req.flash('success', 'Zip Removed Successfully');
                res.redirect(namedRouter.urlFor('zip.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };

}

module.exports = new zipController();