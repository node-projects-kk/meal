const mongoose = require('mongoose');
const About = require('about/models/about.model');
const perPage = config.PAGINATION_PERPAGE;

const AboutRepository = {
  getAll: async (req) => {
    // console.log(req.body);
    try {
      var conditions = {};
      var and_clauses = [];

      and_clauses.push({
        "isDeleted": false
      });

      if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
        //and_clauses.push({"status": /req.body.query.generalSearch/i});
        and_clauses.push({
          $or: [{
              'main_heading': {
                $regex: req.body.query.generalSearch,
                $options: 'i'
              }
            },
            {
              'block_title': {
                $regex: req.body.query.generalSearch,
                $options: 'i'
              }
            }
          ]
        });
      }
      if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
        and_clauses.push({
          "status": req.body.query.Status
        });
      }
      conditions['$and'] = and_clauses;

      var sortOperator = {
        "$sort": {}
      };
      if (_.has(req.body, 'sort')) {
        var sortField = req.body.sort.field;
        if (req.body.sort.sort == 'desc') {
          var sortOrder = -1;
        } else if (req.body.sort.sort == 'asc') {
          var sortOrder = 1;
        }

        sortOperator["$sort"][sortField] = sortOrder;
      } else {
        sortOperator["$sort"]['_id'] = -1;
      }

      var aggregate = About.aggregate([{
          $match: conditions
        },
        sortOperator
      ]);

      var options = {
        page: req.body.pagination.page,
        limit: req.body.pagination.perpage
      };
      let allAbout = await About.aggregatePaginate(aggregate, options);
      return allAbout;
    } catch (e) {
      throw (e);
    }
  },


  getById: async (id) => {
    try {
      let about = await About.findById(id).exec();
      return about;
    } catch (e) {
      throw (e);
    }
  },

  getByField: async (params) => {
    try {
      let about = await About.findOne(params).exec();
      return about;
    } catch (e) {
      throw (e);
    }
  },

  getAllByField: async (params) => {
    try {
      let about = await About.find(params).exec();
      return about;
    } catch (e) {
      throw (e);
    }
  },

  delete: async (id) => {
    try {
      let about = await About.findById(id);
      if (about) {
        let AboutDelete = await About.remove({
          _id: id
        }).exec();
        return AboutDelete;
      } else {
        return null;
      }
    } catch (e) {
      throw (e);
    }
  },

  deleteByField: async (field, fieldValue) => {
    //todo: Implement delete by field
  },


  updateById: async (data, id) => {
    try {
      let about = await About.findByIdAndUpdate(id, data, {
        new: true,
        upsert: true
      }).exec();
      return about;
    } catch (e) {
      throw (e);
    }
  },

  updateByField: async (field, fieldValue, data) => {
    //todo: update by field
  },

  save: async (data) => {
    try {
      let about = await About.create(data);
      if (!about) {
        return null;
      }
      return about;
    } catch (e) {
      throw e;
    }
  },

};

module.exports = AboutRepository;