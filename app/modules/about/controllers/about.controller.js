const aboutRepo = require('about/repositories/about.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');
var gm = require('gm').subClass({
  imageMagick: true
});


class aboutController {

  /* @Method: list
  // @Description: To list about from DB
  */
  async list(req, res) {
    try {
      res.render('about/views/list.ejs', {
        page_name: 'about-management',
        page_title: 'About Lists',
        user: req.user
      });
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  };


  /* @Method: getAll
  // @Description: To get about from DB
  */
  async getAll(req, res) {
    try {
      let aboutData = await aboutRepo.getAll(req);
      if (_.has(req.body, 'sort')) {
        var sortOrder = req.body.sort.sort;
        var sortField = req.body.sort.field;
      } else {
        var sortOrder = -1;
        var sortField = '_id';
      }
      let meta = {
        "page": req.body.pagination.page,
        "pages": aboutData.pageCount,
        "perpage": req.body.pagination.perpage,
        "total": aboutData.totalCount,
        "sort": sortOrder,
        "field": sortField
      };
      return {
        status: 200,
        meta: meta,
        data: aboutData.data,
        message: `Data fetched succesfully.`
      };
    } catch (e) {
      throw e;
    }
  }

  /*
  // @Method: edit
  // @Description: edit about page
  */
  async edit(req, res) {
    try {
      let result = {};
      let aboutData = await aboutRepo.getById(req.params.id);
      if (!_.isEmpty(aboutData)) {
        result.about_data = aboutData;
        res.render('about/views/edit.ejs', {
          page_name: 'about-management',
          page_title: 'Update About',
          user: req.user,
          response: result
        });
      } else {
        req.flash('error', "Sorry about page not found!");
        res.redirect(namedRouter.urlFor('about.listing'));
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  };


  /* @Method: update
  // @Description: update about page
  */
  async update(req, res) {
    try {
      if (typeof req.fileValidationError != 'undefined') {
        req.flash('error', req.fileValidationError);
        res.redirect(namedRouter.urlFor('about.edit', {
          id: req.body.aid
        }));
      } else {
        if (_.has(req, 'files')) {
          if (req.files.length > 0) {
            let about = await aboutRepo.getByField({
              '_id': req.body.aid
            });
            if (!_.isEmpty(about.block_right_image)) {
              if (fs.existsSync('public/uploads/about/' + about.block_right_image)) {
                const upl_img = fs.unlinkSync('public/uploads/about/' + about.block_right_image);
              }
              if (fs.existsSync('public/uploads/about/thumb/' + about.block_right_image)) {
                const upl_thumb_img = fs.unlinkSync('public/uploads/about/thumb/' + about.block_right_image);
              }
            }
            if (req.files[0].fieldname == 'block_right_image') {
              gm('public/uploads/about/' + req.files[0].filename)
                .resize(100)
                .write('public/uploads/about/thumb/' + req.files[0].filename, function (err) {
                  if (err) {
                    req.flash('error', err.message);
                    res.redirect(namedRouter.urlFor('about.edit', {
                      id: req.body.aid
                    }));
                  }
                });
              req.body.block_right_image = req.files[0].filename;
            }
          }
        }
        let aboutUpdate = aboutRepo.updateById(req.body, req.body.aid);
        if (aboutUpdate) {
          req.flash('success', "About page updated succesfully.");
          res.redirect(namedRouter.urlFor('about.listing'));
        } else {
          res.redirect(namedRouter.urlFor('about.edit', {
            id: req.body.aid
          }));
        }
      }
    } catch (e) {
      return res.status(500).send({
        message: e.message
      });
    }
  };
}

module.exports = new aboutController();