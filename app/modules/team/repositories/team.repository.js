const mongoose = require('mongoose');
const Team = require('team/models/team.model');
const perPage = config.PAGINATION_PERPAGE;

const TeamRepository = {
    getAll: async (req) => {
        // console.log(req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                and_clauses.push({
                    $or: [{
                            'name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'designation': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = Team.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allTeam = await Team.aggregatePaginate(aggregate, options);
            return allTeam;
        } catch (e) {
            throw (e);
        }
    },


    getById: async (id) => {
        try {
            let team = await Team.findById(id).exec();
            return team;
        } catch (e) {
            throw (e);
        }
    },

    getByField: async (params) => {
        try {
            let team = await Team.findOne(params).exec();
            return team;
        } catch (e) {
            throw (e);
        }
    },

    getAllByField: async (params) => {
        try {
            let team = await Team.find(params).exec();
            return team;
        } catch (e) {
            throw (e);
        }
    },

    delete: async (id) => {
        try {
            let team = await Team.findById(id);
            if (team) {
                let TeamDelete = await Team.remove({
                    _id: id
                }).exec();
                return TeamDelete;
            } else {
                return null;
            }
        } catch (e) {
            throw (e);
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let team = await Team.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            return team;
        } catch (e) {
            throw (e);
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let team = await Team.create(data);
            if (!team) {
                return null;
            }
            return team;
        } catch (e) {
            throw e;
        }
    },

};

module.exports = TeamRepository;