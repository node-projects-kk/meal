const teamRepo = require('team/repositories/team.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
const errorHandler = require('../../../errorHandler');
var gm = require('gm').subClass({
    imageMagick: true
});

class teamController {

    /* @Method: list
    // @Description: To list all the team member from DB
    */
    async list(req, res) {
        try {
            res.render('team/views/list.ejs', {
                page_name: 'team-management',
                page_title: 'Team Lists',
                user: req.user
            });
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /* @Method: getAll
    // @Description: To get all the team member from DB
    */
    async getAll(req, res) {
        try {
            let teamData = await teamRepo.getAll(req);
            if (_.has(req.body, 'sort')) {
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            } else {
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {
                "page": req.body.pagination.page,
                "pages": teamData.pageCount,
                "perpage": req.body.pagination.perpage,
                "total": teamData.totalCount,
                "sort": sortOrder,
                "field": sortField
            };
            return {
                status: 200,
                meta: meta,
                data: teamData.data,
                message: `Data fetched succesfully.`
            };
        } catch (e) {
            throw e;
        }
    }

    /*
    // @Method: edit
    // @Description:  team member edit page
    */
    async edit(req, res) {
        try {
            let result = {};
            let teamData = await teamRepo.getById(req.params.id);
            if (!_.isEmpty(teamData)) {
                result.team_data = teamData;
                res.render('team/views/edit.ejs', {
                    page_name: 'team-management',
                    page_title: 'Update Team Member',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry team details not found!");
                res.redirect(namedRouter.urlFor('team.listing'));
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };


    /* @Method: update
    // @Description: team member update action
    */
    async update(req, res) {
        try {
            if (typeof req.fileValidationError != 'undefined') {
                req.flash('error', req.fileValidationError);
                res.redirect(namedRouter.urlFor('team.edit', {
                    id: req.body.tid
                }));
            } else {
                if (_.has(req, 'files')) {
                    if (req.files.length > 0) {
                        let team = await teamRepo.getByField({
                            '_id': req.body.tid
                        });
                        if (!_.isEmpty(team.team_image)) {
                            if (fs.existsSync('public/uploads/team/' + team.team_image)) {
                                const upl_img = fs.unlinkSync('public/uploads/team/' + team.team_image);
                            }
                            if (fs.existsSync('public/uploads/team/thumb/' + team.team_image)) {
                                const upl_thumb_img = fs.unlinkSync('public/uploads/team/thumb/' + team.team_image);
                            }
                        }
                        if (req.files[0].fieldname == 'team_image') {
                            gm('public/uploads/team/' + req.files[0].filename)
                                .resize(100)
                                .write('public/uploads/team/thumb/' + req.files[0].filename, function (err) {
                                    if (err) {
                                        req.flash('error', err.message);
                                        res.redirect(namedRouter.urlFor('team.edit', {
                                            id: req.body.tid
                                        }));
                                    }
                                });
                            req.body.team_image = req.files[0].filename;
                        }
                    }
                }
                let teamUpdate = teamRepo.updateById(req.body, req.body.tid);
                if (teamUpdate) {
                    req.flash('success', "Team updated succesfully.");
                    res.redirect(namedRouter.urlFor('team.listing'));
                } else {
                    res.redirect(namedRouter.urlFor('team.edit', {
                        id: req.body.tid
                    }));
                }
            }
        } catch (e) {
            return res.status(500).send({
                message: e.message
            });
        }
    };
}

module.exports = new teamController();