const mongoose = require('mongoose');
const Cms = require('cms/models/cms.model');
const role = require('role/models/role.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;

const cmsRepository = {

    getAll: async (req) => {
        // console.log("REPO",req.body);
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({ "isDeleted": false });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({"status": /req.body.query.generalSearch/i});
                // and_clauses.push({ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'slug': { $regex: req.body.query.generalSearch, $options: 'i' } });
                // and_clauses.push({ 'desc': { $regex: req.body.query.generalSearch, $options: 'i' } });
                and_clauses.push({
                    $or: [
                        { 'title': { $regex: req.body.query.generalSearch, $options: 'i' } },
                        { 'slug': { $regex: req.body.query.generalSearch, $options: 'i' } },
                        { 'desc': { $regex: req.body.query.generalSearch, $options: 'i' } }
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({ "status": req.body.query.Status });
            }
            conditions['$and'] = and_clauses;
            console.log(JSON.stringify(conditions))

            var sortOperator = { "$sort": {} };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate =  Cms.aggregate([
                { $match: conditions },
                sortOperator
            ]);
            // console.log("AGGR", aggregate)
            var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
            let allCms = await Cms.aggregatePaginate(aggregate, options);
            // console.log("AGGR",allCms)
            return allCms;
        } catch (e) {
            throw (e);
        }
    },


    // getAll: async (searchQuery,user, sortOrder, page) => {
    //    try {
    //     let query = [{}];
    //     // serach by keyword //

    //     if (_.has(searchQuery, "keyword")) {
    //         if (searchQuery.keyword != '') {
    //             let search=searchQuery.keyword.trim();
    //             query.push({
    //                 "$or": [
    //                 { 'title': { '$regex': search, '$options': 'i' } },

    //                 { 'desc': { '$regex': search, '$options': 'i' } }]
    //             });

    //         }

    //     }

    //     // serach by role //

    //     let searchBy = { "$and": query };

    //     const aggregate = Cms.aggregate([



    //         { "$sort": sortOrder },
    //         {
    //             $project: {
    //             _id:"$_id",
    // 			title:"$title",
    //             desc:"$desc",
    // 			status: "$status",
    //             }
    //         },
    //         { $match: searchBy },


    //     ])


    //     let options = { page: page, limit: perPage }

    //     let cms_all = await Cms.aggregatePaginate(aggregate, options)


    //     if (!cms_all) {
    //         return null;
    //     } else {

    //         return cms_all;
    //     }
    //  }catch(e){
    //      throw e;
    //  }


    // },

    getById: async (id) => {
        let cms = await Cms.findById(id).exec();
        try {
            if (!cms) {
                return null;
            }
            return cms;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let cms = await Cms.findOne(params).exec();
        try {
            if (!cms) {
                return null;
            }
            return cms;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let user = await User.find(params).exec();
        try {
            if (!user) {
                return null;
            }
            return user;

        } catch (e) {
            return e;
        }
    },

    getCmsCount: async (params) => {
        try {
            let cmsCount = await Cms.countDocuments(params);
            if (!cmsCount) {
                return null;
            }
            return cmsCount;
        } catch (e) {
            return e;
        }

    },




    delete: async (id) => {
        try {
            let cms = await Cms.findById(id);
            if (cms) {
                let cmsDelete = await Cms.remove({ _id: id }).exec();
                if (!cmsDelete) {
                    return null;
                }
                return cmsDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let cms = await Cms.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            if (!cms) {
                return null;
            }
            return cms;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },






};

module.exports = cmsRepository;