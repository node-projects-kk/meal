const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const status = ["Active", "Inactive"];

const CmsSchema = new Schema({
  title: {
    type: String,
    default: ''
  },
  slug: {
    type: String,
    default: ''
  },
  content: {
    type: String,
    default: ''
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: [true, false]
  },
  status: {
    type: String,
    default: "Active",
    enum: status
  },
});
CmsSchema.plugin(beautifyUnique);
// For pagination
CmsSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Cms', CmsSchema);