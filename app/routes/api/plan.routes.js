const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const planController = require('webservice/plan.controller');
const request_param = multer();

/**
 * @api {get} /plan/list Plan List
 * @apiVersion 1.0.0
  * @apiGroup Plan
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d8dd1b47cd8672d9a104338",
            "title": "Lean",
            "description": "Package 4oz Protein Meals",
            "isDeleted": false,
            "createdAt": "2019-09-27T09:08:38.166Z",
            "__v": 0,
            "package_details": [
                {
                    "_id": "5d91acc32773de1afb520cc6",
                    "box": 12,
                    "price": "100",
                    "tag": "MostPopular",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-09-30T07:19:29.303Z",
                    "packageType_id": "5d8dd1b47cd8672d9a104338",
                    "__v": 0
                },
                ...
            ]
        },
        {
            "_id": "5d8dd2a4ff64f15dffefe1f7",
            "title": "Athletic",
            "description": "Package 6oz Protein Meals",
            "isDeleted": false,
            "createdAt": "2019-09-27T09:08:38.166Z",
            "__v": 0,
            "package_details": [
                {
                    "_id": "5d91b7f47fd5eb97ce9cb6ca",
                    "box": 20,
                    "price": "200",
                    "tag": "MostPopular",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-09-30T07:19:29.303Z",
                    "packageType_id": "5d8dd2a4ff64f15dffefe1f7",
                    "__v": 0
                },
                ...
            ]
        },
        {
            "_id": "5d8dd2a4ff64f15dffefe1f9",
            "title": "Bulk",
            "description": "Package 8ozProtein Mealss1",
            "isDeleted": false,
            "createdAt": "2019-09-27T09:08:38.166Z",
            "__v": 0,
            "package_details": [
                {
                    "_id": "5d91b82c7fd5eb97ce9cb806",
                    "box": 25,
                    "price": "250",
                    "tag": "MostPopular",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-09-30T07:19:29.303Z",
                    "packageType_id": "5d8dd2a4ff64f15dffefe1f9",
                    "__v": 0
                },
                ...
            ]
        }
    ],
    "message": "Plan fetched successfully."
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/

namedRouter.get("api.plan.list", '/plan/list', request_param.any(), async (req, res) => {
    try {
        const success = await planController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


// Export the express.Router() instance
module.exports = router;