const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const stateController = require('webservice/states.controller');
const request_param = multer();

/**
 * @api {get} /state/list State List
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "name": "Alabama",
            "short_name": "AL",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be392",
            "__v": 0
        },
        {
            "name": "Alaska",
            "short_name": "AK",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be394",
            "__v": 0
        },
        {
            "name": "Arizona",
            "short_name": "AZ",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be396",
            "__v": 0
        },
        {
            "name": "Arkansas",
            "short_name": "AR",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be398",
            "__v": 0
        },
        {
            "name": "California",
            "short_name": "CA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be39a",
            "__v": 0
        },
        {
            "name": "Colorado",
            "short_name": "CO",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be39c",
            "__v": 0
        },
        {
            "name": "Connecticut",
            "short_name": "CT",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be39e",
            "__v": 0
        },
        {
            "name": "Delaware",
            "short_name": "DE",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3a0",
            "__v": 0
        },
        {
            "name": "Florida",
            "short_name": "FL",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3a2",
            "__v": 0
        },
        {
            "name": "Georgia",
            "short_name": "GA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3a4",
            "__v": 0
        },
        {
            "name": "Hawaii",
            "short_name": "HI",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3a6",
            "__v": 0
        },
        {
            "name": "Idaho",
            "short_name": "ID",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3a8",
            "__v": 0
        },
        {
            "name": "Illinois",
            "short_name": "IL",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3aa",
            "__v": 0
        },
        {
            "name": "Indiana",
            "short_name": "IN",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ac",
            "__v": 0
        },
        {
            "name": "Iowa",
            "short_name": "IA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ae",
            "__v": 0
        },
        {
            "name": "Kansas",
            "short_name": "KS",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3b0",
            "__v": 0
        },
        {
            "name": "Kentucky",
            "short_name": "KY",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3b2",
            "__v": 0
        },
        {
            "name": "Louisiana",
            "short_name": "LA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3b4",
            "__v": 0
        },
        {
            "name": "Maine",
            "short_name": "ME",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3b6",
            "__v": 0
        },
        {
            "name": "Maryland",
            "short_name": "MD",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3b8",
            "__v": 0
        },
        {
            "name": "Massachusetts",
            "short_name": "MA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ba",
            "__v": 0
        },
        {
            "name": "Michigan",
            "short_name": "MI",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3bc",
            "__v": 0
        },
        {
            "name": "Minnesota",
            "short_name": "MN",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3be",
            "__v": 0
        },
        {
            "name": "Mississippi",
            "short_name": "MS",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3c0",
            "__v": 0
        },
        {
            "name": "Missouri",
            "short_name": "MO",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3c2",
            "__v": 0
        },
        {
            "name": "Montana",
            "short_name": "MT",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3c4",
            "__v": 0
        },
        {
            "name": "Nebraska",
            "short_name": "NE",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3c6",
            "__v": 0
        },
        {
            "name": "Nevada",
            "short_name": "NV",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3c8",
            "__v": 0
        },
        {
            "name": "New Hampshire",
            "short_name": "NH",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ca",
            "__v": 0
        },
        {
            "name": "New Jersey",
            "short_name": "NJ",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3cc",
            "__v": 0
        },
        {
            "name": "New Mexico",
            "short_name": "NM",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ce",
            "__v": 0
        },
        {
            "name": "New York",
            "short_name": "NY",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3d0",
            "__v": 0
        },
        {
            "name": "North Carolina",
            "short_name": "NC",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3d2",
            "__v": 0
        },
        {
            "name": "North Dakota",
            "short_name": "ND",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3d5",
            "__v": 0
        },
        {
            "name": "Ohio",
            "short_name": "OH",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3d7",
            "__v": 0
        },
        {
            "name": "Oklahoma",
            "short_name": "OK",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3d9",
            "__v": 0
        },
        {
            "name": "Oregon",
            "short_name": "OR",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3db",
            "__v": 0
        },
        {
            "name": "Pennsylvania",
            "short_name": "PA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3dd",
            "__v": 0
        },
        {
            "name": "Rhode Island",
            "short_name": "RI",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3df",
            "__v": 0
        },
        {
            "name": "South Carolina",
            "short_name": "SC",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3e1",
            "__v": 0
        },
        {
            "name": "South Dakota",
            "short_name": "SD",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3e3",
            "__v": 0
        },
        {
            "name": "Tennessee",
            "short_name": "TN",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3e5",
            "__v": 0
        },
        {
            "name": "Texas",
            "short_name": "TX",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3e7",
            "__v": 0
        },
        {
            "name": "Utah",
            "short_name": "UT",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ea",
            "__v": 0
        },
        {
            "name": "Vermont",
            "short_name": "VT",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ec",
            "__v": 0
        },
        {
            "name": "Virginia",
            "short_name": "VA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3ee",
            "__v": 0
        },
        {
            "name": "Washington",
            "short_name": "WA",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3f0",
            "__v": 0
        },
        {
            "name": "West Virginia",
            "short_name": "WV",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3f2",
            "__v": 0
        },
        {
            "name": "Wisconsin",
            "short_name": "WI",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3f4",
            "__v": 0
        },
        {
            "name": "Wyoming",
            "short_name": "WY",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-25T11:37:35.847Z",
            "_id": "5d0a3c79c1891efa1a6be3f6",
            "__v": 0
        }
    ],
    "message": "States fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/

namedRouter.get("api.state.list", '/state/list', request_param.any(), async (req, res) => {
    try {
        const success = await stateController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


// Export the express.Router() instance
module.exports = router;