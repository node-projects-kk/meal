const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const mealTypeController = require('webservice/mealtype.controller');
const request_param = multer();


/**
 * @api {get} /meal/types  All Meal Types
 * @apiVersion 1.0.0
 * @apiGroup Meal Types
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "title": "Breakfast",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-10T17:39:03.380Z",
            "_id": "5cfe44f5c1891efa1a9123d3",
            "__v": 0
        },
        {
            "title": "Dinner",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-10T12:20:49.646Z",
            "_id": "5cfe4b5b43886608752e09e9",
            "__v": 0
        },
        {
            "title": "Lunch",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-14T09:31:26.317Z",
            "_id": "5d036aadb407ab5b64ac40ac",
            "__v": 0
        }
    ],
    "message": "Meal Types fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.meal.types", '/meal/types', request_param.any(), async (req, res) => {
    try {
        const success = await mealTypeController.mealTypes(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
})

/**
 * @api {get} /mealtype/:id  Get Meals Based on Type
 * @apiVersion 1.0.0
 * @apiParam {string}  id Pass Meal Type Id
 * @apiGroup Meal Types
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d13664a3050bb0eb3b89853",
            "title": "Sample Title",
            "meal_images": [
                "meal_images_1561552458352_new_dummy.jpeg",
                "meal_images_1561552458502_new1.jpg",
                "meal_images_1561552458504_test.jpg"
            ],
            "content": "<p>Brief Description</p>\r\n",
            "contains_text": [
                "asd",
                "qwerty"
            ],
            "detail_blocks": [
                {
                    "heading": "h1",
                    "desc": "<p>c1</p>\r\n",
                    "_id": "5d13664a3050bb0eb3b89855"
                },
                {
                    "heading": "h2",
                    "desc": "c2",
                    "_id": "5d13664a3050bb0eb3b89854"
                }
            ],
            "price": "10",
            "taste_text": "Mouth Watering",
            "block_image": "block_image_1561552458508_autumn.jpg",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-26T12:30:08.314Z",
            "mealtype_details": {
                "_id": "5cfe44f5c1891efa1a9123d3",
                "title": "Breakfast",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-10T17:39:03.380Z",
                "__v": 0
            },
            "topping_details": [
                {
                    "_id": "5d03708db407ab5b64ac40b2",
                    "title": "Sauce",
                    "price": 10.5,
                    "slug": "",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-14T09:31:26.292Z",
                    "__v": 0
                },
                {
                    "_id": "5cff84c311cb740c27fec5d8",
                    "title": "Extra Dal",
                    "price": 11,
                    "slug": "",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-11T10:38:00.701Z",
                    "__v": 0
                }
            ],
            "category_details": {
                "_id": "5d039ffec1891efa1ae990e9",
                "title": "Soups",
                "slug": "soups",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "like_details": [
                {
                    "_id": "5d03ceedd937b53bcc1dc7ac",
                    "title": "Sample Title",
                    "like_icon": "like_icon_1560530668029_dummy1.png",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-14T16:42:33.093Z",
                    "__v": 0
                }
            ],
            "key_details": [
                {
                    "_id": "5d0891c35db2832b613282d6",
                    "title": "test-title",
                    "key_icon": "key_icon_1560842690764_blog1.jpeg",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-18T06:53:32.725Z",
                    "__v": 0
                }
            ],
            "dietary_details": [
                {
                    "_id": "5d03c6ae74e2d71f1b14848d",
                    "title": "Test Title lk",
                    "dietary_icon": "dietary_icon_1560528558138_dummy1.png",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-14T16:01:27.342Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Meals fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.mealtype.id", '/mealtype/:id', request_param.any(), async (req, res) => {
    try {
        const success = await mealTypeController.getMealsOnType(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
})

// Export the express.Router() instance
module.exports = router;