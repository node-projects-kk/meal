const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const contactController = require('webservice/contact.controller');
const request_param = multer();

/**
 * @api {post} /contact/submit Contact Submit
 * @apiVersion 1.0.0
 * @apiGroup Contact
 * @apiParam {String} name Full Name
 * @apiParam {String} phone_number Phone Number
 * @apiParam {String} email_id Email Id
 * @apiParam {String} message Message
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "name": "Subhra Bhattacharya",
        "phone_number": "7896541230",
        "email_id": "subhra.bhattacharya@webskitters.com",
        "message": "Hello ! This is for test purpose.",
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-07-16T13:04:54.782Z",
        "_id": "5d2dcc184126d914d947621b",
        "__v": 0
    },
    "message": "Thank you. Your message has been posted successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.post("api.contact.submit", '/contact/submit', request_param.any(), async (req, res) => {
    try {
        const success = await contactController.contactSubmit(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /contact/info Get Contact Info
 * @apiVersion 1.0.0
 * @apiGroup Contact
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "setting_name": "Site Title",
            "setting_slug": "site-title",
            "setting_value": "Mass Meal",
            "status": "Active",
            "isDeleted": false,
            "_id": "5cffb9e3c1891efa1aa89096"
        },
        {
            "setting_name": "Location",
            "setting_slug": "location",
            "setting_value": "20936 Mission Blvd, Hayward, CA 94541, USA",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d2dd25a822fa522c77d19cc"
        },
        {
            "setting_name": "Phone",
            "setting_slug": "phone",
            "setting_value": "+1 510-398-8997",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d2dd2bb822fa522c77d1d8e"
        },
        {
            "setting_name": "Facebook",
            "setting_slug": "facebook",
            "setting_value": "https//:www.facebook.com",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d2dd2fd822fa522c77d206a"
        },
        {
            "setting_name": "Twitter",
            "setting_slug": "twitter",
            "setting_value": "https//:www.twitter.com",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d2dd312822fa522c77d217d"
        },
        {
            "setting_name": "Instagram",
            "setting_slug": "instagram",
            "setting_value": "https//:www.instagram.com",
            "status": "Active",
            "isDeleted": false,
            "_id": "5d2dd323822fa522c77d222d"
        }
    ],
    "message": "Contact information fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.contact.info", '/contact/info', request_param.any(), async (req, res) => {
    try {
        const success = await contactController.contactInfo(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


// Export the express.Router() instance
module.exports = router;