const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const couponController = require('webservice/coupon.controller');
const request_param = multer();

/**
 * @api {get} /coupon/:coupon_code/:total Get Coupon
 * @apiVersion 1.0.0
 * @apiGroup Coupon
 * @apiParam {String} coupon_code Coupon Code
 * @apiParam {Float} total Total Price
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "name": "Subhra Bhattacharya",
        "phone_number": "7896541230",
        "email_id": "subhra.bhattacharya@webskitters.com",
        "message": "Hello ! This is for test purpose.",
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-07-16T13:04:54.782Z",
        "_id": "5d2dcc184126d914d947621b",
        "__v": 0
    },
    "message": "Thank you. Your message has been posted successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.coupon", '/coupon/:coupon_code/:total', request_param.any(), async (req, res) => {
    try {
        const success = await couponController.getCoupon(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /coupon/apply Apply Coupon
 * @apiVersion 1.0.0
 * @apiGroup Coupon
 * @apiParam {String} coupon_code Coupon Code
 * @apiParam {Float} order_total Total Order Price
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "total": "1000",
        "discounted_amount": 875
    },
    "message": "Coupon applied successfully."
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "message": "Coupon usage limit has been reached."
}
*/
namedRouter.post("api.coupon.apply", '/coupon/apply', request_param.any(), async (req, res) => {
    try {
        const success = await couponController.applyCoupon(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});




// Export the express.Router() instance
module.exports = router;