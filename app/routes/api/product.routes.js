const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const productController = require('webservice/product.controller');
const request_param = multer();

/**
 * @api {post} /product/list Product List
 * @apiVersion 1.0.0
 * @apiGroup Product
 * @apiParam {String} category Search by Category Id
 * @apiParam {Array} like Search by Likes
 * @apiParam {Array} dislike Search by Dislike
 * @apiParam {Array} benefit Search by Benefits
 * @apiparam {Array} dietary Search by Dietary
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d5a9168de10d058d2f5cc03",
            "title": "mojiti",
            "meal_images": [
                "meal_images_1566216552371_mojito-cocktails.jpg"
            ],
            "content": "<p>superb</p>\r\n",
            "contains_text": [
                "all good and healthy ingredents"
            ],
            "detail_blocks": [
                {
                    "heading": "mojito1",
                    "desc": "<p>yummy</p>\r\n",
                    "_id": "5d5a9168de10d058d2f5cc04"
                }
            ],
            "price": "12",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-08-19T09:06:41.616Z",
            "mealtype_details": {
                "_id": "5d3ee5ea2bcc0d074c675c76",
                "title": "No Carb",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [
                {
                    "_id": "5d00f5f0b89eb6111e86261a",
                    "title": "Test Title123",
                    "price": "5.50",
                    "slug": "",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-12T12:33:20.607Z",
                    "__v": 0
                }
            ],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [
                {
                    "_id": "5d15d612f03a11075ef76756",
                    "title": "Test Title",
                    "key_icon": "key_icon_1561712146459_dummy1.png",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-28T08:43:06.435Z",
                    "__v": 0
                }
            ],
            "like_details": [
                {
                    "_id": "5d03ceedd937b53bcc1dc7ac",
                    "title": "Sample Title",
                    "like_icon": "like_icon_1560530668029_dummy1.png",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-14T16:42:33.093Z",
                    "__v": 0
                }
            ],
            "dietary_details": [
                {
                    "_id": "5d03c6ae74e2d71f1b14848d",
                    "title": "Test Title lk",
                    "dietary_icon": "dietary_icon_1560528558138_dummy1.png",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-14T16:01:27.342Z",
                    "__v": 0
                }
            ],
            "review_details": []
        },
        {
            "_id": "5d53bb7f0325346e8aaf0e47",
            "title": "Chicken Thigh with Bacon wrapped Asparagus",
            "meal_images": [
                "meal_images_1565768575522_IMG_1433.jpg"
            ],
            "content": "",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Asparagus<strong>, </strong>Chicken Thigh, Turkey Bacon (Mechanically Separated Turkey, Turkey, Turkey Water, Sugar, Contains 2% or Less Salt, Potassium Lactate, Natural Smoke Flavor, Flavor(Canola Oil, Natural Smoke, Natural Flavouring), Sodium Diacetate, Sodium Phosphate, Rosemary Extract, Sodium Erythorbate, Sodium Nitrite).</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g), Calories per serving: 330. Total Fat 17g &ndash; 22%, Sat. Fat 5g &ndash; 25%, Trans Fat 0g, Cholest. 180Mg &ndash; 60%,&nbsp;Sodium 780mg - 34% . Vitamin D 0.3mcg - 2%, Calcium 40mg - 2%. Total Carb 6g - 2%, Fiber 2g - 7%, Total Sugars 1g, Incl. 0 g added Sugars - 0%, Protein 39g.&nbsp;Iron 2.9mg - 15% . Potassium 670mg - 15%.</p>\r\n",
                    "_id": "5d53bb7f0325346e8aaf0e48"
                }
            ],
            "price": "30",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-08-14T07:25:55.233Z",
            "mealtype_details": {
                "_id": "5d3ee59f2bcc0d074c675c70",
                "title": "Chicken Meals",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3ef0792bcc0d074c675c9b",
            "title": "Tri Tip with Mixed Veggies",
            "meal_images": [
                "meal_images_1564405881505_tri_tip.jpg",
                "meal_images_1564405881524_tri-tip-roast.jpg"
            ],
            "content": "",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Sri Tip, Cauliflower, Cheddar Cheese, Turkey Bacon(Mechanically Separated Turkey, Turkey, Turkey Water, Sugar, Contains 2% or Less Salt, Potassium Lactate, Natural Smoke Flavor, Flavor(Canola Oil, Natural Smoke, Natural Flavouring), Sodium Diacetate, Sodium Phosphate, Rosemary Extract, Sodium Erythorbate, Sodium Nitrite)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g), Calories per serving: 340. Total Fat 18g &ndash; 23%, Sat. Fat 7g &ndash; 35%, Trans Fat 0g, Cholest. 115Mg &ndash; 38%,&nbsp;Sodium 490mg - 21% . Vitamin D 0.1mcg - 0%, Calcium 140mg - 10%. Total Carb 6g - 2%, Fiber 3g - 11%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 40g. Iron 2.7mg - 15% . Potassium 590mg - 15%.</p>\r\n",
                    "_id": "5d512ce1ef08b20e4944d8dd"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5ea2bcc0d074c675c76",
                "title": "No Carb",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3ef0222bcc0d074c675c99",
            "title": "Chicken pesto with zucchini boats",
            "meal_images": [
                "meal_images_1564405794145_Chicken-Pesto-Zucchini-Boats.jpg",
                "meal_images_1564405794237_DSC_0012-3-1.jpg"
            ],
            "content": "",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Zucchini<strong>, </strong>Chicken, Cheddar Cheese, Pesto(Canola Oil, Basil, Parmesan&nbsp; Cheese(Pasteurized Part-Skim Cows&rsquo; Milk, Cheese Culture&rsquo;s, Salt, Enzymes), Fresh Garlic, Water, Rice Vinegar, Salt and Black Pepper), Olive Oil, Black Pepper</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g), Calories per serving: 290. Total Fat 16g &ndash; 28%,&nbsp; Sat. Fat 3.5g &ndash; 18%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 190mg - 8% .&nbsp; Vitamin D 0mcg - 0%, Calcium 100mg - 8%. Total Carb 6g - 2%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%, Protein 30g.Iron 1.2mg - 6% . Potassium 710mg - 15%.</p>\r\n",
                    "_id": "5d512d70ef08b20e4944d8e1"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5ea2bcc0d074c675c76",
                "title": "No Carb",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eefda2bcc0d074c675c97",
            "title": "Protein Muffins",
            "meal_images": [
                "meal_images_1564405722714_Protein-Apple-Muffin.jpg",
                "meal_images_1564405722715_protein-muffins-11.jpg"
            ],
            "content": "",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "",
                    "desc": "",
                    "_id": "5d3eefda2bcc0d074c675c98"
                }
            ],
            "price": "30",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5e12bcc0d074c675c75",
                "title": "Snacks",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eef8d2bcc0d074c675c95",
            "title": "Power Balls ",
            "meal_images": [
                "meal_images_1565691142324_IMG_1431_(1).jpg"
            ],
            "content": "<p>A mixture of flax seeds, bee pollen, oatmeal, honey, peanut butter&nbsp;and variation of chocolate chips or dried fruit.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Rolled Oats, Creamy Peanut Butter, Water, Kirkland Clover Honey, Flaxseed, Almonds, Chia Seeds, Bee Pollen, Cinnamon, Rodelle Organics Pure Vanilla Extract</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts:&nbsp; </strong>1 serving, serving size : 1 ball(57g),&nbsp; Calories per serving: 140. Total Fat 7g &ndash; 9%,&nbsp; Sat. Fat 1g &ndash; 5%, Trans Fat 0g, Cholest. 0Mg &ndash; 0%,&nbsp;Sodium 40mg - 2% .&nbsp; Vitamin D 0mcg - 0%, Calcium 20mg - 2%. Total Carb 17g - 6%, Fiber 3g - 11%, Total Sugars 5g, Incl. 4 g added Sugars - 8%, Protein 5g. Iron 1.1mg - 6% . Potassium 20mg - 0%.</p>\r\n",
                    "_id": "5d528d063063170c27ee9411"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5e12bcc0d074c675c75",
                "title": "Snacks",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eeef02bcc0d074c675c91",
            "title": "Salmon, Brown Rice and Veggies",
            "meal_images": [
                "meal_images_1565710044087_IMG_1430.jpg"
            ],
            "content": "<p>A wild caught salmon with Cajun seasoning with a side of freshly made brown rice and a mixture of broccoli, carrot and cauliflower veggies.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Salmon, Brown Rice, Broccoli, Carrots, Cauliflower&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g),&nbsp; Calories per serving: 360. Total Fat 15g &ndash; 19%, Sat. Fat 3g &ndash; 15%, Trans Fat 0g, Cholest. 70Mg &ndash; 23%,&nbsp;Sodium 100mg - 4% .&nbsp; Vitamin D 0mcg - 0%, Calcium 50mg - 4%. Total Carb 26g - 9%, Fiber 4g - 14%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 28g. Iron 1.2mg - 6% . Potassium 690mg - 15%.</p>\r\n",
                    "_id": "5d52d6dc3063170c27ee9414"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5db2bcc0d074c675c74",
                "title": "Seafood",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eef492bcc0d074c675c93",
            "title": "Pesto Cod white Rice and Veggies",
            "meal_images": [
                "meal_images_1564405577414_cod__hero.jpg",
                "meal_images_1564405577415_recipe-image-legacy.jpg"
            ],
            "content": "<p>Wild caught cod seasoned in a pesto sauce with a side of freshly made white rice and a mixture of broccoli, carrots and cauliflower.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Fish, Cod, Pacific, Raw, White Rice, Broccoli, Cauliflower, Carrots, Olive Oil, Basil, Garlic, Asiago Cheese (Pasturized Milk and Cream, Cheese Cultures, Salt, Enzymes), Sugar, Pine Nuts, Contains, Less Than 1% Lactic Acid, Salt, Spice and Sodium Benzoate</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g),&nbsp; Calories per serving: 330. Total Fat 13g &ndash; 17%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 55Mg &ndash; 18%,<strong>&nbsp;</strong>Sodium 450mg - 20% .&nbsp; Vitamin D 0.6mcg - 2%, Calcium 40mg - 4%. Total Carb 32g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. &lt;1 g added Sugars - 1%, Protein 22g. Iron 4.7mg - 25% . Potassium 480mg - 10%.</p>\r\n",
                    "_id": "5d512bf9ef08b20e4944d8d5"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5db2bcc0d074c675c74",
                "title": "Seafood",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eee572bcc0d074c675c8f",
            "title": "Turkey Chili Corn Bread",
            "meal_images": [
                "meal_images_1564405334898_chili-with-cornbread1.jpg",
                "meal_images_1564405334899_Turkey-Chili-Cornbread.jpg"
            ],
            "content": "<p>A mix of kidney, black and pinto beans with bell peppers onions and garlic. Topped with freshly baked protein cornbread.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Turkey, Black Beans, Kidney Beans, Pinto Beans, Crushed Tomatoes, Garlic, Onions, Bell Peppers, Cornbread</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 280. Total Fat 11g &ndash; 14%, Sat. Fat 0g &ndash; 10%, Trans Fat 0g, Cholest. 0Mg &ndash; 0%,&nbsp;Sodium 250mg - 11% . Vitamin D 0mcg - 0%, Calcium 0mg - 0%. Total Carb 24g - 9%, Fiber 0g - 0%, Total Sugars 0g, Incl. 0 g added Sugars - 0%, Protein 31g. Iron 0mg - 0% . Potassium 0mg - 0%.</p>\r\n",
                    "_id": "5d512c8bef08b20e4944d8db"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5d42bcc0d074c675c73",
                "title": "Turkey",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eed7d2bcc0d074c675c8b",
            "title": "Steak Fajita ",
            "meal_images": [
                "meal_images_1564405117009_beef_fajitas.jpg",
                "meal_images_1564405117012_steak-fajitas-horizontal.jpg"
            ],
            "content": "<p>Marinated tri tip with a side of rice topped with grilled bell peppers and onions.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients:&nbsp; </strong>Tri - Tip, Fajita Peppers, White Rice</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 360. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 80mg - 3% . Vitamin D 0mcg - 0%, Calcium 40mg - 2%. Total Carb 32g - 12%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%,&nbsp; Protein 33g. Iron 3.8mg - 20% . Potassium 560mg - 10%.</p>\r\n",
                    "_id": "5d512a2bef08b20e4944d8d0"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5b12bcc0d074c675c72",
                "title": "Beef",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eea3b2bcc0d074c675c7d",
            "title": "Chicken Green Salad with Miso Dressing",
            "meal_images": [
                "meal_images_1565767973948_IMG_1423.jpg"
            ],
            "content": "<p>A tossed spring mixed salad filled with tomato, carrots, broccoli and&nbsp;seasoned chicken breast. Topped with chopped almonds and a house made miso dressing.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Spring Mix,<strong> </strong>Chicken Breast, Carrots, Broccoli, Tomatoes, Roasted Al</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 350. Total Fat 18g &ndash; 23%, Sat. Fat 2g &ndash; 10%,&nbsp; Trans Fat 0g, Cholest. 90Mg &ndash; 30%, Sodium 190mg - 8% . Vitamin D 0mcg 0%, Calcium 200mg 15%.&nbsp; Total Carb 17g, Fiber 7g,&nbsp; Total Sugars 6g, Incl.&nbsp; 0 g added Sugars, Protein 35g. Iron 5.8mg - 30% .</p>\r\n",
                    "_id": "5d53b9260325346e8aaf0e46"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5962bcc0d074c675c6f",
                "title": "Cold Items",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eee082bcc0d074c675c8d",
            "title": "Turkey Sweet Potatoes with veggies",
            "meal_images": [
                "meal_images_1564405256275_Ground-Beef-Zucchini.jpg",
                "meal_images_1564405256276_ground-turkey-sweet-potato.jpg"
            ],
            "content": "<p>Ground turkey seasoned with spinach with a side of sweet potatoes with a sprinkle of cinnamon and a mixture of carrots, broccoli and cauliflower.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Ground Turkey, Quinoa, Sweet Potato</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g),&nbsp; Calories per serving: 420. Total Fat 15g &ndash; 19%, Sat. Fat 3.5g &ndash; 18%, Trans Fat 0g, Cholest. 120Mg &ndash; 40%,&nbsp;Sodium 140mg - 6% . Vitamin D 0.2mcg - 2%, Calcium 80mg - 6%. Total Carb 36g - 13%, Fiber 5g - 18%, Total Sugars 6g, Incl. 0 g added Sugars - 0%, Protein 36g. Iron 3.6mg - 20% . Potassium 900mg - 20%.</p>\r\n",
                    "_id": "5d512b81ef08b20e4944d8d1"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5d42bcc0d074c675c73",
                "title": "Turkey",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eeacc2bcc0d074c675c81",
            "title": "Teriyaki Chicken, White Rice and Broccoli",
            "meal_images": [
                "meal_images_1564404428659_Slow_Cooker_Chicken_Teriyaki-2-600x400.jpg",
                "meal_images_1564404428661_terichick-1.jpg"
            ],
            "content": "<p>Chicken thigh marinated in a teriyaki sauce with a side of white rice and broccoli.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients:&nbsp; </strong>Chicken Thigh, White Rice, Broccoli, Scallions, Pineapple, Mirin SweetCooking Honteri, Mirin, Liquid Amino Acids, Ginger, Liquid Amino Acids</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(0.0g), Calories per serving: 360. Total Fat 10g &ndash; 13%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 150Mg &ndash; 50%, Sodium 250mg - 11% . Vitamin D 0.2mcg 0%, Calcium 60mg 4%.&nbsp; Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 4g, Incl.&nbsp; 1 g added Sugars - 2%, Protein 33g.&nbsp;Iron 3.2mg - 20% . Potassium 610mg - 15%.</p>\r\n",
                    "_id": "5d51296fef08b20e4944d8ca"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee59f2bcc0d074c675c70",
                "title": "Chicken Meals",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eeb2d2bcc0d074c675c83",
            "title": "Chicken Fajitas",
            "meal_images": [
                "meal_images_1565690765897_IMG_1424_(1).jpg"
            ],
            "content": "<p>Grilled chicken breast marinated in a achiote flavor with a side of rice topped with grilled bell peppers and onions.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Achiote Thigh<strong> (</strong>Chicken , Lime Juice, Olive Oil, Liquid Amino Acids, Water, Annatto Seed, Oregano), Bell Peppers, Onions, Rice</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;<strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 310. Total Fat 8g &ndash; 10%, Sat. Fat 1.5g &ndash; 8%, Trans Fat 0g,&nbsp; Cholest. 95Mg &ndash; 32%, Sodium 370mg - 16% . Vitamin D 0.1mcg 0%, Calcium 60mg 4%. Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. 0 g added Sugars - 0%, Protein 26g.Iron 3.6mg - 20% . Potassium 470mg - 10%.</p>\r\n",
                    "_id": "5d528b8e3063170c27ee940b"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee59f2bcc0d074c675c70",
                "title": "Chicken Meals",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3ee7ea2bcc0d074c675c79",
            "title": "Eggs & Potatoes ",
            "meal_images": [
                "meal_images_1565691045122_IMG_1429.jpg"
            ],
            "content": "<p>A mixture of slow cooked scrambled eggs with seasoned baked red potatoes.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Eggs, Red Potatoes</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(227g), Calories per serving: 270. Total Fat 13g &ndash; 17%, Sat. Fat 4g &ndash; 20%, Trans Fat 0.5g, Cholest. 315Mg &ndash; 105%, Sodium 180mg - 8% . Vitamin D 2.3mcg 10%, Calcium 90mg 6%.&nbsp; Total Carb 24g, Fiber 2g, Total Sugars 3g, Incl.&nbsp; 0 g added Sugars, Protein 14g. Iron 2.3mg - 15% .</p>\r\n",
                    "_id": "5d528ca63063170c27ee9410"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5cfe44f5c1891efa1a9123d3",
                "title": "Breakfast",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-10T17:39:03.380Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3ee8992bcc0d074c675c7b",
            "title": "Egg Salad with Miso Dressing",
            "meal_images": [
                "meal_images_1564403865190_561847875_5141c0e111.jpg",
                "meal_images_1564403865190_Japanese-Salad-Bowls-with-a-Miso-Tahini-Dressing-and-Brown-Rice-700x514.jpg"
            ],
            "content": "<p>A tossed spring mixed salad filled with tomato, carrots and broccoli with a sliced hard boiled egg. Topped with chopped almonds and a house made miso dressing.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "",
                    "desc": "",
                    "_id": "5d47e05121a88e3295bafaa2"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5962bcc0d074c675c6f",
                "title": "Cold Items",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eea7f2bcc0d074c675c7f",
            "title": "Chicken Wrap",
            "meal_images": [
                "meal_images_1565690829519_IMG_1425_(1).jpg"
            ],
            "content": "<p>A spinach wrapped burrito filled with spring mix, carrots, tomatoes and broccoli drizzled with a house made miso dressing.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Chicken, Organic Spring Green Mix, Tomatoes, Spinach Tortilla, Carrot, Miso Dressing, Seasonal Fruit</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 270. Total Fat 6g &ndash; 8%, Sat. Fat 1.5g &ndash; 8%, Trans Fat 0g, Cholest. 95Mg &ndash; 32%, Sodium 260mg - 11% . Vitamin D 0.1mcg 0%, Calcium 80mg 6%. Total Carb 14g - 5%, Fiber 3g - 11%,&nbsp; Total Sugars 3g, Incl.&nbsp; 0 g added Sugars, Protein 38g. Iron 3.2mg - 20% . Potassium 420mg - 8%.</p>\r\n",
                    "_id": "5d528bdc3063170c27ee940d"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5962bcc0d074c675c6f",
                "title": "Cold Items",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d5a90afde10d058d2f5cc00",
            "title": "mojito",
            "meal_images": [
                "meal_images_1566216367088_mojito-cocktails.jpg"
            ],
            "content": "<p>Superb</p>\r\n",
            "contains_text": [
                "all good ingredents"
            ],
            "detail_blocks": [
                {
                    "heading": "its yum",
                    "desc": "<p>Interesting taste</p>\r\n",
                    "_id": "5d5a90afde10d058d2f5cc02"
                },
                {
                    "heading": "",
                    "desc": "",
                    "_id": "5d5a90afde10d058d2f5cc01"
                }
            ],
            "price": "12",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-08-19T09:06:41.616Z",
            "mealtype_details": {
                "_id": "5d3ee5ea2bcc0d074c675c76",
                "title": "No Carb",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [
                {
                    "_id": "5d00f5f0b89eb6111e86261a",
                    "title": "Test Title123",
                    "price": "5.50",
                    "slug": "",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-12T12:33:20.607Z",
                    "__v": 0
                }
            ],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [
                {
                    "_id": "5d15d612f03a11075ef76756",
                    "title": "Test Title",
                    "key_icon": "key_icon_1561712146459_dummy1.png",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-28T08:43:06.435Z",
                    "__v": 0
                }
            ],
            "like_details": [
                {
                    "_id": "5d14e51e82fa99153846f904",
                    "title": "test",
                    "like_icon": "like_icon_1561650462430_autumn.jpg",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-27T15:40:08.858Z",
                    "__v": 0
                }
            ],
            "dietary_details": [
                {
                    "_id": "5d14e6ad82fa99153846f906",
                    "title": "asa asasas",
                    "dietary_icon": "dietary_icon_1561650861631_new_dummy.jpeg",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-06-27T15:40:08.851Z",
                    "__v": 0
                }
            ],
            "review_details": []
        },
        {
            "_id": "5d3eeca12bcc0d074c675c87",
            "title": "Ground Beef, Pasta",
            "meal_images": [
                "meal_images_1564404897875_4524446.jpg",
                "meal_images_1564404897877_ground-beef-pasta.jpg"
            ],
            "content": "<p>Whole wheat spaghetti noodles with a homemade fresh marina sauce topped with seasoned ground beef and spinach.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Ground Beef, Whole Wheat Pasta, Parsley, Onion, Crushed Tomatoes, Sundried Tomato Bits, Garlic</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 420. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 75Mg &ndash; 25%,&nbsp;Sodium 135mg - 6% . Vitamin D 0mcg 0%, Calcium 110mg - 8%. Total Carb 46g - 17%, Fiber 6g - 21%, Total Sugars 15g,&nbsp; Incl. 0 g added Sugars - 0%, Protein 33g. Iron 7.2mg - 40% . Potassium 530mg - 10%.</p>\r\n",
                    "_id": "5d512d1aef08b20e4944d8df"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5b12bcc0d074c675c72",
                "title": "Beef",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3ee7642bcc0d074c675c77",
            "title": "Soyrizo wrap",
            "meal_images": [
                "meal_images_1565690890412_IMG_1427.jpg"
            ],
            "content": "<p>Start your morning off with a Spinach tortilla wrapped burrito filled with soyrizo and eggs. Served with a fresh side of seasonal fruit.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients:</strong> Eggs, Soy Chorizo, Spinach Tortilla, Seasonal Fruit, Onion, Green Bell Pe</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts:</strong> 1 serving, serving size : 1 mass meal(227g), Calories per serving: 370. Total Fat 23g &ndash; 29%, Sat. Fat 6g &ndash; 30%, Trans Fat 1g, Cholest. 355Mg &ndash; 118%, Sodium 640mg - 28% . Vitamin D 2.3mcg 10%, Calcium 150mg 10%, Potassium 210mg 4%. Total Carb 19g, Fiber 3g, Total Sugars 4g, Incl. 0 g added Sugars, Protein 20g.Iron 2.8mg - 15% .</p>\r\n",
                    "_id": "5d528c0b3063170c27ee940e"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5cfe44f5c1891efa1a9123d3",
                "title": "Breakfast",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-10T17:39:03.380Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eec5f2bcc0d074c675c85",
            "title": "Al Pastor, Rice and veggies",
            "meal_images": [
                "meal_images_1565710424764_IMG_1436.jpg"
            ],
            "content": "<p>Pork marinated with pineapples and lime juice with a side of rice&nbsp;and a mix of broccoli, carrots and cauliflower.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Al Pastor, White Rice,<strong> </strong>Broccoli, Cauliflower, Carrots</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(10g), Calories per serving: 250. Total Fat 5g &ndash; 6%, Sat. Fat 1g &ndash; 5%, Trans Fat 0g,&nbsp; Cholest. 40Mg &ndash; 13%, Sodium 220mg - 10% . Vitamin D 0.1mcg 0%, Calcium 35mg 2%. Total Carb 31g - 11%, Fiber 3g - 11%, Total Sugars 3g,&nbsp; Incl. 0 g added Sugars - 0%, Protein 19g. Iron 2.3mg - 15% . Potassium 460mg - 10%.</p>\r\n",
                    "_id": "5d52d8593063170c27ee9415"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5aa2bcc0d074c675c71",
                "title": "Pork",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": []
        },
        {
            "_id": "5d3eed362bcc0d074c675c89",
            "title": "Tri Tip, Brown Rice and Squash",
            "meal_images": [
                "meal_images_1565691302808_IMG_1435.jpg"
            ],
            "content": "<p>Slow cooked marinated tri tip with a side of brown and seasoned squash.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Tri - Tip, Zucchini, Brown Rice<br />\r\n<br />\r\n<strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(277g), Calories per serving: 410. Total Fat 13g &ndash; 17%, Sat. Fat 4.5g &ndash; 23%, Trans Fat 0g, Cholest. 130Mg &ndash; 40%,&nbsp;Sodium 105mg - 5% . Vitamin D 0mcg 0%, Calcium 60mg - 4%. Total Carb 23g - 8%, Fiber 2g - 7%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 49g. Iron 3.6mg - 20% . Potassium 900mg - 20%.</p>\r\n",
                    "_id": "5d528da93063170c27ee9412"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5b12bcc0d074c675c72",
                "title": "Beef",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "dietary_details": [],
            "review_details": [
                {
                    "_id": "5d5bcfb349afef01433af84d",
                    "user_id": "5d4ab2d748a8fe34c9b98ef7",
                    "meal_id": "5d3eed362bcc0d074c675c89",
                    "title": "Delicious",
                    "comment": "Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. ",
                    "rating": 5,
                    "createdAt": "2019-08-19T08:13:49.497Z",
                    "isDeleted": false,
                    "__v": 0
                },
                {
                    "_id": "5d5a5bc349156e065ea391e1",
                    "user_id": "5d4ab2d748a8fe34c9b98ef7",
                    "meal_id": "5d3eed362bcc0d074c675c89",
                    "title": "Delicious",
                    "comment": "Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. ",
                    "rating": 4,
                    "createdAt": "2019-08-19T08:13:49.497Z",
                    "isDeleted": false,
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Product fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/

namedRouter.post("api.product.list", '/product/list', request_param.any(), async (req, res) => {
    try {
        const success = await productController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /product/details/:id  Product Details
 * @apiVersion 1.0.0
 * @apiParam {objectid}  id Product Id
 * @apiGroup Product
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d3eed362bcc0d074c675c89",
            "title": "Tri Tip, Brown Rice and Squash",
            "meal_images": [
                "meal_images_1565691302808_IMG_1435.jpg"
            ],
            "content": "<p>Slow cooked marinated tri tip with a side of brown and seasoned squash.</p>\r\n",
            "contains_text": [
                ""
            ],
            "detail_blocks": [
                {
                    "heading": "Ingredients & Nutrition Facts",
                    "desc": "<p><strong>Ingredients: </strong>Tri - Tip, Zucchini, Brown Rice<br />\r\n<br />\r\n<strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(277g), Calories per serving: 410. Total Fat 13g &ndash; 17%, Sat. Fat 4.5g &ndash; 23%, Trans Fat 0g, Cholest. 130Mg &ndash; 40%,&nbsp;Sodium 105mg - 5% . Vitamin D 0mcg 0%, Calcium 60mg - 4%. Total Carb 23g - 8%, Fiber 2g - 7%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 49g. Iron 3.6mg - 20% . Potassium 900mg - 20%.</p>\r\n",
                    "_id": "5d528da93063170c27ee9412"
                }
            ],
            "price": 30,
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-29T11:23:22.610Z",
            "mealtype_details": {
                "_id": "5d3ee5b12bcc0d074c675c72",
                "title": "Beef",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-29T11:23:22.628Z",
                "__v": 0
            },
            "topping_details": [],
            "category_details": {
                "_id": "5d039fdac1891efa1ae98e41",
                "title": "Sample",
                "slug": "sample",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-14T09:31:26.317Z",
                "__v": 0
            },
            "key_details": [],
            "like_details": [],
            "review_details": [
                {
                    "_id": "5d5bcfb349afef01433af84d",
                    "user_id": [
                        {
                            "_id": "5d4ab2d748a8fe34c9b98ef7",
                            "first_name": "Test",
                            "last_name": "Test",
                            "email": "sann@yopmail.com",
                            "phone": "01234567890",
                            "password": "$2a$08$SMvNVmf1NS/bB2wZZqgFe.EtZ62SPAb0SBGReuMWcB.bBxAQTJKKK",
                            "isNewUser": false,
                            "isVerified": "Yes",
                            "verifyToken": null,
                            "deviceToken": "",
                            "deviceType": "",
                            "isDeleted": false,
                            "isActive": true,
                            "role": "5cfe19dec1891efa1a8e2193",
                            "createdAt": "2019-08-07T11:15:35.213Z",
                            "updatedAt": "2019-08-07T13:23:37.482Z",
                            "__v": 0
                        }
                    ],
                    "meal_id": "5d3eed362bcc0d074c675c89",
                    "title": "Delicious",
                    "comment": "Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. ",
                    "rating": 5,
                    "createdAt": "2019-08-19T08:13:49.497Z",
                    "isDeleted": false,
                    "__v": 0
                },
                {
                    "_id": "5d5a5bc349156e065ea391e1",
                    "user_id": [
                        {
                            "_id": "5d4ab2d748a8fe34c9b98ef7",
                            "first_name": "Test",
                            "last_name": "Test",
                            "email": "sann@yopmail.com",
                            "phone": "01234567890",
                            "password": "$2a$08$SMvNVmf1NS/bB2wZZqgFe.EtZ62SPAb0SBGReuMWcB.bBxAQTJKKK",
                            "isNewUser": false,
                            "isVerified": "Yes",
                            "verifyToken": null,
                            "deviceToken": "",
                            "deviceType": "",
                            "isDeleted": false,
                            "isActive": true,
                            "role": "5cfe19dec1891efa1a8e2193",
                            "createdAt": "2019-08-07T11:15:35.213Z",
                            "updatedAt": "2019-08-07T13:23:37.482Z",
                            "__v": 0
                        }
                    ],
                    "meal_id": "5d3eed362bcc0d074c675c89",
                    "title": "Delicious",
                    "comment": "Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. ",
                    "rating": 4,
                    "createdAt": "2019-08-19T08:13:49.497Z",
                    "isDeleted": false,
                    "__v": 0
                }
            ]
        }
    ],
    "message": "product details fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.product.details", '/product/details/:id', request_param.any(), async (req, res) => {
    try {
        const success = await productController.details(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /product/bestseller/list  Bestseller Product List
 * @apiVersion 1.0.0
 * @apiGroup Product
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d09d65819672c02668c6500",
            "meals": {
                "_id": "5d09d65819672c02668c6500",
                "title": "test",
                "meal_images": [
                    "meal_images_1560925782885_dymmy2.jpg",
                    "meal_images_1560925782917_man.png"
                ],
                "topping_id": [
                    "5cff84c311cb740c27fec5d8",
                    "5d03708db407ab5b64ac40b2"
                ],
                "category_id": [
                    "5d03a1b1711e791395218566"
                ],
                "like_id": [
                    "5d03ceedd937b53bcc1dc7ac",
                    "5d03bec6c1891efa1aebde0a"
                ],
                "taste_text": "Mouth Watering",
                "content": "<p>Brief Description</p>",
                "key_id": [
                    "5d0891c35db2832b613282d6",
                    "5d03b6c1f435d21a267886a1"
                ],
                "dietary_id": [
                    "5d03c4e674e2d71f1b14848c",
                    "5d03c6ae74e2d71f1b14848d"
                ],
                "contains_text": [
                    "asd",
                    "qwer",
                    "fgre"
                ],
                "block_image": "block_image_1560925782922_download_1.png",
                "price": "5.50",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-19T06:14:23.682Z",
                "meal_type": "5cfe44f5c1891efa1a9123d3",
                "detail_blocks": [
                    {
                        "heading": "h1",
                        "desc": "<p>c1</p>",
                        "_id": "5d09d65819672c02668c6503"
                    },
                    {
                        "heading": "h2",
                        "desc": "c2",
                        "_id": "5d09d65819672c02668c6502"
                    },
                    {
                        "heading": "h3",
                        "desc": "c3",
                        "_id": "5d09d65819672c02668c6501"
                    }
                ],
                "__v": 0
            },
            "category": "Lattes",
            "itemsale": 2
        },
        {
            "_id": "5d079db6de3abb0d9a3cde64",
            "meals": {
                "_id": "5d079db6de3abb0d9a3cde64",
                "title": "Chicken Roast",
                "meal_images": [],
                "topping_id": [
                    "5d03708db407ab5b64ac40b2"
                ],
                "category_id": [
                    "5d039ffec1891efa1ae990e9"
                ],
                "taste_text": "Mouth Watering",
                "content": "<p>Sample Content</p>\r\n",
                "key_id": [
                    "5d03b6a5f435d21a267886a0"
                ],
                "dietary_id": [
                    "5d03c6ae74e2d71f1b14848d"
                ],
                "like_id": [
                    "5d03bec6c1891efa1aebde0a",
                    "5d03ceedd937b53bcc1dc7ac"
                ],
                "contains_text": [],
                "block_image": "",
                "price": "500",
                "slug": "",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-06-17T13:18:41.019Z",
                "meal_type": "5cfe4b5b43886608752e09e9",
                "detail_blocks": [],
                "__v": 0
            },
            "category": "Soups",
            "itemsale": 1
        }
    ],
    "message": "Best sell product list fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.product.bestseller", '/product/bestseller/list', request_param.any(), async (req, res) => {
    try {
        const success = await productController.bestsellerList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.all('/products*', auth.authenticate);


/**
 * @api {post} /products/review  Post Product Review
 * @apiVersion 1.0.0
 * @apiHeader {string} x-access-token User's Access Token
 * @apiParam {String} meal_id Meal Id
 * @apiParam {String} title Review Short Title
 * @apiParam {String} comment Review Content
 * @apiParam {String} rating Rating Number[ex: 1/2/3/4/5]
 * @apiGroup Product
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "user_id": "5d4ab2d748a8fe34c9b98ef7",
        "meal_id": "5d3eed362bcc0d074c675c89",
        "title": "Delicious",
        "comment": "Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. ",
        "rating": 4,
        "createdAt": "2019-08-19T08:13:49.497Z",
        "isDeleted": false,
        "_id": "5d5a5bc349156e065ea391e1",
        "__v": 0
    },
    "message": "Thank you, We have received your review on this Meal."
}
*/
namedRouter.post("api.products.review", '/products/review', request_param.any(), async (req, res) => {
    try {
        const success = await productController.productReview(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Export the express.Router() instance
module.exports = router;