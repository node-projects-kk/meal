const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const userController = require('webservice/user.controller');

const request_param = multer();

/**
 * @api {post} /user/checkzipcode Check Zip Code
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam zip Zipcode
 * 
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "title": 78945,
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-06-14T09:31:26.317Z",
        "_id": "5d037fcac1891efa1ae74604",
        "__v": 0
    },
    "message": "Zipcode fetched successfully"
}
*/
namedRouter.post("api.user.checkzip", '/user/checkzipcode', request_param.any(), async (req, res) => {
    try {
        const success = await userController.checkZipCode(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /user/login Login
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {string} email Email
 * @apiParam {string} password Password
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZmZhNTgwZDI0Mjc4MTFkN2VmOGUyZiIsImlhdCI6MTU2MDc2NzAzOCwiZXhwIjoxNTYwODUzNDM4fQ.zJlAXvAj0DeK5ktYQ9T6JjyY6eFZbvraIRb8e_kCpvI",
    "data": {
        "first_name": "Sougatadsdsds",
        "last_name": "Royttyry",
        "email": "vendor10@mahk.eu",
        "phone": "9932155101",
        "password": "$2a$08$.ungCsiSf9xBIAIydlHqxON4h.5twDUQyasua9/4Sie6f6wSdzz1W",
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5cffa580d2427811d7ef8e2f",
        "role": {
            "desc": "User of the application.",
            "_id": "5cfe19dec1891efa1a8e2193",
            "roleDisplayName": "User",
            "role": "user",
            "id": "5cfe19dec1891efa1a8e2193"
        },
        "__v": 0
    },
    "message": "You have successfully logged in"
}

*@apiErrorExample {json} Error
{
    "status": 200,
    "data":{},
    "message": "Authentication failed, Wrong Password"
}
*/

namedRouter.post("api.user.login", '/user/login', request_param.any(), async (req, res) => {
    try {
        const success = await userController.login(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/reset-password Forgot Password
 * @apiVersion 1.0.0
 * @apiParam {string} email Email.
 * @apiGroup User
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {},
    "message": "A email with new password has been sent to your email address."
}
*@apiErrorExample {json} Error
{
    "status": 200,
    "data": {},
    "message": "No user found."
}
*/

namedRouter.post("api.user.forgotPassword", '/user/reset-password', request_param.any(), async (req, res) => {
    try {
        const success = await userController.forgotPassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /user/logout/:id Logout
 * @apiVersion 1.0.0
  * @apiGroup User
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {},
    "message": "You have successfully logout."
}
*/

namedRouter.get("api.user.logout", '/user/logout/:id', request_param.any(), async (req, res) => {
    try {
        const success = await userController.logout(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.all('/user*', auth.authenticateAPI);

/**
 * @api {post} /user/changePassword User Change Password
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {string} old_password Old password
 * @apiParam {string} new_password New password
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "test",
        "last_name": "test",
        "email": "test046@test.com",
        "phone": "1230123013",
        "password": "$2a$08$WAH3M8RxUzufNZnCQxgoJ.7mAaPilKkRRNpteBsdUeiyWzbZMjMxe",
        "isNewUser": false,
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d5419e727fd1004ab79a35b",
        "role": "5cfe19dec1891efa1a8e2193",
        "createdAt": "2019-08-14T14:25:43.906Z",
        "updatedAt": "2019-08-20T11:15:58.434Z",
        "__v": 0
    },
    "message": "Password Changed Successfully"
}
*/
namedRouter.post("api.user.changePassword", '/user/changePassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/profileUpdate Update Profile
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiParam {string} first_name First Name
 * @apiParam {string} last_name Last Name
 * @apiParam {string} phone Phone
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "first_name": "Ram1",
        "last_name": "Singh1",
        "email": "test046@test.com",
        "phone": "7890321456",
        "password": "$2a$08$g94fMRqms0pygwbI1.rhQ.U4Lv5RpSkBBHjSXKIXWfNHiEMQtgKhq",
        "isNewUser": false,
        "isVerified": "Yes",
        "verifyToken": null,
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d5419e727fd1004ab79a35b",
        "role": "5cfe19dec1891efa1a8e2193",
        "createdAt": "2019-08-14T14:25:43.906Z",
        "updatedAt": "2019-08-20T16:04:56.941Z",
        "__v": 0
    },
    "message": "Profile updated successfully."
}
*/
namedRouter.post("api.user.profileUpdate", '/user/profileUpdate', request_param.any(), async (req, res) => {
    try {
        const success = await userController.UpdateProfile(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /user/profile/remove Remove Profile
 * @apiVersion 1.0.0
 * @apiGroup User
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {},
    "message": "Your profile has been removed successfully."
}
*/

namedRouter.get("api.user.profile.remove", '/user/profile/remove', request_param.any(), async (req, res) => {
    try {
        const success = await userController.removeProfile(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Export the express.Router() instance
module.exports = router;