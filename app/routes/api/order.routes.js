const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const orderController = require('webservice/order.controller');
const request_param = multer();


/**
 * @api {post} /order/place Order Place
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiHeader {string} x-access-token User's Access Token (Only for logged in Users)
 * @apiParam {Number} total_price Order Total Price
 * @apiParam {Float} discounted_price Discounted Price
 * @apiParam {string} coupon_code Coupon Code
 * @apiParam {String} first_name First Name
 * @apiParam {String} last_name Last Name
 * @apiParam {String} address Address
 * @apiParam {String} apt_suite Aparment, Suite
 * @apiParam {String} city City
 * @apiParam {String} state State
 * @apiParam {String} phone Phone Number
 * @apiParam {String} company_name Company Name
 * @apiParam {String} email  Email (Only for new users)
 * @apiParam {String} password Password (Only for new users)
 * @apiParam {Date} deliver_arrive_on Delivery Date (example: YYYY-MM-DD)
 * @apiParam {String} deliver_day Day of the week (example: Thursday)
 * @apiParam {String} card_number Card Number
 * @apiParam {String} exp_month Expiry Month
 * @apiParam {String} exp_year Expiry Year
 * @apiParam {String} cvc CVV
 * @apiParam {String} post_code Postal Code
 * @apiParam {Array} order_details Order Details (example: order_details[0][meal_id]:5d11e60017334d0b52666155
order_details[0][quantity]:1
order_details[0][meal_price]:700
order_details[0][topping_details][0][topping_id]:5cff69e3c1891efa1aa27120
order_details[0][topping_details][0][price]:75)
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "shipping_address": {
            "first_name": "Ayan",
            "last_name": "Chakraborty",
            "address": "Tollygounge",
            "apt_suite": "test apt",
            "city": "Kolkata",
            "state": "WB",
            "phone": "7894561236",
            "company_name": "WTS LTD"
        },
        "order_no": "G5OG20H4IA",
        "total_price": "2000",
        "card_number": "4242424242424242",
        "exp_month": "04",
        "exp_year": "2020",
        "cvc": "890",
        "post_code": "74152",
        "deliver_arrive_on": "Sat, Jun  29",
        "deliver_day": "Thursday",
        "order_status": "Pending",
        "payment_status": "Pending",
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-06-26T12:44:34.996Z",
        "_id": "5d13698b6019f70f0eb13e35",
        "order_details": [
            {
                "quantity": 1,
                "meal_price": "700",
                "meal_id": "5d11e60017334d0b52666155",
                "topping_details": [
                    {
                        "topping_id": "5cff69e3c1891efa1aa27120",
                        "price": "75"
                    },
                    {
                        "topping_id": "5cff84c311cb740c27fec5d8",
                        "price": "11"
                    }
                ]
            },
            {
                "quantity": 1,
                "meal_price": "700",
                "meal_id": "5d13664a3050bb0eb3b89853",
                "topping_details": [
                    {
                        "topping_id": "5d07419b74186a02526b1e8f",
                        "price": "75"
                    },
                    {
                        "topping_id": "5d00f5f0b89eb6111e86261a",
                        "price": "100"
                    }
                ]
            }
        ],
        "user_id": "5d1369856019f70f0eb13e34",
        "__v": 0
    },
    "message": "Order placed successfully"
}
*/

namedRouter.post("api.order.place", '/order/place', request_param.any(), async (req, res) => {
    try {
        const success = await orderController.placeOrder(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.all('/orders*', auth.authenticateAPI);

/**
 * @api {get} /orders/history User Order History
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d6f8ca6b8a9b52be4038d43",
            "order_no": "k6CWWRchpT",
            "shipping_address": {
                "first_name": "Test",
                "last_name": "test",
                "address": "test address",
                "apt_suite": "test area",
                "city": "test city",
                "state": "Florida",
                "phone": "2123123131",
                "company_name": ""
            },
            "coupon": {
                "coupon_code": "",
                "coupon_description": "",
                "amount": 0,
                "discount_type": "flat"
            },
            "deliver_arrive_on": "Thu, Aug 22",
            "deliver_day": "Thursday",
            "total_price": 360,
            "discounted_price": 0,
            "createdAt": "2019-09-04T10:04:15.437Z",
            "order_details": [
                {
                    "quantity": 1,
                    "meal_price": 30,
                    "meal_id": "5d3eef492bcc0d074c675c93",
                    "meal_info": {
                        "_id": "5d3eef492bcc0d074c675c93",
                        "title": "Pesto Cod white Rice and Veggies",
                        "meal_images": [
                            "meal_images_1564405577414_cod__hero.jpg",
                            "meal_images_1564405577415_recipe-image-legacy.jpg"
                        ],
                        "topping_id": [],
                        "category_id": [
                            "5d039fdac1891efa1ae98e41"
                        ],
                        "like_id": [],
                        "taste_text": "",
                        "content": "<p>Wild caught cod seasoned in a pesto sauce with a side of freshly made white rice and a mixture of broccoli, carrots and cauliflower.</p>\r\n",
                        "key_id": [],
                        "dietary_id": [],
                        "contains_text": [
                            ""
                        ],
                        "block_image": "",
                        "price": 30,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-07-29T11:23:22.610Z",
                        "meal_type": "5d3ee5db2bcc0d074c675c74",
                        "detail_blocks": [
                            {
                                "heading": "Ingredients & Nutrition Facts",
                                "desc": "<p><strong>Ingredients: </strong>Fish, Cod, Pacific, Raw, White Rice, Broccoli, Cauliflower, Carrots, Olive Oil, Basil, Garlic, Asiago Cheese (Pasturized Milk and Cream, Cheese Cultures, Salt, Enzymes), Sugar, Pine Nuts, Contains, Less Than 1% Lactic Acid, Salt, Spice and Sodium Benzoate</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g),&nbsp; Calories per serving: 330. Total Fat 13g &ndash; 17%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 55Mg &ndash; 18%,<strong>&nbsp;</strong>Sodium 450mg - 20% .&nbsp; Vitamin D 0.6mcg - 2%, Calcium 40mg - 4%. Total Carb 32g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. &lt;1 g added Sugars - 1%, Protein 22g. Iron 4.7mg - 25% . Potassium 480mg - 10%.</p>\r\n",
                                "_id": "5d512bf9ef08b20e4944d8d5"
                            }
                        ],
                        "__v": 0
                    },
                    "category_info": [
                        {
                            "_id": "5d039fdac1891efa1ae98e41",
                            "title": "Sample",
                            "slug": "sample",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-06-14T09:31:26.317Z",
                            "__v": 0
                        }
                    ]
                },
                {
                    "quantity": 1,
                    "meal_price": 30,
                    "meal_id": "5d3eed7d2bcc0d074c675c8b",
                    "meal_info": {
                        "_id": "5d3eed7d2bcc0d074c675c8b",
                        "title": "Steak Fajita ",
                        "meal_images": [
                            "meal_images_1564405117009_beef_fajitas.jpg",
                            "meal_images_1564405117012_steak-fajitas-horizontal.jpg"
                        ],
                        "topping_id": [],
                        "category_id": [
                            "5d039fdac1891efa1ae98e41"
                        ],
                        "like_id": [],
                        "taste_text": "",
                        "content": "<p>Marinated tri tip with a side of rice topped with grilled bell peppers and onions.</p>\r\n",
                        "key_id": [],
                        "dietary_id": [],
                        "contains_text": [
                            ""
                        ],
                        "block_image": "",
                        "price": 30,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-07-29T11:23:22.610Z",
                        "meal_type": "5d3ee5b12bcc0d074c675c72",
                        "detail_blocks": [
                            {
                                "heading": "Ingredients & Nutrition Facts",
                                "desc": "<p><strong>Ingredients:&nbsp; </strong>Tri - Tip, Fajita Peppers, White Rice</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 360. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 80mg - 3% . Vitamin D 0mcg - 0%, Calcium 40mg - 2%. Total Carb 32g - 12%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%,&nbsp; Protein 33g. Iron 3.8mg - 20% . Potassium 560mg - 10%.</p>\r\n",
                                "_id": "5d512a2bef08b20e4944d8d0"
                            }
                        ],
                        "__v": 0
                    },
                    "category_info": [
                        {
                            "_id": "5d039fdac1891efa1ae98e41",
                            "title": "Sample",
                            "slug": "sample",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-06-14T09:31:26.317Z",
                            "__v": 0
                        }
                    ]
                },
                {
                    "quantity": 1,
                    "meal_price": 30,
                    "meal_id": "5d3eeacc2bcc0d074c675c81",
                    "meal_info": {
                        "_id": "5d3eeacc2bcc0d074c675c81",
                        "title": "Teriyaki Chicken, White Rice and Broccoli",
                        "meal_images": [
                            "meal_images_1564404428659_Slow_Cooker_Chicken_Teriyaki-2-600x400.jpg",
                            "meal_images_1564404428661_terichick-1.jpg"
                        ],
                        "topping_id": [],
                        "category_id": [
                            "5d039fdac1891efa1ae98e41"
                        ],
                        "like_id": [],
                        "taste_text": "",
                        "content": "<p>Chicken thigh marinated in a teriyaki sauce with a side of white rice and broccoli.</p>\r\n",
                        "key_id": [],
                        "dietary_id": [],
                        "contains_text": [
                            ""
                        ],
                        "block_image": "",
                        "price": 30,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-07-29T11:23:22.610Z",
                        "meal_type": "5d3ee59f2bcc0d074c675c70",
                        "detail_blocks": [
                            {
                                "heading": "Ingredients & Nutrition Facts",
                                "desc": "<p><strong>Ingredients:&nbsp; </strong>Chicken Thigh, White Rice, Broccoli, Scallions, Pineapple, Mirin SweetCooking Honteri, Mirin, Liquid Amino Acids, Ginger, Liquid Amino Acids</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(0.0g), Calories per serving: 360. Total Fat 10g &ndash; 13%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 150Mg &ndash; 50%, Sodium 250mg - 11% . Vitamin D 0.2mcg 0%, Calcium 60mg 4%.&nbsp; Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 4g, Incl.&nbsp; 1 g added Sugars - 2%, Protein 33g.&nbsp;Iron 3.2mg - 20% . Potassium 610mg - 15%.</p>\r\n",
                                "_id": "5d51296fef08b20e4944d8ca"
                            }
                        ],
                        "__v": 0
                    },
                    "category_info": [
                        {
                            "_id": "5d039fdac1891efa1ae98e41",
                            "title": "Sample",
                            "slug": "sample",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-06-14T09:31:26.317Z",
                            "__v": 0
                        }
                    ]
                }
            ]
        }
    ],
    "message": "Your orders fetched successfully."
}
*/
namedRouter.get("api.orders.history", '/orders/history', request_param.any(), async (req, res) => {
    try {
        const success = await orderController.orderHistory(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /orders/historyDetails/:id Order History Details
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiParam {ObjectId} id Pass Order Id
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d6f8ca6b8a9b52be4038d43",
            "order_no": "P8CWhRchpT",
            "shipping_address": {
                "first_name": "Test",
                "last_name": "test",
                "address": "test address",
                "apt_suite": "test area",
                "city": "test city",
                "state": "Florida",
                "phone": "2123123131",
                "company_name": ""
            },
            "coupon": {
                "coupon_code": "",
                "coupon_description": "",
                "amount": 0,
                "discount_type": "flat"
            },
            "deliver_arrive_on": "Thu, Aug 22",
            "deliver_day": "Thursday",
            "total_price": 360,
            "discounted_price": 0,
            "createdAt": "2019-09-04T10:04:15.437Z",
            "order_status": "Completed",
            "order_details": [
                {
                    "quantity": 1,
                    "meal_price": 30,
                    "meal_id": "5d3eef492bcc0d074c675c93",
                    "meal_info": {
                        "_id": "5d3eef492bcc0d074c675c93",
                        "title": "Pesto Cod white Rice and Veggies",
                        "meal_images": [
                            "meal_images_1564405577414_cod__hero.jpg",
                            "meal_images_1564405577415_recipe-image-legacy.jpg"
                        ],
                        "topping_id": [],
                        "category_id": [
                            "5d039fdac1891efa1ae98e41"
                        ],
                        "like_id": [],
                        "taste_text": "",
                        "content": "<p>Wild caught cod seasoned in a pesto sauce with a side of freshly made white rice and a mixture of broccoli, carrots and cauliflower.</p>\r\n",
                        "key_id": [],
                        "dietary_id": [],
                        "contains_text": [
                            ""
                        ],
                        "block_image": "",
                        "price": 30,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-07-29T11:23:22.610Z",
                        "meal_type": "5d3ee5db2bcc0d074c675c74",
                        "detail_blocks": [
                            {
                                "heading": "Ingredients & Nutrition Facts",
                                "desc": "<p><strong>Ingredients: </strong>Fish, Cod, Pacific, Raw, White Rice, Broccoli, Cauliflower, Carrots, Olive Oil, Basil, Garlic, Asiago Cheese (Pasturized Milk and Cream, Cheese Cultures, Salt, Enzymes), Sugar, Pine Nuts, Contains, Less Than 1% Lactic Acid, Salt, Spice and Sodium Benzoate</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g),&nbsp; Calories per serving: 330. Total Fat 13g &ndash; 17%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 55Mg &ndash; 18%,<strong>&nbsp;</strong>Sodium 450mg - 20% .&nbsp; Vitamin D 0.6mcg - 2%, Calcium 40mg - 4%. Total Carb 32g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. &lt;1 g added Sugars - 1%, Protein 22g. Iron 4.7mg - 25% . Potassium 480mg - 10%.</p>\r\n",
                                "_id": "5d512bf9ef08b20e4944d8d5"
                            }
                        ],
                        "__v": 0
                    },
                    "category_info": [
                        {
                            "_id": "5d039fdac1891efa1ae98e41",
                            "title": "Sample",
                            "slug": "sample",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-06-14T09:31:26.317Z",
                            "__v": 0
                        }
                    ]
                },
                {
                    "quantity": 1,
                    "meal_price": 30,
                    "meal_id": "5d3eed7d2bcc0d074c675c8b",
                    "meal_info": {
                        "_id": "5d3eed7d2bcc0d074c675c8b",
                        "title": "Steak Fajita ",
                        "meal_images": [
                            "meal_images_1564405117009_beef_fajitas.jpg",
                            "meal_images_1564405117012_steak-fajitas-horizontal.jpg"
                        ],
                        "topping_id": [],
                        "category_id": [
                            "5d039fdac1891efa1ae98e41"
                        ],
                        "like_id": [],
                        "taste_text": "",
                        "content": "<p>Marinated tri tip with a side of rice topped with grilled bell peppers and onions.</p>\r\n",
                        "key_id": [],
                        "dietary_id": [],
                        "contains_text": [
                            ""
                        ],
                        "block_image": "",
                        "price": 30,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-07-29T11:23:22.610Z",
                        "meal_type": "5d3ee5b12bcc0d074c675c72",
                        "detail_blocks": [
                            {
                                "heading": "Ingredients & Nutrition Facts",
                                "desc": "<p><strong>Ingredients:&nbsp; </strong>Tri - Tip, Fajita Peppers, White Rice</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 360. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 80mg - 3% . Vitamin D 0mcg - 0%, Calcium 40mg - 2%. Total Carb 32g - 12%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%,&nbsp; Protein 33g. Iron 3.8mg - 20% . Potassium 560mg - 10%.</p>\r\n",
                                "_id": "5d512a2bef08b20e4944d8d0"
                            }
                        ],
                        "__v": 0
                    },
                    "category_info": [
                        {
                            "_id": "5d039fdac1891efa1ae98e41",
                            "title": "Sample",
                            "slug": "sample",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-06-14T09:31:26.317Z",
                            "__v": 0
                        }
                    ]
                },
                {
                    "quantity": 1,
                    "meal_price": 30,
                    "meal_id": "5d3eeacc2bcc0d074c675c81",
                    "meal_info": {
                        "_id": "5d3eeacc2bcc0d074c675c81",
                        "title": "Teriyaki Chicken, White Rice and Broccoli",
                        "meal_images": [
                            "meal_images_1564404428659_Slow_Cooker_Chicken_Teriyaki-2-600x400.jpg",
                            "meal_images_1564404428661_terichick-1.jpg"
                        ],
                        "topping_id": [],
                        "category_id": [
                            "5d039fdac1891efa1ae98e41"
                        ],
                        "like_id": [],
                        "taste_text": "",
                        "content": "<p>Chicken thigh marinated in a teriyaki sauce with a side of white rice and broccoli.</p>\r\n",
                        "key_id": [],
                        "dietary_id": [],
                        "contains_text": [
                            ""
                        ],
                        "block_image": "",
                        "price": 30,
                        "slug": "",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-07-29T11:23:22.610Z",
                        "meal_type": "5d3ee59f2bcc0d074c675c70",
                        "detail_blocks": [
                            {
                                "heading": "Ingredients & Nutrition Facts",
                                "desc": "<p><strong>Ingredients:&nbsp; </strong>Chicken Thigh, White Rice, Broccoli, Scallions, Pineapple, Mirin SweetCooking Honteri, Mirin, Liquid Amino Acids, Ginger, Liquid Amino Acids</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(0.0g), Calories per serving: 360. Total Fat 10g &ndash; 13%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 150Mg &ndash; 50%, Sodium 250mg - 11% . Vitamin D 0.2mcg 0%, Calcium 60mg 4%.&nbsp; Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 4g, Incl.&nbsp; 1 g added Sugars - 2%, Protein 33g.&nbsp;Iron 3.2mg - 20% . Potassium 610mg - 15%.</p>\r\n",
                                "_id": "5d51296fef08b20e4944d8ca"
                            }
                        ],
                        "__v": 0
                    },
                    "category_info": [
                        {
                            "_id": "5d039fdac1891efa1ae98e41",
                            "title": "Sample",
                            "slug": "sample",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-06-14T09:31:26.317Z",
                            "__v": 0
                        }
                    ]
                }
            ]
        }
    ],
    "message": "Order details fetched successfully."
}
*/
namedRouter.get("api.orders.historyDetails", '/orders/historyDetails/:id', request_param.any(), async (req, res) => {
    try {
        const success = await orderController.orderHistoryDetails(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /orders/cancel/:id Order Cancel
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiHeader {string} x-access-token User's Access Token
 * @apiParam {ObjectId} id Pass Order Id
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
    { 
        'id': 're_1FFKlYCae2wvBG9BQUbRybR6',
        'object': 'refund',
        'amount': 50000,
        'balance_transaction': 'txn_1FFKlYCae2wvBG9Bjy95FiKD',
        'charge': 'ch_1FFKlDCae2wvBG9B1thxlmAp',
        'created': 1567689168,
        'currency': 'usd',
        'metadata': {},
        'reason': null,
        'receipt_number': null,
        'source_transfer_reversal': null,
        'status': 'succeeded',
        'transfer_reversal': null 
    }
    "message": "Refund process initiated. Please wait sometime, you will receive an email from the Payment Gateway."
}
*/
namedRouter.get("api.orders.cancel", '/orders/cancel/:id', request_param.any(), async (req, res) => {
    try {
        const success = await orderController.orderCancel(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Export the express.Router() instance
module.exports = router;