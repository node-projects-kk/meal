const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const teamController = require('webservice/team.controller');
const request_param = multer();



/**
 * @api {get} /about/content About Content
 * @apiVersion 1.0.0
 * @apiGroup About
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "main_heading": "About Us",
        "block_title": "Who we are?",
        "block_content": "<p>Mass Meals was not originally created to be a company. It was created to save my fathers life. My life was rocked when my father had a heart attack. Doctors told me he needed a heart and a kidney transplant. In order to get on these lists to receive these surgeries he had to loose 85lbs to be eligible. With the doctors, nutritionist at Stanford Hospital and my knowledge of food we created what is now called The Mass Meals Program. My father was also insulin dependent diabetic, so salt was an issue. We created a low sodium multi meal diet to help speed up metabolism. This diet is created to loose stored fat while building lean muscle mass. With in three months we reached our goal of weight loss and reversed the need of a kidney transplant strictly with the diet. As a result of the meal plan he was able to pull and lessen up on medication t. I then applied the diet to myself, friends, family, only to receive amazing results.</p>\r\n\r\n<p>We have put together simple ingredients, process, and program to really help people loose weight, put on lean muscle, control sugar levels, and even reverse surgeries. I have seen our diet do wonders with diabetics and pre diabetics. The key to our diet is portioned, low sodium, multi meal system.</p>\r\n",
        "block_right_image": "block_right_image_1563353294207_aboutimg.png",
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-07-17T13:12:26.798Z",
        "_id": "5d2ed7b5822fa522c786be07"
    },
    "message": "About page fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.about.content", '/about/content', request_param.any(), async (req, res) => {
    try {
        const success = await teamController.aboutContent(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /team/list Team List
 * @apiVersion 1.0.0
 * @apiGroup About
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "name": "Subhra Bhattacharya",
            "team_image": "team_image_1563292967182_man.png",
            "designation": "Delivery",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-16T13:04:54.782Z",
            "_id": "5d2deeef822fa522c77e9674",
            "__v": 0
        },
        {
            "name": "Micheal Lewis",
            "team_image": "team_image_1563292924756_avatar7.png",
            "designation": "Chief",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-16T13:04:54.782Z",
            "_id": "5d2df4ce822fa522c77edf0e",
            "__v": 0
        },
        {
            "name": "Sarah Lewis",
            "team_image": "team_image_1563292908223_dymmy2.jpg",
            "designation": "Worker",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-07-16T13:04:54.782Z",
            "_id": "5d2df4d5822fa522c77edf46",
            "__v": 0
        }
    ],
    "message": "Team fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.team.list", '/team/list', request_param.any(), async (req, res) => {
    try {
        const success = await teamController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});





// Export the express.Router() instance
module.exports = router;