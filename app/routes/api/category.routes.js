const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const categoryController = require('webservice/category.controller');
const request_param = multer();

/**
 * @api {get} /category/list category List
 * @apiVersion 1.0.0
 * @apiGroup Category
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "title": "Supper123",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-14T09:31:26.317Z",
            "_id": "5d039ff3c1891efa1ae98ffc",
            "__v": 0
        },
        {
            "title": "Sample",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-14T09:31:26.317Z",
            "_id": "5d039ffec1891efa1ae990e9",
            "__v": 0
        },
        {
            "title": "Sample Titlekk",
            "slug": "",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-14T13:12:26.798Z",
            "_id": "5d03a1b1711e791395218566",
            "__v": 0
        }
    ],
    "message": "Category fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/

namedRouter.get("api.category.list", '/category/list', request_param.any(), async (req, res) => {
    try {
        const success = await categoryController.list(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {get} /master/list/:type  Master List
 * @apiVersion 1.0.0
 * @apiParam {string}  type [like/dietary/key_benefits/state]
 * @apiGroup Category
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "title": "Sample Title",
            "key_icon": "key_icon_1560524453452_dymmy2.jpg",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-14T14:59:53.086Z",
            "_id": "5d03b6a5f435d21a267886a0",
            "__v": 0
        },
        {
            "title": "Test Title1232323",
            "key_icon": "key_icon_1560524518216_download_2.png",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-06-14T14:59:53.086Z",
            "_id": "5d03b6c1f435d21a267886a1",
            "__v": 0
        }
    ],
    "message": "key_benefits fetched successfully"
}
*@apiErrorExample {json} Error
{
    "status": 201,
    "data": {},
    "message": "Sorry, No record found."
}
*/
namedRouter.get("api.master.list", '/master/list/:type', request_param.any(), async (req, res) => {
    try {
        const success = await categoryController.typeList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});



// Export the express.Router() instance
module.exports = router;