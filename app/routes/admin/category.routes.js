const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const CategoryController = require('category/controllers/category.controller');

const request_param = multer();

namedRouter.all('/category*', auth.authenticate);

// category Listing Route
namedRouter.get("category.listing", '/category/listing', CategoryController.list);

// category Get All Route
namedRouter.post("category.getall", '/category/getall', async (req, res) => {
    try {
        const success = await CategoryController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// category Create Route
namedRouter.get("category.create", '/category/create', CategoryController.create);

// category Insert Route
namedRouter.post("category.insert", '/category/insert', request_param.any(), CategoryController.insert);

// category Edit Route
namedRouter.get("category.edit", "/category/edit/:id", CategoryController.edit);

// category Update Route
namedRouter.post("category.update", '/category/update', request_param.any(), CategoryController.update);

// category Delete Route
namedRouter.get("category.delete", "/category/delete/:id", CategoryController.delete);

// Export the express.Router() instance
module.exports = router;