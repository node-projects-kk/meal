const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const packageTypeController = require('packageType/controllers/packageType.controller');

const request_param = multer();

namedRouter.all('/package*', auth.authenticate);

// topping Listing Route
namedRouter.get("packageType.listing", '/packageType/listing', packageTypeController.list);

// topping Get All Route
namedRouter.post("package.getall", '/packageType/getall', async (req, res) => {
	
  try {
    const success = await packageTypeController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});



// topping Edit Route
namedRouter.get("packageType.edit", "/packageType/edit/:id", packageTypeController.edit);

// topping Update Route
namedRouter.post("packageType.update", '/packageType/update', request_param.any(), packageTypeController.update);

// topping Delete Route
namedRouter.get("packageType.delete", "/packageType/delete/:id", packageTypeController.delete);

// Export the express.Router() instance
module.exports = router;