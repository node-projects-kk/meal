const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const stateController = require('state/controllers/state.controller');

const request_param = multer();

namedRouter.all('/state*', auth.authenticate);

// state Listing Route
namedRouter.get("state.listing", '/state/listing', stateController.list);

// state Get All Route
namedRouter.post("state.getall", '/state/getall', async (req, res) => {
  try {
    const success = await stateController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
// state Create Route
namedRouter.get("state.create", '/state/create', stateController.create);

// state Insert Route
namedRouter.post("state.insert", '/state/insert', request_param.any(), stateController.insert);

// state Edit Route
namedRouter.get("state.edit", "/state/edit/:id", stateController.edit);

// state Update Route
namedRouter.post("state.update", '/state/update', request_param.any(), stateController.update);

// state Delete Route
namedRouter.get("state.delete", "/state/delete/:id", stateController.delete);

// Export the express.Router() instance
module.exports = router;