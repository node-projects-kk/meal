const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const PlanController = require('plan/controllers/plan.controller');

const request_param = multer();

namedRouter.all('/plan*', auth.authenticate);

// topping Listing Route
namedRouter.get("plan.listing", '/plan/listing', PlanController.list);

// topping Get All Route
namedRouter.post("plan.getall", '/plan/getall', async (req, res) => {
  try {
    const success = await PlanController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
// topping Create Route
namedRouter.get("plan.create", '/plan/create', PlanController.create);

// topping Insert Route
namedRouter.post("plan.insert", '/plan/insert', request_param.any(), PlanController.insert);

// topping Edit Route
namedRouter.get("plan.edit", "/plan/edit/:id", PlanController.edit);

// topping Update Route
namedRouter.post("plan.update", '/plan/update', request_param.any(), PlanController.update);

// topping Delete Route
namedRouter.get("plan.delete", "/plan/delete/:id", PlanController.delete);

// Export the express.Router() instance
module.exports = router;