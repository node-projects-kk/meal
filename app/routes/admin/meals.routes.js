const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const MealController = require('meals/controllers/meals.controller');

const Storage = multer.diskStorage({
  destination: function (req, file, callback) {
    // console.log(file.fieldname)
    if (file.fieldname === 'meal_images') {
      callback(null, "./public/uploads/meal")
    }
    if (file.fieldname === 'block_image') {
      callback(null, "./public/uploads/meal_block")
    }
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
  }
});
const uploadFile = multer({
  storage: Storage,
  fileFilter: function (req, file, cb) {
    if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
      req.fileValidationError = 'Only support jpeg, jpg or png file types.';
      return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
    }
    cb(null, true);
  }
});


const request_param = multer({
  storage: Storage
});

namedRouter.all('/meals*', auth.authenticate);

// meals Listing Route
namedRouter.get("meals.listing", '/meals/listing', MealController.list);

// meals Get All Route
namedRouter.post("meals.getall", '/meals/getall', async (req, res) => {
  try {
    const success = await MealController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
// meals Create Route
namedRouter.get("meals.create", '/meals/create', MealController.create);

// meals Insert Route
namedRouter.post("meals.insert", '/meals/insert', uploadFile.any(), MealController.insert);

// meals Edit Route
namedRouter.get("meals.edit", "/meals/edit/:id", MealController.edit);

// meals Update Route
namedRouter.post("meals.update", '/meals/update', uploadFile.any(), MealController.update);

// meals Delete Route
namedRouter.get("meals.delete", "/meals/delete/:id", MealController.delete);

/*
// @Route: Chapter title verify
*/
namedRouter.post('delete.meal.images', '/delete-meal-images', request_param.any(), async (req, res) => {
  try {
    const success = await MealController.deleteMealImages(req);
    return res.send(success.message);
  } catch (error) {
    res.status(error.status).send(error);
    req.flash('error', error.message);
    // return res.redirect(namedRouter.urlFor('chapter.create'));
  }
});

// Meals Review Route
namedRouter.get("meals.review", '/meals/review/:id', MealController.MealReviewPage);

// Meals Review data Route
namedRouter.post("meals.reviewdata", '/meals/reviewdata/:id', async (req, res) => {
  try {
    const success = await MealController.MealReviewData(req, res);
    res.send({
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});


// Export the express.Router() instance
module.exports = router;