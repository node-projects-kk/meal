const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const TeamController = require('team/controllers/team.controller');

const Storage = multer.diskStorage({
  destination: function (req, file, callback) {
    // console.log(file.fieldname)
    if (file.fieldname === 'team_image') {
      callback(null, "./public/uploads/team")
    }
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
  }
});

const uploadFile = multer({
  storage: Storage,
  fileFilter: function (req, file, cb) {
    if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
      req.fileValidationError = 'Only support jpeg, jpg or png file types.';
      return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
    }
    cb(null, true);
  }
});

const request_param = multer({
  storage: Storage
});


namedRouter.all('/team*', auth.authenticate);

// category Listing Route
namedRouter.get("team.listing", '/team/listing', TeamController.list);

// category Get All Route
namedRouter.post("team.getall", '/team/getall', async (req, res) => {
  try {
    const success = await TeamController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
// category Create Route
// namedRouter.get("category.create", '/category/create', CategoryController.create);

// category Insert Route
// namedRouter.post("category.insert", '/category/insert', request_param.any(), CategoryController.insert);

// category Edit Route
namedRouter.get("team.edit", "/team/edit/:id", TeamController.edit);

// category Update Route
namedRouter.post("team.update", '/team/update', uploadFile.any(), TeamController.update);

// category Delete Route
// namedRouter.get("team.delete", "/team/delete/:id", TeamController.delete);

// Export the express.Router() instance
module.exports = router;