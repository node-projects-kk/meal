const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const dietaryController = require('dietary/controllers/dietary.controller');

const Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // console.log(file.fieldname)
        if (file.fieldname === 'dietary_icon') {
            callback(null, "./public/uploads/dietary")
        }
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});
const uploadFile = multer({
    storage: Storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
            req.fileValidationError = 'Only support jpeg, jpg or png file types.';
            return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
        }
        cb(null, true);
    }
});
const request_param = multer({
    storage: Storage
});


namedRouter.all('/dietary*', auth.authenticate);

/*
// @Route: dietary Listing [Admin]
*/
// dietary Listing Route
namedRouter.get("dietary.listing", '/dietary/listing', dietaryController.list);

// dietary Get All Route
namedRouter.post("dietary.getall", '/dietary/getall', async (req, res) => {
    try {
        const success = await dietaryController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// dietary Create Route
namedRouter.get("dietary.create", '/dietary/create', dietaryController.create);

// dietary Insert Route
namedRouter.post("dietary.insert", '/dietary/insert', uploadFile.any(), dietaryController.insert);

// dietary Edit Route
namedRouter.get("dietary.edit", "/dietary/edit/:id", dietaryController.edit);

// dietary Update Route
namedRouter.post("dietary.update", '/dietary/update', uploadFile.any(), dietaryController.update);

// dietary Delete Route
namedRouter.get("dietary.delete", "/dietary/delete/:id", dietaryController.delete);

// Export the express.Router() instance
module.exports = router;