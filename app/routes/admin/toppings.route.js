const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const ToppingController = require('toppings/controllers/toppings.controller');

const Storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "./public/uploads/book");
  },
  filename: (req, file, callback) => {
    callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
  }
});

const uploadFile = multer({
  storage: Storage
});
const request_param = multer();

namedRouter.all('/toppings*', auth.authenticate);

// topping Listing Route
namedRouter.get("toppings.listing", '/toppings/listing', ToppingController.list);

// topping Get All Route
namedRouter.post("toppings.getall", '/toppings/getall', async (req, res) => {
  try {
    const success = await ToppingController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
// topping Create Route
namedRouter.get("toppings.create", '/toppings/create', ToppingController.create);

// topping Insert Route
namedRouter.post("toppings.insert", '/toppings/insert', request_param.any(), ToppingController.insert);

// topping Edit Route
namedRouter.get("toppings.edit", "/toppings/edit/:id", ToppingController.edit);

// topping Update Route
namedRouter.post("toppings.update", '/toppings/update', request_param.any(), ToppingController.update);

// topping Delete Route
namedRouter.get("toppings.delete", "/toppings/delete/:id", ToppingController.delete);

// Export the express.Router() instance
module.exports = router;