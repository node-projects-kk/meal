const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const couponController = require('coupon/controllers/coupon.controller');
const request_param = multer();

namedRouter.all('/coupon*', auth.authenticate);

// coupon Listing Route
namedRouter.get("coupon.listing", '/coupon/listing', couponController.list);

// coupon Get All Route
namedRouter.post("coupon.getall", '/coupon/getall', async (req, res) => {
  try {
    const success = await couponController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});
// coupon Create Route
namedRouter.get("coupon.create", '/coupon/create', couponController.create);

// coupon Insert Route
namedRouter.post("coupon.insert", '/coupon/insert', request_param.any(), couponController.insert);

namedRouter.get("coupon.statusChange", '/coupon/status-change/:id',request_param.any(),couponController.statusChange);
 // coupon Edit Route
namedRouter.get("coupon.edit", "/coupon/edit/:id", couponController.edit);

// coupon Update Route
namedRouter.post("coupon.update", '/coupon/update', request_param.any(), couponController.update);

// coupon Delete Route
namedRouter.get("coupon.delete", "/coupon/delete/:id", couponController.destroy);

namedRouter.post('coupon.verify', '/verify-coupon', request_param.any(), async (req, res) => {
	try {
		const success = await couponController.verifyCoupon(req);
		return res.send(success.message);
		
	} catch (error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('page.create'));
	}
});
/*
// @Route: Chapter title verify
*/


// Export the express.Router() instance
module.exports = router;