const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const ZipController = require('zip_management/controllers/zip.controller');

const request_param = multer();

namedRouter.all('/zip*', auth.authenticate);

// zip Listing Route
namedRouter.get("zip.listing", '/zip/listing', ZipController.list);

// zip Get All Route
namedRouter.post("zip.getall", '/zip/getall', async (req, res) => {
    try {
        const success = await ZipController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// zip Create Route
namedRouter.get("zip.create", '/zip/create', ZipController.create);

// zip Insert Route
namedRouter.post("zip.insert", '/zip/insert', request_param.any(), ZipController.insert);

// zip Edit Route
namedRouter.get("zip.edit", "/zip/edit/:id", ZipController.edit);

// zip Update Route
namedRouter.post("zip.update", '/zip/update', request_param.any(), ZipController.update);

// zip Delete Route
namedRouter.get("zip.delete", "/zip/delete/:id", ZipController.delete);

// Export the express.Router() instance
module.exports = router;