const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const OrderController = require('order_management/controllers/order.controller');

const request_param = multer();

namedRouter.all('/orders*', auth.authenticate);

// order Listing Route
namedRouter.get("orders.listing", '/orders/listing', OrderController.list);

// order Get All Route
namedRouter.post("orders.getall", '/orders/getall', async (req, res) => {
  try {
    const success = await OrderController.getAll(req, res);
    res.send({
      "meta": success.meta,
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});


// order Edit Route
namedRouter.get("orders.view", "/orders/view/:id", OrderController.view);

// order Update Route
namedRouter.post("orders.update", '/orders/update', request_param.any(), OrderController.update);


namedRouter.post("orders.completed", '/orders/completed', async (req, res) => {
  try {
    const success = await OrderController.autoRenewUserOrder(req, res);
    res.send({
      "data": success.data
    });
  } catch (error) {
    res.status(error.status).send(error);
  }
});



// Export the express.Router() instance
module.exports = router;