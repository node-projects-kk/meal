const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const MealTypesController = require('meal_types/controllers/meal_types.controller');

const Storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, "./public/uploads/book");
    },
    filename: (req, file, callback) => {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});

const uploadFile = multer({
    storage: Storage
});
const request_param = multer();

namedRouter.all('/mealtypes*', auth.authenticate);

// mealtypes Listing Route
namedRouter.get("mealtypes.listing", '/mealtypes/listing', MealTypesController.list);

// mealtypes Get All Route
namedRouter.post("mealtypes.getall", '/mealtypes/getall', async (req, res) => {
    try {
        const success = await MealTypesController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});
// mealtypes Create Route
namedRouter.get("mealtypes.create", '/mealtypes/create', MealTypesController.create);

// mealtypes Insert Route
namedRouter.post("mealtypes.insert", '/mealtypes/insert', request_param.any(), MealTypesController.insert);

// mealtypes Edit Route
namedRouter.get("mealtypes.edit", "/mealtypes/edit/:id", MealTypesController.edit);

// mealtypes Update Route
namedRouter.post("mealtypes.update", '/mealtypes/update', request_param.any(), MealTypesController.update);

// mealtypes Delete Route
namedRouter.get("mealtypes.delete", "/mealtypes/delete/:id", MealTypesController.delete);

// Export the express.Router() instance
module.exports = router;