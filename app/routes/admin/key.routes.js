const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const keyController = require('key_benefits/controllers/key.controller');

const Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // console.log(file.fieldname)
        if (file.fieldname === 'key_icon') {
            callback(null, "./public/uploads/key")
        }

    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});
const uploadFile = multer({
    storage: Storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png' && file.mimetype !== 'image/ico') {
            req.fileValidationError = 'Only support svg,jpeg, jpg or png file types.';
            return cb(null, false, new Error('Only support svg,jpeg, jpg or png file types'));
        }
        cb(null, true);
    }
});
const request_param = multer({
    storage: Storage
});


namedRouter.all('/key*', auth.authenticate);

/*
// @Route: key Listing [Admin]
*/
// key Listing Route
namedRouter.get("key.listing", '/key/listing', keyController.list);

// key Get All Route
namedRouter.post("key.getall", '/key/getall', async (req, res) => {
    try {
        const success = await keyController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// key Create Route
namedRouter.get("key.create", '/key/create', keyController.create);

// key Insert Route
namedRouter.post("key.insert", '/key/insert', uploadFile.any(), keyController.insert);

// key Edit Route
namedRouter.get("key.edit", "/key/edit/:id", keyController.edit);

// key Update Route
namedRouter.post("key.update", '/key/update', uploadFile.any(), keyController.update);

// key Delete Route
namedRouter.get("key.delete", "/key/delete/:id", keyController.delete);

// Export the express.Router() instance
module.exports = router;