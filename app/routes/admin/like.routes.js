const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const likeController = require('like/controllers/like.controller');

const Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // console.log(file.fieldname)
        if (file.fieldname === 'like_icon') {
            callback(null, "./public/uploads/like")
        }

    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
    }
});

const uploadFile = multer({
    storage: Storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
            req.fileValidationError = 'Only support jpeg, jpg or png file types.';
            return cb(null, false, new Error('Only support jpeg, jpg or png file types'));
        }
        cb(null, true);
    }
});

const request_param = multer({
    storage: Storage
});


namedRouter.all('/like*', auth.authenticate);

/*
// @Route: like Listing [Admin]
*/
// like Listing Route
namedRouter.get("like.listing", '/like/listing', likeController.list);

// like Get All Route
namedRouter.post("like.getall", '/like/getall', async (req, res) => {
    try {
        const success = await likeController.getAll(req, res);
        res.send({
            "meta": success.meta,
            "data": success.data
        });
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// like Create Route
namedRouter.get("like.create", '/like/create', likeController.create);

// like Insert Route
namedRouter.post("like.insert", '/like/insert', uploadFile.any(), likeController.insert);

// like Edit Route
namedRouter.get("like.edit", "/like/edit/:id", likeController.edit);

// like Update Route
namedRouter.post("like.update", '/like/update', uploadFile.any(), likeController.update);

// like Delete Route
namedRouter.get("like.delete", "/like/delete/:id", likeController.delete);

// Export the express.Router() instance
module.exports = router;