define({
  "name": "Mass Meal API documentation",
  "version": "1.0.0",
  "description": "API Mass Meal",
  "url": "http://122.15.50.137:1396/api",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-05-28T15:12:55.894Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});