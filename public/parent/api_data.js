define({ "api": [
  {
    "type": "get",
    "url": "/parent/profile",
    "title": "Profile",
    "version": "1.0.0",
    "group": "Parent",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"Parent \",\n        \"last_name\": \"sir\",\n        \"email\": \"parentEmail@gmail.com\",\n        \"password\": \"$2a$08$RcOWAVF7xgwoPAxnafXRJeM5J2Q.Dw4ytDNsAOli8w9cQQRmWMX1.\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"No\",\n        \"verifyToken\": 3190,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced1b05c1e1283f30667e9b\",\n        \"role\": \"5cd56cdac6fff8f0dc8daae7\",\n        \"__v\": 0\n    },\n    \"message\": \"You have successfully regsitered.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/parent.routes.js",
    "groupTitle": "Parent",
    "name": "GetParentProfile"
  },
  {
    "type": "post",
    "url": "/parent/change/password",
    "title": "Change Password",
    "version": "1.0.0",
    "group": "Parent",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "old_password",
            "description": "<p>Old password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "new_password",
            "description": "<p>New password</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"Parent \",\n        \"last_name\": \"sir\",\n        \"email\": \"parent@gmail.com\",\n        \"password\": \"$2a$08$gYO0BRQrD0uEG0wr2CZK5OxrDVdm8fWujDSuTb1sAAiWpkCfpHL6y\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced44553d45955a272c583d\",\n        \"role\": \"5cd56d27c6fff8f0dc8dae69\",\n        \"__v\": 0\n    },\n    \"message\": \"Password Changed Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/parent.routes.js",
    "groupTitle": "Parent",
    "name": "PostParentChangePassword"
  },
  {
    "type": "post",
    "url": "/parent/store",
    "title": "Register",
    "version": "1.0.0",
    "group": "Parent",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "parent_email",
            "description": "<p>Parent Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "child",
            "description": "<p>[{child_name:'',child_email:'',child_password:'',child_confirm_password:''}]</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"Parent \",\n        \"last_name\": \"sir\",\n        \"email\": \"parentEmail@gmail.com\",\n        \"password\": \"$2a$08$RcOWAVF7xgwoPAxnafXRJeM5J2Q.Dw4ytDNsAOli8w9cQQRmWMX1.\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"No\",\n        \"verifyToken\": 3190,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced1b05c1e1283f30667e9b\",\n        \"role\": \"5cd56cdac6fff8f0dc8daae7\",\n        \"__v\": 0\n    },\n    \"message\": \"You have successfully regsitered.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/parent.routes.js",
    "groupTitle": "Parent",
    "name": "PostParentStore"
  },
  {
    "type": "post",
    "url": "/user/email/available",
    "title": "Email Available Check",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          }
        ]
      }
    },
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": true,\n    \"message\": \"Email is already exists.\"\n}",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\n    \"status\": 200,\n    \"data\": false,\n    \"message\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserEmailAvailable"
  },
  {
    "type": "post",
    "url": "/user/login",
    "title": "Login",
    "version": "1.0.0",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZWQyY2E0YTk1MzkxNDdlN2YzOWZjOCIsImlhdCI6MTU1OTA0ODg0NSwiZXhwIjoxNTU5MTM1MjQ1fQ.t-mi86TxPmtVJ5g1LnG7P2xY7hzlKwpSIN1ib40nhOY\",\n    \"data\": {\n        \"first_name\": \"Parent \",\n        \"last_name\": \"sir\",\n        \"email\": \"kuldeep.mishra@webskitters.com\",\n        \"password\": \"$2a$08$uKHFLntNhMOVUmBnUnX3QeYxvWRb/uSskfblgFwl5qTKkS8Qkso4K\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced2ca4a9539147e7f39fc8\",\n        \"role\": {\n            \"desc\": \"Parent of the student.\",\n            \"_id\": \"5cd56cdac6fff8f0dc8daae7\",\n            \"roleDisplayName\": \"Parent\",\n            \"role\": \"parent\",\n            \"id\": \"5cd56cdac6fff8f0dc8daae7\"\n        },\n        \"__v\": 0\n    },\n    \"message\": \"You have successfully logged in\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 200,\n    \"data\":{},\n    \"message\": \"Authentication failed, Wrong Password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserLogin"
  },
  {
    "type": "post",
    "url": "/user/resendOtp",
    "title": "Resend OTP",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          }
        ]
      }
    },
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"Parent \",\n        \"last_name\": \"sir\",\n        \"email\": \"kuldeep.mishra@webskitters.com\",\n        \"password\": \"$2a$08$Sya5fxHAF116siwap4u96OigcAohwSzWtvf4U1N2Qrnz3d4mJp5Zi\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"No\",\n        \"verifyToken\": 2007,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced2ca4a9539147e7f39fc8\",\n        \"role\": \"5cd56cdac6fff8f0dc8daae7\",\n        \"__v\": 0\n    },\n    \"message\": \"A email with new OTP has been sent to your email address.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserResendotp"
  },
  {
    "type": "post",
    "url": "/user/reset-password",
    "title": "Forgot Password",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          }
        ]
      }
    },
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {},\n    \"message\": \"A email with new password has been sent to your email address.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 200,\n    \"data\": {},\n    \"message\": \"No user found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserResetPassword"
  },
  {
    "type": "post",
    "url": "/user/verify",
    "title": "Email Otp Verification",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>User id.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "otp",
            "description": "<p>Otp .</p>"
          }
        ]
      }
    },
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZWQxYjA1YzFlMTI4M2YzMDY2N2U5YiIsImlhdCI6MTU1OTA0MzUyNSwiZXhwIjoxNTU5MTI5OTI1fQ.M-6LByjXyA4QR5mDvSJ5BJ_-rdzVbV7e_7uj5LO3Fi8\",\n    \"data\": {\n        \"first_name\": \"Parent \",\n        \"last_name\": \"sir\",\n        \"email\": \"parentEmail@gmail.com\",\n        \"password\": \"$2a$08$RcOWAVF7xgwoPAxnafXRJeM5J2Q.Dw4ytDNsAOli8w9cQQRmWMX1.\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced1b05c1e1283f30667e9b\",\n        \"role\": \"5cd56cdac6fff8f0dc8daae7\",\n        \"__v\": 0\n    },\n    \"message\": \"Your account is verified succesfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/parent/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserVerify"
  }
] });
