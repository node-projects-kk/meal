"use strict";
// Class definition
var KTDatatableOrders = function () {
  // Private functions
  var options = {
    // datasource definition
    data: {
      type: 'remote',
      source: {
        read: {
          url: `${location.protocol}//${window.location.host}/admin/orders/getall`,
        },
      },
      pageSize: 10,
      serverPaging: true,
      serverFiltering: true,
      serverSorting: true,
    },
    // layout definition
    layout: {
      scroll: true, // enable/disable datatable scroll both horizontal and
      // vertical when needed.
      height: 500, // datatable's body's fixed height
      footer: false // display/hide footer
    },

    // column sorting
    sortable: true,

    pagination: true,

    // columns definition

    columns: [{
        field: 'order_no',
        title: 'Order No',
        sortable: true,
        template: '{{order_no}}',
      },
      {
        field: 'user',
        title: 'User',
        sortable: true,
        template: function (row) {
          if (row.hasOwnProperty('user_info')) {
            return row.user_info.first_name + ' ' + row.user_info.last_name;
          } else {
            return '--';
          }
        }
      },

      {
        field: 'total_price',
        title: 'Total Price($)',
        sortable: true,
        template: '{{total_price}}',
      },
      {
        field: 'order_status',
        title: 'Order Status',
        sortable: true,
        template: '{{order_status}}',
      },
      {
        field: 'payment_status',
        title: 'Payment Status',
        sortable: true,
        template: '{{payment_status}}',
      },
      {
        field: 'Actions',
        title: 'Actions',
        sortable: false,
        width: 110,
        overflow: 'visible',
        textAlign: 'center',
        autoHide: false,
        template: function (row) {
          return '\
                    \<a href="' + location.protocol + "//" + window.location.host + '/admin/orders/view/' + row._id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit">\
                        <i class="flaticon-eye"></i>\
                    </a>\
                ';
        },
      }
    ],
  };

  // basic demo
  var orderRecordSelection = function () {

    options.search = {
      input: $('#generalSearch'),
    };

    var datatable = $('#orderSelection').KTDatatable(options);

    $('#kt_form_status').on('change', function () {
      datatable.search($(this).val(), 'Status');
    });

    $('#kt_form_type').on('change', function () {
      datatable.search($(this).val().toLowerCase(), 'Type');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

    datatable.on(
      'kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
      function (e) {
        var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes();
        var count = checkedNodes.length;
        $('#kt_datatable_selected_number').html(count);
        if (count > 0) {
          $('#kt_datatable_group_action_form').collapse('show');
        } else {
          $('#kt_datatable_group_action_form').collapse('hide');
        }
      });

    $('#kt_modal_fetch_id').on('show.bs.modal', function (e) {
      var ids = datatable.rows('.kt-datatable__row--active').
      nodes().
      find('.kt-checkbox--single > [type="checkbox"]').
      map(function (i, chk) {
        return $(chk).val();
      });
      var c = document.createDocumentFragment();
      for (var i = 0; i < ids.length; i++) {
        var li = document.createElement('li');
        li.setAttribute('data-id', ids[i]);
        li.innerHTML = 'Selected record ID: ' + ids[i];
        c.appendChild(li);
      }
      $(e.target).find('.kt-datatable_selected_ids').append(c);
    }).on('hide.bs.modal', function (e) {
      $(e.target).find('.kt-datatable_selected_ids').empty();
    });

    $(document).on('click', '.ktDelete', function () {
      var elemID = $(this).attr('id').replace('del-', '');
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          window.location.href = `${location.protocol}//${window.location.host}/admin/orders/delete/${elemID}`;
        }
      });
    });
  };



  return {
    // public functions
    init: function () {
      orderRecordSelection();
    },
  };
}();

jQuery(document).ready(function () {
  KTDatatableOrders.init();
});