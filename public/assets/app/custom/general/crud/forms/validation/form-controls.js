// Class definition

var KTFormControls = function () {
    // Private functions

    var myProfileValidation = function () {
        $("#frmMyProfile").validate({
            // define validation rules
            rules: {
                first_name: {
                    required: true,
                    letterswithbasicpunc: true
                },
                last_name: {
                    required: true,
                    letterswithbasicpunc: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter your first name",
                    letterswithbasicpunc: "Please enter alphabets only"
                },
                last_name: {
                    required: "Please enter your last name",
                    letterswithbasicpunc: "Please enter alphabets only"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createcouponValidation = function () {
        $("#createcouponValidation").validate({
            // define validation rules
            rules: {
                coupon_code: {
                    required: true
                },
                amount: {
                    required: true
                },
                coupon_count: {
                    required: true
                },
                coupon_description: {
                    required: true
                },
                expiry_date: {
                    required: true
                }
            },
            messages: {
                coupon_code: {
                    required: "Please enter your coupon code"
                },
                amount: {
                    required: "Please enter discount"
                },
                coupon_count: {
                    required: "Please enter coupon count"
                },
                coupon_description: {
                    required: "Please enter description"
                },
                expiry_date: {
                    required: "Please select expiry date"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editcouponValidation = function () {
        $("#editcouponValidation").validate({
            // define validation rules
            rules: {
                coupon_code: {
                    required: true
                },
                amount: {
                    required: true
                },
                coupon_count: {
                    required: true
                },
                coupon_description: {
                    required: true
                },
                expiry_date: {
                    required: true
                }
            },
            messages: {
                coupon_code: {
                    required: "Please enter your coupon code"
                },
                amount: {
                    required: "Please enter discount"
                },
                coupon_count: {
                    required: "Please enter coupon count"
                },
                coupon_description: {
                    required: "Please enter description"
                },
                expiry_date: {
                    required: "Please select expiry date"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var changePasswordValidation = function () {
        $("#changePasswordForm").validate({
            // define validation rules
            rules: {
                old_password: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirm: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                old_password: {
                    required: "Please enter your old password",
                },
                password: {
                    required: "Please enter your new password",
                },
                password_confirm: {
                    required: "Make sure that you have entered the same password here.",
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createMealTypeValidation = function () {
        // alert('hgfd');
        $("#CreateMealType").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editMealTypeValidation = function () {
        // alert('hgfd');
        $("#EditMealType").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createMealsValidation = function () {
        // alert('hgfd');
        $("#CreateMeals").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                meal_type: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                price: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                category_id: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                meal_images: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|svg|x-ms-bmp"
                },

            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                meal_type: {
                    required: "Please select meal type"
                },
                price: {
                    required: "Please enter price"
                },
                category_id: {
                    required: "Please select category"
                },
                meal_images: {
                    required: "Please upload an image"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editMealsValidation = function () {
        // alert('hgfd');
        $("#EditMeals").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                meal_type: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                price: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                category_id: {
                    required: true,
                    // letterswithbasicpunc: true
                },
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                meal_type: {
                    required: "Please select meal type"
                },
                price: {
                    required: "Please enter price"
                },
                category_id: {
                    required: "Please select category"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createToppingsValidation = function () {
        // alert('hgfd');
        $("#createToppingsValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                meal: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                price: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                meal_type: {
                    required: "Please select meal"
                },
                price: {
                    required: "Please enter price"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editToppingsValidation = function () {
        // alert('hgfd');
        $("#editToppingsValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                meal: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                price: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                meal_type: {
                    required: "Please select meal"
                },
                price: {
                    required: "Please enter price"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editUserValidation = function () {
        // alert('hgfd');
        $("#editToppingsValidation").validate({

            // define validation rules
            rules: {
                first_name: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                last_name: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                email: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                phone: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter firstname"
                },
                last_name: {
                    required: "Please enter lastname"
                },
                email: {
                    required: "Please enter email"
                },
                phone: {
                    required: "Please enter phone"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var CreateZipCodeValidation = function () {
        // alert('hgfd');
        $("#CreateZipCode").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter zip code"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var EditZipCodeValidation = function () {
        // alert('hgfd');
        $("#EditZipCode").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter zip code"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var CreateCategoryValidation = function () {
        // alert('hgfd');
        $("#CreateCategory").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter category name"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var EditCategoryValidation = function () {
        // alert('hgfd');
        $("#EditCategory").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter category name"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createKeyValidation = function () {
        // alert('hgfd');
        $("#createKeyValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                key_icon: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|svg|x-ms-bmp"
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter Key Benefits Title"
                },
                key_icon: {
                    required: "Please upload an image"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editKeyValidation = function () {
        // alert('hgfd');
        $("#editKeyValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                // key_icon: {
                //     required: true,
                //     extension: "jpg|jpeg|png|gif|x-ms-bmp"
                //     // letterswithbasicpunc: true
                // }
            },
            messages: {
                title: {
                    required: "Please enter Key Benefits Title"
                },
                // key_icon: {
                //     required: "Please upload an image"
                // }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createDietaryValidation = function () {
        // alert('hgfd');
        $("#createDietaryValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                dietary_icon: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter dietary name"
                },
                dietary_icon: {
                    required: "Please upload an image"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editDietaryValidation = function () {
        // alert('hgfd');
        $("#editDietaryValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
            },
            messages: {
                title: {
                    required: "Please enter dietary name"
                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createLikeValidation = function () {
        // alert('hgfd');
        $("#createLikeValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                like_icon: {
                    required: true,
                    extension: "jpg|jpeg|png|gif|x-ms-bmp"
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                key_icon: {
                    required: "Please upload an image"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editLikeValidation = function () {
        // alert('hgfd');
        $("#editLikeValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },

            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                // key_icon: {
                //     required: "Please upload an image"
                // }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var createPlansValidation = function () {
        // alert('hgfd');
        $("#createPlansValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                box: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                price: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                tag: {
                    required: false,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please select plan type"
                },
                box: {
                    required: "Please enter box count"
                },
                price: {
                    required: "Please enter price"
                },
                tag: {
                    required: "Please select tag"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var editPlansValidation = function () {
        // alert('hgfd');
        $("#editPlansValidation").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                box: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                price: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                tag: {
                    required: false,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please select plan type"
                },
                box: {
                    required: "Please enter box count"
                },
                price: {
                    required: "Please enter price"
                },
                tag: {
                    required: "Please select tag"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var CreateStateValidation = function () {
        // alert('hgfd');
        $("#CreateState").validate({

            // define validation rules
            rules: {
                name: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                short_name: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                name: {
                    required: "Please enter state name"
                },
                short_name: {
                    required: "Please enter state code"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var EditStateValidation = function () {
        // alert('hgfd');
        $("#EditState").validate({

            // define validation rules
            rules: {
                name: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                short_name: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                name: {
                    required: "Please enter state name"
                },
                short_name: {
                    required: "Please enter state code"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var EditAboutValidation = function () {
        // alert('hgfd');
        $("#editAboutValidation").validate({

            // define validation rules
            rules: {
                main_heading: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                block_title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                main_heading: {
                    required: "Please enter heading"
                },
                block_title: {
                    required: "Please enter content title"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var EditTeamValidation = function () {
        // alert('hgfd');
        $("#editTeamValidation").validate({

            // define validation rules
            rules: {
                name: {
                    required: true,
                    // letterswithbasicpunc: true
                },
                designation: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                designation: {
                    required: "Please enter designation"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }

    var EditCMSValidation = function () {
        // alert('hgfd');
        $("#frmEditCMS").validate({

            // define validation rules
            rules: {
                title: {
                    required: true,
                    // letterswithbasicpunc: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit();
            }
        });
    }


    var blankSpaceNotAllow = function () {
        $("input").on("keypress", function (e) {
            var startPos = e.currentTarget.selectionStart;
            if (e.which === 32 && startPos == 0)
                e.preventDefault();
        })
    }

    return {
        // public functions
        init: function () {
            myProfileValidation();
            createcouponValidation();
            editcouponValidation();
            createMealTypeValidation();
            editMealTypeValidation();
            createMealsValidation();
            editMealsValidation();
            createToppingsValidation();
            editToppingsValidation();
            editUserValidation();
            CreateZipCodeValidation();
            EditZipCodeValidation();
            CreateCategoryValidation();
            EditCategoryValidation();
            createKeyValidation();
            editKeyValidation();
            createDietaryValidation();
            editDietaryValidation();
            createLikeValidation();
            editLikeValidation();
            createPlansValidation();
            editPlansValidation();
            CreateStateValidation();
            EditStateValidation();
            changePasswordValidation();
            EditAboutValidation();
            EditTeamValidation();
            EditCMSValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTFormControls.init();
});