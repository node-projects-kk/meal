define({
  "name": "Mass Meal API documentation",
  "version": "1.0.0",
  "description": "API Mass Meal",
  "url": "http://122.15.50.137:1396/api",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-05-31T10:21:57.332Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});