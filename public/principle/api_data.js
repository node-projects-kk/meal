define({ "api": [
  {
    "type": "post",
    "url": "/principle/change/password",
    "title": "Change Password",
    "version": "1.0.0",
    "group": "Principle",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "old_password",
            "description": "<p>Old password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "new_password",
            "description": "<p>New password</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"principle \",\n        \"last_name\": \"sir\",\n        \"email\": \"principle@gmail.com\",\n        \"password\": \"$2a$08$gYO0BRQrD0uEG0wr2CZK5OxrDVdm8fWujDSuTb1sAAiWpkCfpHL6y\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced44553d45955a272c583d\",\n        \"role\": \"5cd56d27c6fff8f0dc8dae69\",\n        \"__v\": 0\n    },\n    \"message\": \"Password Changed Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/principle/principle.routes.js",
    "groupTitle": "Principle",
    "name": "PostPrincipleChangePassword"
  },
  {
    "type": "post",
    "url": "/principle/store",
    "title": "Register",
    "version": "1.0.0",
    "group": "Principle",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "school",
            "description": "<p>School.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"Principle \",\n        \"last_name\": \"sir\",\n        \"email\": \"principleSir@gmail.com\",\n        \"password\": \"$2a$08$7Vg98coDmDO1M3OFf6iZseJ9EgyhVvEyoQhZCqHR4wz6u957LqCsq\",\n        \"teacher_id\": null,\n        \"parent_id\": null,\n        \"principle_id\": null,\n        \"school_name\": \"\",\n        \"isVerified\": \"No\",\n        \"verifyToken\": 1048,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5ced489d122e445d98606a59\",\n        \"role\": \"5cd56d4cc6fff8f0dc8db02f\",\n        \"__v\": 0\n    },\n    \"message\": \"You have successfully regsitered.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/principle/principle.routes.js",
    "groupTitle": "Principle",
    "name": "PostPrincipleStore"
  }
] });
