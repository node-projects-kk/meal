//meal section multi select
$('#topping_ids, #key_ids, #dietary_ids, #like_ids').select2({
    placeholder: "Select your choices",
});

//meal add page multiple field
$(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper

    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function () {
        var fieldHTML = '<div><div class="form-group row col-lg-12"><label>Heading:</label><input type="text" name="heading[]" class="form-control" placeholder="Enter Title" value=""/></div><div class="form-group row col-lg-12"><label>Description:</label><textarea name="desc[]" id="content' + x + '" class="ckeditor form-control" placeholder="Enter Description"></textarea></div><a href="javascript:void(0);" class="remove_button">Remove</a></div>'; //New input field html 
        //Check maximum number of input fields
        if (x < maxField) {
            $(wrapper).append(fieldHTML); //Add field html
            // CKEDITOR.replace('content', {
            //     toolbar: 'Basic'
            // });
            CKEDITOR.replace('content' + x, {});
            x++; //Increment field counter
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});


$('.close_btn_pic').click(function (e) {
    $(this).parent('div').remove();
});


function deleteMealImage(image) {
    // alert("came");
    var apiBaseURL = `${window.location.origin}/admin`;
    var id = $("#mid").val(); // value in field email
    // alert('id' + ' ' + id);
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/delete-meal-images", // put your real file name 
        data: {
            id: id,
            image: image
        },
        success: function (msg) {
            // if (msg) {
            //     document.getElementById('imgMsg').style.display = 'block';
            // } else {
            //     document.getElementById('imgMsg').style.display = 'none';
            // }
        }
    });
}

function getOrderId(status) {
    // alert(status);
    var apiBaseURL = `${window.location.origin}/admin`;
    var id = $("#orderid").val(); // value in field email
    var nextOrderStatus = $("#next_order_status").val(); // value in field email

    $.ajax({
        type: 'post',
        url: apiBaseURL + "/orders/completed", // put your real file name 
        data: {
            id: id,
            status: status,
            next_order_status: nextOrderStatus
        },

        success: function (msg) {
            if (msg) {
                toastr.success('Order status has been updated.', 'Success!', {
                    positionClass: 'toast-top-right'
                });
            }
        }
    });
}


$(function () {
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;
    $('#expiry_date').attr('min', maxDate);
});

function couponVerify() {
    var code = $('#coupon_code').val();
    var apiBaseURL = `${window.location.origin}/admin`;
    $.ajax({
        type: 'post',
        url: apiBaseURL + "/verify-coupon", // put your real file name 
        data: {
            code: code
        },
        success: function (msg) {
            if (msg) {
                $('#coupon_code').val("");
                document.getElementById('coupon-message').style.display = 'block';
                return false;
            } else {
                document.getElementById('coupon-message').style.display = 'none';
            }
        }
    });
}

function couponVerify2(c) {
    var code = $('#coupon_code').val();
    if (c != code) {
        var apiBaseURL = `${window.location.origin}/admin`;
        $.ajax({
            type: 'post',
            url: apiBaseURL + "/verify-coupon", // put your real file name 
            data: {
                code: code
            },
            success: function (msg) {
                if (msg) {
                    $('#coupon_code').val(c);
                    document.getElementById('coupon-message').style.display = 'block';
                    return false;
                } else {
                    document.getElementById('coupon-message').style.display = 'none';
                }
            }
        });
    }
}