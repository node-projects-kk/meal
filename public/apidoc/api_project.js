define({
  "name": "Mass Meal API documentation",
  "version": "1.0.0",
  "description": "API Mass Meal",
  "url": "http://111.93.167.180:1415/api",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-09-30T08:31:31.897Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
