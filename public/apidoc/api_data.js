define({ "api": [
  {
    "type": "get",
    "url": "/about/content",
    "title": "About Content",
    "version": "1.0.0",
    "group": "About",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"main_heading\": \"About Us\",\n        \"block_title\": \"Who we are?\",\n        \"block_content\": \"<p>Mass Meals was not originally created to be a company. It was created to save my fathers life. My life was rocked when my father had a heart attack. Doctors told me he needed a heart and a kidney transplant. In order to get on these lists to receive these surgeries he had to loose 85lbs to be eligible. With the doctors, nutritionist at Stanford Hospital and my knowledge of food we created what is now called The Mass Meals Program. My father was also insulin dependent diabetic, so salt was an issue. We created a low sodium multi meal diet to help speed up metabolism. This diet is created to loose stored fat while building lean muscle mass. With in three months we reached our goal of weight loss and reversed the need of a kidney transplant strictly with the diet. As a result of the meal plan he was able to pull and lessen up on medication t. I then applied the diet to myself, friends, family, only to receive amazing results.</p>\\r\\n\\r\\n<p>We have put together simple ingredients, process, and program to really help people loose weight, put on lean muscle, control sugar levels, and even reverse surgeries. I have seen our diet do wonders with diabetics and pre diabetics. The key to our diet is portioned, low sodium, multi meal system.</p>\\r\\n\",\n        \"block_right_image\": \"block_right_image_1563353294207_aboutimg.png\",\n        \"status\": \"Active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-07-17T13:12:26.798Z\",\n        \"_id\": \"5d2ed7b5822fa522c786be07\"\n    },\n    \"message\": \"About page fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/about.routes.js",
    "groupTitle": "About",
    "name": "GetAboutContent"
  },
  {
    "type": "get",
    "url": "/team/list",
    "title": "Team List",
    "version": "1.0.0",
    "group": "About",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"name\": \"Subhra Bhattacharya\",\n            \"team_image\": \"team_image_1563292967182_man.png\",\n            \"designation\": \"Delivery\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-16T13:04:54.782Z\",\n            \"_id\": \"5d2deeef822fa522c77e9674\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Micheal Lewis\",\n            \"team_image\": \"team_image_1563292924756_avatar7.png\",\n            \"designation\": \"Chief\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-16T13:04:54.782Z\",\n            \"_id\": \"5d2df4ce822fa522c77edf0e\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Sarah Lewis\",\n            \"team_image\": \"team_image_1563292908223_dymmy2.jpg\",\n            \"designation\": \"Worker\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-16T13:04:54.782Z\",\n            \"_id\": \"5d2df4d5822fa522c77edf46\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"Team fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/about.routes.js",
    "groupTitle": "About",
    "name": "GetTeamList"
  },
  {
    "type": "get",
    "url": "/cms/:slug",
    "title": "CMS Content",
    "version": "1.0.0",
    "group": "CMS",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Pass slug in params</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"title\": \"Privacy Policy\",\n        \"slug\": \"privacy-policy\",\n        \"content\": \"<h2>Where does it come from?</h2>\\r\\n\\r\\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\\r\\n\\r\\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\\r\\n\\r\\n<h2>Why do we use it?</h2>\\r\\n\\r\\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\\r\\n\",\n        \"isDeleted\": false,\n        \"status\": \"Active\",\n        \"_id\": \"5d69396a780477ef567771e7\"\n    },\n    \"message\": \"Data Fetched Successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/cms.routes.js",
    "groupTitle": "CMS",
    "name": "GetCmsSlug"
  },
  {
    "type": "get",
    "url": "/category/list",
    "title": "category List",
    "version": "1.0.0",
    "group": "Category",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"title\": \"Supper123\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n            \"_id\": \"5d039ff3c1891efa1ae98ffc\",\n            \"__v\": 0\n        },\n        {\n            \"title\": \"Sample\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n            \"_id\": \"5d039ffec1891efa1ae990e9\",\n            \"__v\": 0\n        },\n        {\n            \"title\": \"Sample Titlekk\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-14T13:12:26.798Z\",\n            \"_id\": \"5d03a1b1711e791395218566\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"Category fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/category.routes.js",
    "groupTitle": "Category",
    "name": "GetCategoryList"
  },
  {
    "type": "get",
    "url": "/master/list/:type",
    "title": "Master List",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>[like/dietary/key_benefits/state]</p>"
          }
        ]
      }
    },
    "group": "Category",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"title\": \"Sample Title\",\n            \"key_icon\": \"key_icon_1560524453452_dymmy2.jpg\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-14T14:59:53.086Z\",\n            \"_id\": \"5d03b6a5f435d21a267886a0\",\n            \"__v\": 0\n        },\n        {\n            \"title\": \"Test Title1232323\",\n            \"key_icon\": \"key_icon_1560524518216_download_2.png\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-14T14:59:53.086Z\",\n            \"_id\": \"5d03b6c1f435d21a267886a1\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"key_benefits fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/category.routes.js",
    "groupTitle": "Category",
    "name": "GetMasterListType"
  },
  {
    "type": "get",
    "url": "/contact/info",
    "title": "Get Contact Info",
    "version": "1.0.0",
    "group": "Contact",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"setting_name\": \"Site Title\",\n            \"setting_slug\": \"site-title\",\n            \"setting_value\": \"Mass Meal\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"_id\": \"5cffb9e3c1891efa1aa89096\"\n        },\n        {\n            \"setting_name\": \"Location\",\n            \"setting_slug\": \"location\",\n            \"setting_value\": \"20936 Mission Blvd, Hayward, CA 94541, USA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"_id\": \"5d2dd25a822fa522c77d19cc\"\n        },\n        {\n            \"setting_name\": \"Phone\",\n            \"setting_slug\": \"phone\",\n            \"setting_value\": \"+1 510-398-8997\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"_id\": \"5d2dd2bb822fa522c77d1d8e\"\n        },\n        {\n            \"setting_name\": \"Facebook\",\n            \"setting_slug\": \"facebook\",\n            \"setting_value\": \"https//:www.facebook.com\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"_id\": \"5d2dd2fd822fa522c77d206a\"\n        },\n        {\n            \"setting_name\": \"Twitter\",\n            \"setting_slug\": \"twitter\",\n            \"setting_value\": \"https//:www.twitter.com\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"_id\": \"5d2dd312822fa522c77d217d\"\n        },\n        {\n            \"setting_name\": \"Instagram\",\n            \"setting_slug\": \"instagram\",\n            \"setting_value\": \"https//:www.instagram.com\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"_id\": \"5d2dd323822fa522c77d222d\"\n        }\n    ],\n    \"message\": \"Contact information fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/contact.routes.js",
    "groupTitle": "Contact",
    "name": "GetContactInfo"
  },
  {
    "type": "post",
    "url": "/contact/submit",
    "title": "Contact Submit",
    "version": "1.0.0",
    "group": "Contact",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Full Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>Phone Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email_id",
            "description": "<p>Email Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"name\": \"Subhra Bhattacharya\",\n        \"phone_number\": \"7896541230\",\n        \"email_id\": \"subhra.bhattacharya@webskitters.com\",\n        \"message\": \"Hello ! This is for test purpose.\",\n        \"status\": \"Active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-07-16T13:04:54.782Z\",\n        \"_id\": \"5d2dcc184126d914d947621b\",\n        \"__v\": 0\n    },\n    \"message\": \"Thank you. Your message has been posted successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/contact.routes.js",
    "groupTitle": "Contact",
    "name": "PostContactSubmit"
  },
  {
    "type": "get",
    "url": "/coupon/:coupon_code/:total",
    "title": "Get Coupon",
    "version": "1.0.0",
    "group": "Coupon",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>Coupon Code</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "total",
            "description": "<p>Total Price</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"name\": \"Subhra Bhattacharya\",\n        \"phone_number\": \"7896541230\",\n        \"email_id\": \"subhra.bhattacharya@webskitters.com\",\n        \"message\": \"Hello ! This is for test purpose.\",\n        \"status\": \"Active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-07-16T13:04:54.782Z\",\n        \"_id\": \"5d2dcc184126d914d947621b\",\n        \"__v\": 0\n    },\n    \"message\": \"Thank you. Your message has been posted successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/coupons.routes.js",
    "groupTitle": "Coupon",
    "name": "GetCouponCoupon_codeTotal"
  },
  {
    "type": "post",
    "url": "/coupon/apply",
    "title": "Apply Coupon",
    "version": "1.0.0",
    "group": "Coupon",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>Coupon Code</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "order_total",
            "description": "<p>Total Order Price</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"total\": \"1000\",\n        \"discounted_amount\": 875\n    },\n    \"message\": \"Coupon applied successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"message\": \"Coupon usage limit has been reached.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/coupons.routes.js",
    "groupTitle": "Coupon",
    "name": "PostCouponApply"
  },
  {
    "type": "get",
    "url": "/meal/types",
    "title": "All Meal Types",
    "version": "1.0.0",
    "group": "Meal_Types",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"title\": \"Breakfast\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-10T17:39:03.380Z\",\n            \"_id\": \"5cfe44f5c1891efa1a9123d3\",\n            \"__v\": 0\n        },\n        {\n            \"title\": \"Dinner\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-10T12:20:49.646Z\",\n            \"_id\": \"5cfe4b5b43886608752e09e9\",\n            \"__v\": 0\n        },\n        {\n            \"title\": \"Lunch\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n            \"_id\": \"5d036aadb407ab5b64ac40ac\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"Meal Types fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/mealtype.routes.js",
    "groupTitle": "Meal_Types",
    "name": "GetMealTypes"
  },
  {
    "type": "get",
    "url": "/mealtype/:id",
    "title": "Get Meals Based on Type",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": "<p>Pass Meal Type Id</p>"
          }
        ]
      }
    },
    "group": "Meal_Types",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d13664a3050bb0eb3b89853\",\n            \"title\": \"Sample Title\",\n            \"meal_images\": [\n                \"meal_images_1561552458352_new_dummy.jpeg\",\n                \"meal_images_1561552458502_new1.jpg\",\n                \"meal_images_1561552458504_test.jpg\"\n            ],\n            \"content\": \"<p>Brief Description</p>\\r\\n\",\n            \"contains_text\": [\n                \"asd\",\n                \"qwerty\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"h1\",\n                    \"desc\": \"<p>c1</p>\\r\\n\",\n                    \"_id\": \"5d13664a3050bb0eb3b89855\"\n                },\n                {\n                    \"heading\": \"h2\",\n                    \"desc\": \"c2\",\n                    \"_id\": \"5d13664a3050bb0eb3b89854\"\n                }\n            ],\n            \"price\": \"10\",\n            \"taste_text\": \"Mouth Watering\",\n            \"block_image\": \"block_image_1561552458508_autumn.jpg\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-26T12:30:08.314Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5cfe44f5c1891efa1a9123d3\",\n                \"title\": \"Breakfast\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-10T17:39:03.380Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [\n                {\n                    \"_id\": \"5d03708db407ab5b64ac40b2\",\n                    \"title\": \"Sauce\",\n                    \"price\": 10.5,\n                    \"slug\": \"\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-14T09:31:26.292Z\",\n                    \"__v\": 0\n                },\n                {\n                    \"_id\": \"5cff84c311cb740c27fec5d8\",\n                    \"title\": \"Extra Dal\",\n                    \"price\": 11,\n                    \"slug\": \"\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-11T10:38:00.701Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"category_details\": {\n                \"_id\": \"5d039ffec1891efa1ae990e9\",\n                \"title\": \"Soups\",\n                \"slug\": \"soups\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"like_details\": [\n                {\n                    \"_id\": \"5d03ceedd937b53bcc1dc7ac\",\n                    \"title\": \"Sample Title\",\n                    \"like_icon\": \"like_icon_1560530668029_dummy1.png\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-14T16:42:33.093Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"key_details\": [\n                {\n                    \"_id\": \"5d0891c35db2832b613282d6\",\n                    \"title\": \"test-title\",\n                    \"key_icon\": \"key_icon_1560842690764_blog1.jpeg\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-18T06:53:32.725Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"dietary_details\": [\n                {\n                    \"_id\": \"5d03c6ae74e2d71f1b14848d\",\n                    \"title\": \"Test Title lk\",\n                    \"dietary_icon\": \"dietary_icon_1560528558138_dummy1.png\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-14T16:01:27.342Z\",\n                    \"__v\": 0\n                }\n            ]\n        }\n    ],\n    \"message\": \"Meals fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/mealtype.routes.js",
    "groupTitle": "Meal_Types",
    "name": "GetMealtypeId"
  },
  {
    "type": "get",
    "url": "/orders/cancel/:id",
    "title": "Order Cancel",
    "version": "1.0.0",
    "group": "Order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "id",
            "description": "<p>Pass Order Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n    { \n        'id': 're_1FFKlYCae2wvBG9BQUbRybR6',\n        'object': 'refund',\n        'amount': 50000,\n        'balance_transaction': 'txn_1FFKlYCae2wvBG9Bjy95FiKD',\n        'charge': 'ch_1FFKlDCae2wvBG9B1thxlmAp',\n        'created': 1567689168,\n        'currency': 'usd',\n        'metadata': {},\n        'reason': null,\n        'receipt_number': null,\n        'source_transfer_reversal': null,\n        'status': 'succeeded',\n        'transfer_reversal': null \n    }\n    \"message\": \"Refund process initiated. Please wait sometime, you will receive an email from the Payment Gateway.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/order.routes.js",
    "groupTitle": "Order",
    "name": "GetOrdersCancelId"
  },
  {
    "type": "get",
    "url": "/orders/history",
    "title": "User Order History",
    "version": "1.0.0",
    "group": "Order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d6f8ca6b8a9b52be4038d43\",\n            \"order_no\": \"k6CWWRchpT\",\n            \"shipping_address\": {\n                \"first_name\": \"Test\",\n                \"last_name\": \"test\",\n                \"address\": \"test address\",\n                \"apt_suite\": \"test area\",\n                \"city\": \"test city\",\n                \"state\": \"Florida\",\n                \"phone\": \"2123123131\",\n                \"company_name\": \"\"\n            },\n            \"coupon\": {\n                \"coupon_code\": \"\",\n                \"coupon_description\": \"\",\n                \"amount\": 0,\n                \"discount_type\": \"flat\"\n            },\n            \"deliver_arrive_on\": \"Thu, Aug 22\",\n            \"deliver_day\": \"Thursday\",\n            \"total_price\": 360,\n            \"discounted_price\": 0,\n            \"createdAt\": \"2019-09-04T10:04:15.437Z\",\n            \"order_details\": [\n                {\n                    \"quantity\": 1,\n                    \"meal_price\": 30,\n                    \"meal_id\": \"5d3eef492bcc0d074c675c93\",\n                    \"meal_info\": {\n                        \"_id\": \"5d3eef492bcc0d074c675c93\",\n                        \"title\": \"Pesto Cod white Rice and Veggies\",\n                        \"meal_images\": [\n                            \"meal_images_1564405577414_cod__hero.jpg\",\n                            \"meal_images_1564405577415_recipe-image-legacy.jpg\"\n                        ],\n                        \"topping_id\": [],\n                        \"category_id\": [\n                            \"5d039fdac1891efa1ae98e41\"\n                        ],\n                        \"like_id\": [],\n                        \"taste_text\": \"\",\n                        \"content\": \"<p>Wild caught cod seasoned in a pesto sauce with a side of freshly made white rice and a mixture of broccoli, carrots and cauliflower.</p>\\r\\n\",\n                        \"key_id\": [],\n                        \"dietary_id\": [],\n                        \"contains_text\": [\n                            \"\"\n                        ],\n                        \"block_image\": \"\",\n                        \"price\": 30,\n                        \"slug\": \"\",\n                        \"status\": \"Active\",\n                        \"isDeleted\": false,\n                        \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n                        \"meal_type\": \"5d3ee5db2bcc0d074c675c74\",\n                        \"detail_blocks\": [\n                            {\n                                \"heading\": \"Ingredients & Nutrition Facts\",\n                                \"desc\": \"<p><strong>Ingredients: </strong>Fish, Cod, Pacific, Raw, White Rice, Broccoli, Cauliflower, Carrots, Olive Oil, Basil, Garlic, Asiago Cheese (Pasturized Milk and Cream, Cheese Cultures, Salt, Enzymes), Sugar, Pine Nuts, Contains, Less Than 1% Lactic Acid, Salt, Spice and Sodium Benzoate</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g),&nbsp; Calories per serving: 330. Total Fat 13g &ndash; 17%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 55Mg &ndash; 18%,<strong>&nbsp;</strong>Sodium 450mg - 20% .&nbsp; Vitamin D 0.6mcg - 2%, Calcium 40mg - 4%. Total Carb 32g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. &lt;1 g added Sugars - 1%, Protein 22g. Iron 4.7mg - 25% . Potassium 480mg - 10%.</p>\\r\\n\",\n                                \"_id\": \"5d512bf9ef08b20e4944d8d5\"\n                            }\n                        ],\n                        \"__v\": 0\n                    },\n                    \"category_info\": [\n                        {\n                            \"_id\": \"5d039fdac1891efa1ae98e41\",\n                            \"title\": \"Sample\",\n                            \"slug\": \"sample\",\n                            \"status\": \"Active\",\n                            \"isDeleted\": false,\n                            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                            \"__v\": 0\n                        }\n                    ]\n                },\n                {\n                    \"quantity\": 1,\n                    \"meal_price\": 30,\n                    \"meal_id\": \"5d3eed7d2bcc0d074c675c8b\",\n                    \"meal_info\": {\n                        \"_id\": \"5d3eed7d2bcc0d074c675c8b\",\n                        \"title\": \"Steak Fajita \",\n                        \"meal_images\": [\n                            \"meal_images_1564405117009_beef_fajitas.jpg\",\n                            \"meal_images_1564405117012_steak-fajitas-horizontal.jpg\"\n                        ],\n                        \"topping_id\": [],\n                        \"category_id\": [\n                            \"5d039fdac1891efa1ae98e41\"\n                        ],\n                        \"like_id\": [],\n                        \"taste_text\": \"\",\n                        \"content\": \"<p>Marinated tri tip with a side of rice topped with grilled bell peppers and onions.</p>\\r\\n\",\n                        \"key_id\": [],\n                        \"dietary_id\": [],\n                        \"contains_text\": [\n                            \"\"\n                        ],\n                        \"block_image\": \"\",\n                        \"price\": 30,\n                        \"slug\": \"\",\n                        \"status\": \"Active\",\n                        \"isDeleted\": false,\n                        \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n                        \"meal_type\": \"5d3ee5b12bcc0d074c675c72\",\n                        \"detail_blocks\": [\n                            {\n                                \"heading\": \"Ingredients & Nutrition Facts\",\n                                \"desc\": \"<p><strong>Ingredients:&nbsp; </strong>Tri - Tip, Fajita Peppers, White Rice</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 360. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 80mg - 3% . Vitamin D 0mcg - 0%, Calcium 40mg - 2%. Total Carb 32g - 12%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%,&nbsp; Protein 33g. Iron 3.8mg - 20% . Potassium 560mg - 10%.</p>\\r\\n\",\n                                \"_id\": \"5d512a2bef08b20e4944d8d0\"\n                            }\n                        ],\n                        \"__v\": 0\n                    },\n                    \"category_info\": [\n                        {\n                            \"_id\": \"5d039fdac1891efa1ae98e41\",\n                            \"title\": \"Sample\",\n                            \"slug\": \"sample\",\n                            \"status\": \"Active\",\n                            \"isDeleted\": false,\n                            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                            \"__v\": 0\n                        }\n                    ]\n                },\n                {\n                    \"quantity\": 1,\n                    \"meal_price\": 30,\n                    \"meal_id\": \"5d3eeacc2bcc0d074c675c81\",\n                    \"meal_info\": {\n                        \"_id\": \"5d3eeacc2bcc0d074c675c81\",\n                        \"title\": \"Teriyaki Chicken, White Rice and Broccoli\",\n                        \"meal_images\": [\n                            \"meal_images_1564404428659_Slow_Cooker_Chicken_Teriyaki-2-600x400.jpg\",\n                            \"meal_images_1564404428661_terichick-1.jpg\"\n                        ],\n                        \"topping_id\": [],\n                        \"category_id\": [\n                            \"5d039fdac1891efa1ae98e41\"\n                        ],\n                        \"like_id\": [],\n                        \"taste_text\": \"\",\n                        \"content\": \"<p>Chicken thigh marinated in a teriyaki sauce with a side of white rice and broccoli.</p>\\r\\n\",\n                        \"key_id\": [],\n                        \"dietary_id\": [],\n                        \"contains_text\": [\n                            \"\"\n                        ],\n                        \"block_image\": \"\",\n                        \"price\": 30,\n                        \"slug\": \"\",\n                        \"status\": \"Active\",\n                        \"isDeleted\": false,\n                        \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n                        \"meal_type\": \"5d3ee59f2bcc0d074c675c70\",\n                        \"detail_blocks\": [\n                            {\n                                \"heading\": \"Ingredients & Nutrition Facts\",\n                                \"desc\": \"<p><strong>Ingredients:&nbsp; </strong>Chicken Thigh, White Rice, Broccoli, Scallions, Pineapple, Mirin SweetCooking Honteri, Mirin, Liquid Amino Acids, Ginger, Liquid Amino Acids</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(0.0g), Calories per serving: 360. Total Fat 10g &ndash; 13%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 150Mg &ndash; 50%, Sodium 250mg - 11% . Vitamin D 0.2mcg 0%, Calcium 60mg 4%.&nbsp; Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 4g, Incl.&nbsp; 1 g added Sugars - 2%, Protein 33g.&nbsp;Iron 3.2mg - 20% . Potassium 610mg - 15%.</p>\\r\\n\",\n                                \"_id\": \"5d51296fef08b20e4944d8ca\"\n                            }\n                        ],\n                        \"__v\": 0\n                    },\n                    \"category_info\": [\n                        {\n                            \"_id\": \"5d039fdac1891efa1ae98e41\",\n                            \"title\": \"Sample\",\n                            \"slug\": \"sample\",\n                            \"status\": \"Active\",\n                            \"isDeleted\": false,\n                            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                            \"__v\": 0\n                        }\n                    ]\n                }\n            ]\n        }\n    ],\n    \"message\": \"Your orders fetched successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/order.routes.js",
    "groupTitle": "Order",
    "name": "GetOrdersHistory"
  },
  {
    "type": "get",
    "url": "/orders/historyDetails/:id",
    "title": "Order History Details",
    "version": "1.0.0",
    "group": "Order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "id",
            "description": "<p>Pass Order Id</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d6f8ca6b8a9b52be4038d43\",\n            \"order_no\": \"P8CWhRchpT\",\n            \"shipping_address\": {\n                \"first_name\": \"Test\",\n                \"last_name\": \"test\",\n                \"address\": \"test address\",\n                \"apt_suite\": \"test area\",\n                \"city\": \"test city\",\n                \"state\": \"Florida\",\n                \"phone\": \"2123123131\",\n                \"company_name\": \"\"\n            },\n            \"coupon\": {\n                \"coupon_code\": \"\",\n                \"coupon_description\": \"\",\n                \"amount\": 0,\n                \"discount_type\": \"flat\"\n            },\n            \"deliver_arrive_on\": \"Thu, Aug 22\",\n            \"deliver_day\": \"Thursday\",\n            \"total_price\": 360,\n            \"discounted_price\": 0,\n            \"createdAt\": \"2019-09-04T10:04:15.437Z\",\n            \"order_status\": \"Completed\",\n            \"order_details\": [\n                {\n                    \"quantity\": 1,\n                    \"meal_price\": 30,\n                    \"meal_id\": \"5d3eef492bcc0d074c675c93\",\n                    \"meal_info\": {\n                        \"_id\": \"5d3eef492bcc0d074c675c93\",\n                        \"title\": \"Pesto Cod white Rice and Veggies\",\n                        \"meal_images\": [\n                            \"meal_images_1564405577414_cod__hero.jpg\",\n                            \"meal_images_1564405577415_recipe-image-legacy.jpg\"\n                        ],\n                        \"topping_id\": [],\n                        \"category_id\": [\n                            \"5d039fdac1891efa1ae98e41\"\n                        ],\n                        \"like_id\": [],\n                        \"taste_text\": \"\",\n                        \"content\": \"<p>Wild caught cod seasoned in a pesto sauce with a side of freshly made white rice and a mixture of broccoli, carrots and cauliflower.</p>\\r\\n\",\n                        \"key_id\": [],\n                        \"dietary_id\": [],\n                        \"contains_text\": [\n                            \"\"\n                        ],\n                        \"block_image\": \"\",\n                        \"price\": 30,\n                        \"slug\": \"\",\n                        \"status\": \"Active\",\n                        \"isDeleted\": false,\n                        \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n                        \"meal_type\": \"5d3ee5db2bcc0d074c675c74\",\n                        \"detail_blocks\": [\n                            {\n                                \"heading\": \"Ingredients & Nutrition Facts\",\n                                \"desc\": \"<p><strong>Ingredients: </strong>Fish, Cod, Pacific, Raw, White Rice, Broccoli, Cauliflower, Carrots, Olive Oil, Basil, Garlic, Asiago Cheese (Pasturized Milk and Cream, Cheese Cultures, Salt, Enzymes), Sugar, Pine Nuts, Contains, Less Than 1% Lactic Acid, Salt, Spice and Sodium Benzoate</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g),&nbsp; Calories per serving: 330. Total Fat 13g &ndash; 17%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 55Mg &ndash; 18%,<strong>&nbsp;</strong>Sodium 450mg - 20% .&nbsp; Vitamin D 0.6mcg - 2%, Calcium 40mg - 4%. Total Carb 32g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. &lt;1 g added Sugars - 1%, Protein 22g. Iron 4.7mg - 25% . Potassium 480mg - 10%.</p>\\r\\n\",\n                                \"_id\": \"5d512bf9ef08b20e4944d8d5\"\n                            }\n                        ],\n                        \"__v\": 0\n                    },\n                    \"category_info\": [\n                        {\n                            \"_id\": \"5d039fdac1891efa1ae98e41\",\n                            \"title\": \"Sample\",\n                            \"slug\": \"sample\",\n                            \"status\": \"Active\",\n                            \"isDeleted\": false,\n                            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                            \"__v\": 0\n                        }\n                    ]\n                },\n                {\n                    \"quantity\": 1,\n                    \"meal_price\": 30,\n                    \"meal_id\": \"5d3eed7d2bcc0d074c675c8b\",\n                    \"meal_info\": {\n                        \"_id\": \"5d3eed7d2bcc0d074c675c8b\",\n                        \"title\": \"Steak Fajita \",\n                        \"meal_images\": [\n                            \"meal_images_1564405117009_beef_fajitas.jpg\",\n                            \"meal_images_1564405117012_steak-fajitas-horizontal.jpg\"\n                        ],\n                        \"topping_id\": [],\n                        \"category_id\": [\n                            \"5d039fdac1891efa1ae98e41\"\n                        ],\n                        \"like_id\": [],\n                        \"taste_text\": \"\",\n                        \"content\": \"<p>Marinated tri tip with a side of rice topped with grilled bell peppers and onions.</p>\\r\\n\",\n                        \"key_id\": [],\n                        \"dietary_id\": [],\n                        \"contains_text\": [\n                            \"\"\n                        ],\n                        \"block_image\": \"\",\n                        \"price\": 30,\n                        \"slug\": \"\",\n                        \"status\": \"Active\",\n                        \"isDeleted\": false,\n                        \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n                        \"meal_type\": \"5d3ee5b12bcc0d074c675c72\",\n                        \"detail_blocks\": [\n                            {\n                                \"heading\": \"Ingredients & Nutrition Facts\",\n                                \"desc\": \"<p><strong>Ingredients:&nbsp; </strong>Tri - Tip, Fajita Peppers, White Rice</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 360. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 80mg - 3% . Vitamin D 0mcg - 0%, Calcium 40mg - 2%. Total Carb 32g - 12%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%,&nbsp; Protein 33g. Iron 3.8mg - 20% . Potassium 560mg - 10%.</p>\\r\\n\",\n                                \"_id\": \"5d512a2bef08b20e4944d8d0\"\n                            }\n                        ],\n                        \"__v\": 0\n                    },\n                    \"category_info\": [\n                        {\n                            \"_id\": \"5d039fdac1891efa1ae98e41\",\n                            \"title\": \"Sample\",\n                            \"slug\": \"sample\",\n                            \"status\": \"Active\",\n                            \"isDeleted\": false,\n                            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                            \"__v\": 0\n                        }\n                    ]\n                },\n                {\n                    \"quantity\": 1,\n                    \"meal_price\": 30,\n                    \"meal_id\": \"5d3eeacc2bcc0d074c675c81\",\n                    \"meal_info\": {\n                        \"_id\": \"5d3eeacc2bcc0d074c675c81\",\n                        \"title\": \"Teriyaki Chicken, White Rice and Broccoli\",\n                        \"meal_images\": [\n                            \"meal_images_1564404428659_Slow_Cooker_Chicken_Teriyaki-2-600x400.jpg\",\n                            \"meal_images_1564404428661_terichick-1.jpg\"\n                        ],\n                        \"topping_id\": [],\n                        \"category_id\": [\n                            \"5d039fdac1891efa1ae98e41\"\n                        ],\n                        \"like_id\": [],\n                        \"taste_text\": \"\",\n                        \"content\": \"<p>Chicken thigh marinated in a teriyaki sauce with a side of white rice and broccoli.</p>\\r\\n\",\n                        \"key_id\": [],\n                        \"dietary_id\": [],\n                        \"contains_text\": [\n                            \"\"\n                        ],\n                        \"block_image\": \"\",\n                        \"price\": 30,\n                        \"slug\": \"\",\n                        \"status\": \"Active\",\n                        \"isDeleted\": false,\n                        \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n                        \"meal_type\": \"5d3ee59f2bcc0d074c675c70\",\n                        \"detail_blocks\": [\n                            {\n                                \"heading\": \"Ingredients & Nutrition Facts\",\n                                \"desc\": \"<p><strong>Ingredients:&nbsp; </strong>Chicken Thigh, White Rice, Broccoli, Scallions, Pineapple, Mirin SweetCooking Honteri, Mirin, Liquid Amino Acids, Ginger, Liquid Amino Acids</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(0.0g), Calories per serving: 360. Total Fat 10g &ndash; 13%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 150Mg &ndash; 50%, Sodium 250mg - 11% . Vitamin D 0.2mcg 0%, Calcium 60mg 4%.&nbsp; Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 4g, Incl.&nbsp; 1 g added Sugars - 2%, Protein 33g.&nbsp;Iron 3.2mg - 20% . Potassium 610mg - 15%.</p>\\r\\n\",\n                                \"_id\": \"5d51296fef08b20e4944d8ca\"\n                            }\n                        ],\n                        \"__v\": 0\n                    },\n                    \"category_info\": [\n                        {\n                            \"_id\": \"5d039fdac1891efa1ae98e41\",\n                            \"title\": \"Sample\",\n                            \"slug\": \"sample\",\n                            \"status\": \"Active\",\n                            \"isDeleted\": false,\n                            \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                            \"__v\": 0\n                        }\n                    ]\n                }\n            ]\n        }\n    ],\n    \"message\": \"Order details fetched successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/order.routes.js",
    "groupTitle": "Order",
    "name": "GetOrdersHistorydetailsId"
  },
  {
    "type": "get",
    "url": "/state/list",
    "title": "State List",
    "version": "1.0.0",
    "group": "Order",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"name\": \"Alabama\",\n            \"short_name\": \"AL\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be392\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Alaska\",\n            \"short_name\": \"AK\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be394\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Arizona\",\n            \"short_name\": \"AZ\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be396\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Arkansas\",\n            \"short_name\": \"AR\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be398\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"California\",\n            \"short_name\": \"CA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be39a\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Colorado\",\n            \"short_name\": \"CO\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be39c\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Connecticut\",\n            \"short_name\": \"CT\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be39e\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Delaware\",\n            \"short_name\": \"DE\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3a0\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Florida\",\n            \"short_name\": \"FL\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3a2\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Georgia\",\n            \"short_name\": \"GA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3a4\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Hawaii\",\n            \"short_name\": \"HI\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3a6\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Idaho\",\n            \"short_name\": \"ID\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3a8\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Illinois\",\n            \"short_name\": \"IL\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3aa\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Indiana\",\n            \"short_name\": \"IN\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ac\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Iowa\",\n            \"short_name\": \"IA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ae\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Kansas\",\n            \"short_name\": \"KS\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3b0\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Kentucky\",\n            \"short_name\": \"KY\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3b2\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Louisiana\",\n            \"short_name\": \"LA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3b4\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Maine\",\n            \"short_name\": \"ME\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3b6\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Maryland\",\n            \"short_name\": \"MD\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3b8\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Massachusetts\",\n            \"short_name\": \"MA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ba\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Michigan\",\n            \"short_name\": \"MI\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3bc\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Minnesota\",\n            \"short_name\": \"MN\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3be\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Mississippi\",\n            \"short_name\": \"MS\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3c0\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Missouri\",\n            \"short_name\": \"MO\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3c2\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Montana\",\n            \"short_name\": \"MT\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3c4\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Nebraska\",\n            \"short_name\": \"NE\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3c6\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Nevada\",\n            \"short_name\": \"NV\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3c8\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"New Hampshire\",\n            \"short_name\": \"NH\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ca\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"New Jersey\",\n            \"short_name\": \"NJ\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3cc\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"New Mexico\",\n            \"short_name\": \"NM\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ce\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"New York\",\n            \"short_name\": \"NY\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3d0\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"North Carolina\",\n            \"short_name\": \"NC\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3d2\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"North Dakota\",\n            \"short_name\": \"ND\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3d5\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Ohio\",\n            \"short_name\": \"OH\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3d7\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Oklahoma\",\n            \"short_name\": \"OK\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3d9\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Oregon\",\n            \"short_name\": \"OR\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3db\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Pennsylvania\",\n            \"short_name\": \"PA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3dd\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Rhode Island\",\n            \"short_name\": \"RI\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3df\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"South Carolina\",\n            \"short_name\": \"SC\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3e1\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"South Dakota\",\n            \"short_name\": \"SD\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3e3\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Tennessee\",\n            \"short_name\": \"TN\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3e5\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Texas\",\n            \"short_name\": \"TX\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3e7\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Utah\",\n            \"short_name\": \"UT\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ea\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Vermont\",\n            \"short_name\": \"VT\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ec\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Virginia\",\n            \"short_name\": \"VA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3ee\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Washington\",\n            \"short_name\": \"WA\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3f0\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"West Virginia\",\n            \"short_name\": \"WV\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3f2\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Wisconsin\",\n            \"short_name\": \"WI\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3f4\",\n            \"__v\": 0\n        },\n        {\n            \"name\": \"Wyoming\",\n            \"short_name\": \"WY\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-06-25T11:37:35.847Z\",\n            \"_id\": \"5d0a3c79c1891efa1a6be3f6\",\n            \"__v\": 0\n        }\n    ],\n    \"message\": \"States fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/states.routes.js",
    "groupTitle": "Order",
    "name": "GetStateList"
  },
  {
    "type": "post",
    "url": "/order/place",
    "title": "Order Place",
    "version": "1.0.0",
    "group": "Order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token (Only for logged in Users)</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "total_price",
            "description": "<p>Order Total Price</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "discounted_price",
            "description": "<p>Discounted Price</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "coupon_code",
            "description": "<p>Coupon Code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apt_suite",
            "description": "<p>Aparment, Suite</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>State</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Company Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email (Only for new users)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password (Only for new users)</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "deliver_arrive_on",
            "description": "<p>Delivery Date (example: YYYY-MM-DD)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "deliver_day",
            "description": "<p>Day of the week (example: Thursday)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "card_number",
            "description": "<p>Card Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "exp_month",
            "description": "<p>Expiry Month</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "exp_year",
            "description": "<p>Expiry Year</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cvc",
            "description": "<p>CVV</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "post_code",
            "description": "<p>Postal Code</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "order_details",
            "description": "<p>Order Details (example: order_details[0][meal_id]:5d11e60017334d0b52666155 order_details[0][quantity]:1 order_details[0][meal_price]:700 order_details[0][topping_details][0][topping_id]:5cff69e3c1891efa1aa27120 order_details[0][topping_details][0][price]:75)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"shipping_address\": {\n            \"first_name\": \"Ayan\",\n            \"last_name\": \"Chakraborty\",\n            \"address\": \"Tollygounge\",\n            \"apt_suite\": \"test apt\",\n            \"city\": \"Kolkata\",\n            \"state\": \"WB\",\n            \"phone\": \"7894561236\",\n            \"company_name\": \"WTS LTD\"\n        },\n        \"order_no\": \"G5OG20H4IA\",\n        \"total_price\": \"2000\",\n        \"card_number\": \"4242424242424242\",\n        \"exp_month\": \"04\",\n        \"exp_year\": \"2020\",\n        \"cvc\": \"890\",\n        \"post_code\": \"74152\",\n        \"deliver_arrive_on\": \"Sat, Jun  29\",\n        \"deliver_day\": \"Thursday\",\n        \"order_status\": \"Pending\",\n        \"payment_status\": \"Pending\",\n        \"status\": \"Active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-06-26T12:44:34.996Z\",\n        \"_id\": \"5d13698b6019f70f0eb13e35\",\n        \"order_details\": [\n            {\n                \"quantity\": 1,\n                \"meal_price\": \"700\",\n                \"meal_id\": \"5d11e60017334d0b52666155\",\n                \"topping_details\": [\n                    {\n                        \"topping_id\": \"5cff69e3c1891efa1aa27120\",\n                        \"price\": \"75\"\n                    },\n                    {\n                        \"topping_id\": \"5cff84c311cb740c27fec5d8\",\n                        \"price\": \"11\"\n                    }\n                ]\n            },\n            {\n                \"quantity\": 1,\n                \"meal_price\": \"700\",\n                \"meal_id\": \"5d13664a3050bb0eb3b89853\",\n                \"topping_details\": [\n                    {\n                        \"topping_id\": \"5d07419b74186a02526b1e8f\",\n                        \"price\": \"75\"\n                    },\n                    {\n                        \"topping_id\": \"5d00f5f0b89eb6111e86261a\",\n                        \"price\": \"100\"\n                    }\n                ]\n            }\n        ],\n        \"user_id\": \"5d1369856019f70f0eb13e34\",\n        \"__v\": 0\n    },\n    \"message\": \"Order placed successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/order.routes.js",
    "groupTitle": "Order",
    "name": "PostOrderPlace"
  },
  {
    "type": "get",
    "url": "/plan/list",
    "title": "Plan List",
    "version": "1.0.0",
    "group": "Plan",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d8dd1b47cd8672d9a104338\",\n            \"title\": \"Lean\",\n            \"description\": \"Package 4oz Protein Meals\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-09-27T09:08:38.166Z\",\n            \"__v\": 0,\n            \"package_details\": [\n                {\n                    \"_id\": \"5d91acc32773de1afb520cc6\",\n                    \"box\": 12,\n                    \"price\": \"100\",\n                    \"tag\": \"MostPopular\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-09-30T07:19:29.303Z\",\n                    \"packageType_id\": \"5d8dd1b47cd8672d9a104338\",\n                    \"__v\": 0\n                },\n                ...\n            ]\n        },\n        {\n            \"_id\": \"5d8dd2a4ff64f15dffefe1f7\",\n            \"title\": \"Athletic\",\n            \"description\": \"Package 6oz Protein Meals\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-09-27T09:08:38.166Z\",\n            \"__v\": 0,\n            \"package_details\": [\n                {\n                    \"_id\": \"5d91b7f47fd5eb97ce9cb6ca\",\n                    \"box\": 20,\n                    \"price\": \"200\",\n                    \"tag\": \"MostPopular\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-09-30T07:19:29.303Z\",\n                    \"packageType_id\": \"5d8dd2a4ff64f15dffefe1f7\",\n                    \"__v\": 0\n                },\n                ...\n            ]\n        },\n        {\n            \"_id\": \"5d8dd2a4ff64f15dffefe1f9\",\n            \"title\": \"Bulk\",\n            \"description\": \"Package 8ozProtein Mealss1\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-09-27T09:08:38.166Z\",\n            \"__v\": 0,\n            \"package_details\": [\n                {\n                    \"_id\": \"5d91b82c7fd5eb97ce9cb806\",\n                    \"box\": 25,\n                    \"price\": \"250\",\n                    \"tag\": \"MostPopular\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-09-30T07:19:29.303Z\",\n                    \"packageType_id\": \"5d8dd2a4ff64f15dffefe1f9\",\n                    \"__v\": 0\n                },\n                ...\n            ]\n        }\n    ],\n    \"message\": \"Plan fetched successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/plan.routes.js",
    "groupTitle": "Plan",
    "name": "GetPlanList"
  },
  {
    "type": "get",
    "url": "/product/bestseller/list",
    "title": "Bestseller Product List",
    "version": "1.0.0",
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d09d65819672c02668c6500\",\n            \"meals\": {\n                \"_id\": \"5d09d65819672c02668c6500\",\n                \"title\": \"test\",\n                \"meal_images\": [\n                    \"meal_images_1560925782885_dymmy2.jpg\",\n                    \"meal_images_1560925782917_man.png\"\n                ],\n                \"topping_id\": [\n                    \"5cff84c311cb740c27fec5d8\",\n                    \"5d03708db407ab5b64ac40b2\"\n                ],\n                \"category_id\": [\n                    \"5d03a1b1711e791395218566\"\n                ],\n                \"like_id\": [\n                    \"5d03ceedd937b53bcc1dc7ac\",\n                    \"5d03bec6c1891efa1aebde0a\"\n                ],\n                \"taste_text\": \"Mouth Watering\",\n                \"content\": \"<p>Brief Description</p>\",\n                \"key_id\": [\n                    \"5d0891c35db2832b613282d6\",\n                    \"5d03b6c1f435d21a267886a1\"\n                ],\n                \"dietary_id\": [\n                    \"5d03c4e674e2d71f1b14848c\",\n                    \"5d03c6ae74e2d71f1b14848d\"\n                ],\n                \"contains_text\": [\n                    \"asd\",\n                    \"qwer\",\n                    \"fgre\"\n                ],\n                \"block_image\": \"block_image_1560925782922_download_1.png\",\n                \"price\": \"5.50\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-19T06:14:23.682Z\",\n                \"meal_type\": \"5cfe44f5c1891efa1a9123d3\",\n                \"detail_blocks\": [\n                    {\n                        \"heading\": \"h1\",\n                        \"desc\": \"<p>c1</p>\",\n                        \"_id\": \"5d09d65819672c02668c6503\"\n                    },\n                    {\n                        \"heading\": \"h2\",\n                        \"desc\": \"c2\",\n                        \"_id\": \"5d09d65819672c02668c6502\"\n                    },\n                    {\n                        \"heading\": \"h3\",\n                        \"desc\": \"c3\",\n                        \"_id\": \"5d09d65819672c02668c6501\"\n                    }\n                ],\n                \"__v\": 0\n            },\n            \"category\": \"Lattes\",\n            \"itemsale\": 2\n        },\n        {\n            \"_id\": \"5d079db6de3abb0d9a3cde64\",\n            \"meals\": {\n                \"_id\": \"5d079db6de3abb0d9a3cde64\",\n                \"title\": \"Chicken Roast\",\n                \"meal_images\": [],\n                \"topping_id\": [\n                    \"5d03708db407ab5b64ac40b2\"\n                ],\n                \"category_id\": [\n                    \"5d039ffec1891efa1ae990e9\"\n                ],\n                \"taste_text\": \"Mouth Watering\",\n                \"content\": \"<p>Sample Content</p>\\r\\n\",\n                \"key_id\": [\n                    \"5d03b6a5f435d21a267886a0\"\n                ],\n                \"dietary_id\": [\n                    \"5d03c6ae74e2d71f1b14848d\"\n                ],\n                \"like_id\": [\n                    \"5d03bec6c1891efa1aebde0a\",\n                    \"5d03ceedd937b53bcc1dc7ac\"\n                ],\n                \"contains_text\": [],\n                \"block_image\": \"\",\n                \"price\": \"500\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-17T13:18:41.019Z\",\n                \"meal_type\": \"5cfe4b5b43886608752e09e9\",\n                \"detail_blocks\": [],\n                \"__v\": 0\n            },\n            \"category\": \"Soups\",\n            \"itemsale\": 1\n        }\n    ],\n    \"message\": \"Best sell product list fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/product.routes.js",
    "groupTitle": "Product",
    "name": "GetProductBestsellerList"
  },
  {
    "type": "get",
    "url": "/product/details/:id",
    "title": "Product Details",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "objectid",
            "optional": false,
            "field": "id",
            "description": "<p>Product Id</p>"
          }
        ]
      }
    },
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d3eed362bcc0d074c675c89\",\n            \"title\": \"Tri Tip, Brown Rice and Squash\",\n            \"meal_images\": [\n                \"meal_images_1565691302808_IMG_1435.jpg\"\n            ],\n            \"content\": \"<p>Slow cooked marinated tri tip with a side of brown and seasoned squash.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Tri - Tip, Zucchini, Brown Rice<br />\\r\\n<br />\\r\\n<strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(277g), Calories per serving: 410. Total Fat 13g &ndash; 17%, Sat. Fat 4.5g &ndash; 23%, Trans Fat 0g, Cholest. 130Mg &ndash; 40%,&nbsp;Sodium 105mg - 5% . Vitamin D 0mcg 0%, Calcium 60mg - 4%. Total Carb 23g - 8%, Fiber 2g - 7%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 49g. Iron 3.6mg - 20% . Potassium 900mg - 20%.</p>\\r\\n\",\n                    \"_id\": \"5d528da93063170c27ee9412\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5b12bcc0d074c675c72\",\n                \"title\": \"Beef\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"review_details\": [\n                {\n                    \"_id\": \"5d5bcfb349afef01433af84d\",\n                    \"user_id\": [\n                        {\n                            \"_id\": \"5d4ab2d748a8fe34c9b98ef7\",\n                            \"first_name\": \"Test\",\n                            \"last_name\": \"Test\",\n                            \"email\": \"sann@yopmail.com\",\n                            \"phone\": \"01234567890\",\n                            \"password\": \"$2a$08$SMvNVmf1NS/bB2wZZqgFe.EtZ62SPAb0SBGReuMWcB.bBxAQTJKKK\",\n                            \"isNewUser\": false,\n                            \"isVerified\": \"Yes\",\n                            \"verifyToken\": null,\n                            \"deviceToken\": \"\",\n                            \"deviceType\": \"\",\n                            \"isDeleted\": false,\n                            \"isActive\": true,\n                            \"role\": \"5cfe19dec1891efa1a8e2193\",\n                            \"createdAt\": \"2019-08-07T11:15:35.213Z\",\n                            \"updatedAt\": \"2019-08-07T13:23:37.482Z\",\n                            \"__v\": 0\n                        }\n                    ],\n                    \"meal_id\": \"5d3eed362bcc0d074c675c89\",\n                    \"title\": \"Delicious\",\n                    \"comment\": \"Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. \",\n                    \"rating\": 5,\n                    \"createdAt\": \"2019-08-19T08:13:49.497Z\",\n                    \"isDeleted\": false,\n                    \"__v\": 0\n                },\n                {\n                    \"_id\": \"5d5a5bc349156e065ea391e1\",\n                    \"user_id\": [\n                        {\n                            \"_id\": \"5d4ab2d748a8fe34c9b98ef7\",\n                            \"first_name\": \"Test\",\n                            \"last_name\": \"Test\",\n                            \"email\": \"sann@yopmail.com\",\n                            \"phone\": \"01234567890\",\n                            \"password\": \"$2a$08$SMvNVmf1NS/bB2wZZqgFe.EtZ62SPAb0SBGReuMWcB.bBxAQTJKKK\",\n                            \"isNewUser\": false,\n                            \"isVerified\": \"Yes\",\n                            \"verifyToken\": null,\n                            \"deviceToken\": \"\",\n                            \"deviceType\": \"\",\n                            \"isDeleted\": false,\n                            \"isActive\": true,\n                            \"role\": \"5cfe19dec1891efa1a8e2193\",\n                            \"createdAt\": \"2019-08-07T11:15:35.213Z\",\n                            \"updatedAt\": \"2019-08-07T13:23:37.482Z\",\n                            \"__v\": 0\n                        }\n                    ],\n                    \"meal_id\": \"5d3eed362bcc0d074c675c89\",\n                    \"title\": \"Delicious\",\n                    \"comment\": \"Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. \",\n                    \"rating\": 4,\n                    \"createdAt\": \"2019-08-19T08:13:49.497Z\",\n                    \"isDeleted\": false,\n                    \"__v\": 0\n                }\n            ]\n        }\n    ],\n    \"message\": \"product details fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/product.routes.js",
    "groupTitle": "Product",
    "name": "GetProductDetailsId"
  },
  {
    "type": "post",
    "url": "/product/list",
    "title": "Product List",
    "version": "1.0.0",
    "group": "Product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Search by Category Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "like",
            "description": "<p>Search by Likes</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "dislike",
            "description": "<p>Search by Dislike</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "benefit",
            "description": "<p>Search by Benefits</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "dietary",
            "description": "<p>Search by Dietary</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [\n        {\n            \"_id\": \"5d5a9168de10d058d2f5cc03\",\n            \"title\": \"mojiti\",\n            \"meal_images\": [\n                \"meal_images_1566216552371_mojito-cocktails.jpg\"\n            ],\n            \"content\": \"<p>superb</p>\\r\\n\",\n            \"contains_text\": [\n                \"all good and healthy ingredents\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"mojito1\",\n                    \"desc\": \"<p>yummy</p>\\r\\n\",\n                    \"_id\": \"5d5a9168de10d058d2f5cc04\"\n                }\n            ],\n            \"price\": \"12\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-08-19T09:06:41.616Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5ea2bcc0d074c675c76\",\n                \"title\": \"No Carb\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [\n                {\n                    \"_id\": \"5d00f5f0b89eb6111e86261a\",\n                    \"title\": \"Test Title123\",\n                    \"price\": \"5.50\",\n                    \"slug\": \"\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-12T12:33:20.607Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [\n                {\n                    \"_id\": \"5d15d612f03a11075ef76756\",\n                    \"title\": \"Test Title\",\n                    \"key_icon\": \"key_icon_1561712146459_dummy1.png\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-28T08:43:06.435Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"like_details\": [\n                {\n                    \"_id\": \"5d03ceedd937b53bcc1dc7ac\",\n                    \"title\": \"Sample Title\",\n                    \"like_icon\": \"like_icon_1560530668029_dummy1.png\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-14T16:42:33.093Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"dietary_details\": [\n                {\n                    \"_id\": \"5d03c6ae74e2d71f1b14848d\",\n                    \"title\": \"Test Title lk\",\n                    \"dietary_icon\": \"dietary_icon_1560528558138_dummy1.png\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-14T16:01:27.342Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d53bb7f0325346e8aaf0e47\",\n            \"title\": \"Chicken Thigh with Bacon wrapped Asparagus\",\n            \"meal_images\": [\n                \"meal_images_1565768575522_IMG_1433.jpg\"\n            ],\n            \"content\": \"\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Asparagus<strong>, </strong>Chicken Thigh, Turkey Bacon (Mechanically Separated Turkey, Turkey, Turkey Water, Sugar, Contains 2% or Less Salt, Potassium Lactate, Natural Smoke Flavor, Flavor(Canola Oil, Natural Smoke, Natural Flavouring), Sodium Diacetate, Sodium Phosphate, Rosemary Extract, Sodium Erythorbate, Sodium Nitrite).</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g), Calories per serving: 330. Total Fat 17g &ndash; 22%, Sat. Fat 5g &ndash; 25%, Trans Fat 0g, Cholest. 180Mg &ndash; 60%,&nbsp;Sodium 780mg - 34% . Vitamin D 0.3mcg - 2%, Calcium 40mg - 2%. Total Carb 6g - 2%, Fiber 2g - 7%, Total Sugars 1g, Incl. 0 g added Sugars - 0%, Protein 39g.&nbsp;Iron 2.9mg - 15% . Potassium 670mg - 15%.</p>\\r\\n\",\n                    \"_id\": \"5d53bb7f0325346e8aaf0e48\"\n                }\n            ],\n            \"price\": \"30\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-08-14T07:25:55.233Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee59f2bcc0d074c675c70\",\n                \"title\": \"Chicken Meals\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3ef0792bcc0d074c675c9b\",\n            \"title\": \"Tri Tip with Mixed Veggies\",\n            \"meal_images\": [\n                \"meal_images_1564405881505_tri_tip.jpg\",\n                \"meal_images_1564405881524_tri-tip-roast.jpg\"\n            ],\n            \"content\": \"\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Sri Tip, Cauliflower, Cheddar Cheese, Turkey Bacon(Mechanically Separated Turkey, Turkey, Turkey Water, Sugar, Contains 2% or Less Salt, Potassium Lactate, Natural Smoke Flavor, Flavor(Canola Oil, Natural Smoke, Natural Flavouring), Sodium Diacetate, Sodium Phosphate, Rosemary Extract, Sodium Erythorbate, Sodium Nitrite)</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g), Calories per serving: 340. Total Fat 18g &ndash; 23%, Sat. Fat 7g &ndash; 35%, Trans Fat 0g, Cholest. 115Mg &ndash; 38%,&nbsp;Sodium 490mg - 21% . Vitamin D 0.1mcg - 0%, Calcium 140mg - 10%. Total Carb 6g - 2%, Fiber 3g - 11%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 40g. Iron 2.7mg - 15% . Potassium 590mg - 15%.</p>\\r\\n\",\n                    \"_id\": \"5d512ce1ef08b20e4944d8dd\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5ea2bcc0d074c675c76\",\n                \"title\": \"No Carb\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3ef0222bcc0d074c675c99\",\n            \"title\": \"Chicken pesto with zucchini boats\",\n            \"meal_images\": [\n                \"meal_images_1564405794145_Chicken-Pesto-Zucchini-Boats.jpg\",\n                \"meal_images_1564405794237_DSC_0012-3-1.jpg\"\n            ],\n            \"content\": \"\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Zucchini<strong>, </strong>Chicken, Cheddar Cheese, Pesto(Canola Oil, Basil, Parmesan&nbsp; Cheese(Pasteurized Part-Skim Cows&rsquo; Milk, Cheese Culture&rsquo;s, Salt, Enzymes), Fresh Garlic, Water, Rice Vinegar, Salt and Black Pepper), Olive Oil, Black Pepper</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g), Calories per serving: 290. Total Fat 16g &ndash; 28%,&nbsp; Sat. Fat 3.5g &ndash; 18%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 190mg - 8% .&nbsp; Vitamin D 0mcg - 0%, Calcium 100mg - 8%. Total Carb 6g - 2%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%, Protein 30g.Iron 1.2mg - 6% . Potassium 710mg - 15%.</p>\\r\\n\",\n                    \"_id\": \"5d512d70ef08b20e4944d8e1\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5ea2bcc0d074c675c76\",\n                \"title\": \"No Carb\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eefda2bcc0d074c675c97\",\n            \"title\": \"Protein Muffins\",\n            \"meal_images\": [\n                \"meal_images_1564405722714_Protein-Apple-Muffin.jpg\",\n                \"meal_images_1564405722715_protein-muffins-11.jpg\"\n            ],\n            \"content\": \"\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"\",\n                    \"desc\": \"\",\n                    \"_id\": \"5d3eefda2bcc0d074c675c98\"\n                }\n            ],\n            \"price\": \"30\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5e12bcc0d074c675c75\",\n                \"title\": \"Snacks\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eef8d2bcc0d074c675c95\",\n            \"title\": \"Power Balls \",\n            \"meal_images\": [\n                \"meal_images_1565691142324_IMG_1431_(1).jpg\"\n            ],\n            \"content\": \"<p>A mixture of flax seeds, bee pollen, oatmeal, honey, peanut butter&nbsp;and variation of chocolate chips or dried fruit.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Rolled Oats, Creamy Peanut Butter, Water, Kirkland Clover Honey, Flaxseed, Almonds, Chia Seeds, Bee Pollen, Cinnamon, Rodelle Organics Pure Vanilla Extract</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts:&nbsp; </strong>1 serving, serving size : 1 ball(57g),&nbsp; Calories per serving: 140. Total Fat 7g &ndash; 9%,&nbsp; Sat. Fat 1g &ndash; 5%, Trans Fat 0g, Cholest. 0Mg &ndash; 0%,&nbsp;Sodium 40mg - 2% .&nbsp; Vitamin D 0mcg - 0%, Calcium 20mg - 2%. Total Carb 17g - 6%, Fiber 3g - 11%, Total Sugars 5g, Incl. 4 g added Sugars - 8%, Protein 5g. Iron 1.1mg - 6% . Potassium 20mg - 0%.</p>\\r\\n\",\n                    \"_id\": \"5d528d063063170c27ee9411\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5e12bcc0d074c675c75\",\n                \"title\": \"Snacks\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eeef02bcc0d074c675c91\",\n            \"title\": \"Salmon, Brown Rice and Veggies\",\n            \"meal_images\": [\n                \"meal_images_1565710044087_IMG_1430.jpg\"\n            ],\n            \"content\": \"<p>A wild caught salmon with Cajun seasoning with a side of freshly made brown rice and a mixture of broccoli, carrot and cauliflower veggies.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Salmon, Brown Rice, Broccoli, Carrots, Cauliflower&nbsp;</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g),&nbsp; Calories per serving: 360. Total Fat 15g &ndash; 19%, Sat. Fat 3g &ndash; 15%, Trans Fat 0g, Cholest. 70Mg &ndash; 23%,&nbsp;Sodium 100mg - 4% .&nbsp; Vitamin D 0mcg - 0%, Calcium 50mg - 4%. Total Carb 26g - 9%, Fiber 4g - 14%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 28g. Iron 1.2mg - 6% . Potassium 690mg - 15%.</p>\\r\\n\",\n                    \"_id\": \"5d52d6dc3063170c27ee9414\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5db2bcc0d074c675c74\",\n                \"title\": \"Seafood\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eef492bcc0d074c675c93\",\n            \"title\": \"Pesto Cod white Rice and Veggies\",\n            \"meal_images\": [\n                \"meal_images_1564405577414_cod__hero.jpg\",\n                \"meal_images_1564405577415_recipe-image-legacy.jpg\"\n            ],\n            \"content\": \"<p>Wild caught cod seasoned in a pesto sauce with a side of freshly made white rice and a mixture of broccoli, carrots and cauliflower.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Fish, Cod, Pacific, Raw, White Rice, Broccoli, Cauliflower, Carrots, Olive Oil, Basil, Garlic, Asiago Cheese (Pasturized Milk and Cream, Cheese Cultures, Salt, Enzymes), Sugar, Pine Nuts, Contains, Less Than 1% Lactic Acid, Salt, Spice and Sodium Benzoate</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size (0.0g),&nbsp; Calories per serving: 330. Total Fat 13g &ndash; 17%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 55Mg &ndash; 18%,<strong>&nbsp;</strong>Sodium 450mg - 20% .&nbsp; Vitamin D 0.6mcg - 2%, Calcium 40mg - 4%. Total Carb 32g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. &lt;1 g added Sugars - 1%, Protein 22g. Iron 4.7mg - 25% . Potassium 480mg - 10%.</p>\\r\\n\",\n                    \"_id\": \"5d512bf9ef08b20e4944d8d5\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5db2bcc0d074c675c74\",\n                \"title\": \"Seafood\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eee572bcc0d074c675c8f\",\n            \"title\": \"Turkey Chili Corn Bread\",\n            \"meal_images\": [\n                \"meal_images_1564405334898_chili-with-cornbread1.jpg\",\n                \"meal_images_1564405334899_Turkey-Chili-Cornbread.jpg\"\n            ],\n            \"content\": \"<p>A mix of kidney, black and pinto beans with bell peppers onions and garlic. Topped with freshly baked protein cornbread.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Turkey, Black Beans, Kidney Beans, Pinto Beans, Crushed Tomatoes, Garlic, Onions, Bell Peppers, Cornbread</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 280. Total Fat 11g &ndash; 14%, Sat. Fat 0g &ndash; 10%, Trans Fat 0g, Cholest. 0Mg &ndash; 0%,&nbsp;Sodium 250mg - 11% . Vitamin D 0mcg - 0%, Calcium 0mg - 0%. Total Carb 24g - 9%, Fiber 0g - 0%, Total Sugars 0g, Incl. 0 g added Sugars - 0%, Protein 31g. Iron 0mg - 0% . Potassium 0mg - 0%.</p>\\r\\n\",\n                    \"_id\": \"5d512c8bef08b20e4944d8db\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5d42bcc0d074c675c73\",\n                \"title\": \"Turkey\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eed7d2bcc0d074c675c8b\",\n            \"title\": \"Steak Fajita \",\n            \"meal_images\": [\n                \"meal_images_1564405117009_beef_fajitas.jpg\",\n                \"meal_images_1564405117012_steak-fajitas-horizontal.jpg\"\n            ],\n            \"content\": \"<p>Marinated tri tip with a side of rice topped with grilled bell peppers and onions.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients:&nbsp; </strong>Tri - Tip, Fajita Peppers, White Rice</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 360. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 90Mg &ndash; 30%,&nbsp;Sodium 80mg - 3% . Vitamin D 0mcg - 0%, Calcium 40mg - 2%. Total Carb 32g - 12%, Fiber 2g - 7%, Total Sugars 3g, Incl. 0 g added Sugars - 0%,&nbsp; Protein 33g. Iron 3.8mg - 20% . Potassium 560mg - 10%.</p>\\r\\n\",\n                    \"_id\": \"5d512a2bef08b20e4944d8d0\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5b12bcc0d074c675c72\",\n                \"title\": \"Beef\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eea3b2bcc0d074c675c7d\",\n            \"title\": \"Chicken Green Salad with Miso Dressing\",\n            \"meal_images\": [\n                \"meal_images_1565767973948_IMG_1423.jpg\"\n            ],\n            \"content\": \"<p>A tossed spring mixed salad filled with tomato, carrots, broccoli and&nbsp;seasoned chicken breast. Topped with chopped almonds and a house made miso dressing.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Spring Mix,<strong> </strong>Chicken Breast, Carrots, Broccoli, Tomatoes, Roasted Al</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 350. Total Fat 18g &ndash; 23%, Sat. Fat 2g &ndash; 10%,&nbsp; Trans Fat 0g, Cholest. 90Mg &ndash; 30%, Sodium 190mg - 8% . Vitamin D 0mcg 0%, Calcium 200mg 15%.&nbsp; Total Carb 17g, Fiber 7g,&nbsp; Total Sugars 6g, Incl.&nbsp; 0 g added Sugars, Protein 35g. Iron 5.8mg - 30% .</p>\\r\\n\",\n                    \"_id\": \"5d53b9260325346e8aaf0e46\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5962bcc0d074c675c6f\",\n                \"title\": \"Cold Items\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eee082bcc0d074c675c8d\",\n            \"title\": \"Turkey Sweet Potatoes with veggies\",\n            \"meal_images\": [\n                \"meal_images_1564405256275_Ground-Beef-Zucchini.jpg\",\n                \"meal_images_1564405256276_ground-turkey-sweet-potato.jpg\"\n            ],\n            \"content\": \"<p>Ground turkey seasoned with spinach with a side of sweet potatoes with a sprinkle of cinnamon and a mixture of carrots, broccoli and cauliflower.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Ground Turkey, Quinoa, Sweet Potato</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g),&nbsp; Calories per serving: 420. Total Fat 15g &ndash; 19%, Sat. Fat 3.5g &ndash; 18%, Trans Fat 0g, Cholest. 120Mg &ndash; 40%,&nbsp;Sodium 140mg - 6% . Vitamin D 0.2mcg - 2%, Calcium 80mg - 6%. Total Carb 36g - 13%, Fiber 5g - 18%, Total Sugars 6g, Incl. 0 g added Sugars - 0%, Protein 36g. Iron 3.6mg - 20% . Potassium 900mg - 20%.</p>\\r\\n\",\n                    \"_id\": \"5d512b81ef08b20e4944d8d1\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5d42bcc0d074c675c73\",\n                \"title\": \"Turkey\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eeacc2bcc0d074c675c81\",\n            \"title\": \"Teriyaki Chicken, White Rice and Broccoli\",\n            \"meal_images\": [\n                \"meal_images_1564404428659_Slow_Cooker_Chicken_Teriyaki-2-600x400.jpg\",\n                \"meal_images_1564404428661_terichick-1.jpg\"\n            ],\n            \"content\": \"<p>Chicken thigh marinated in a teriyaki sauce with a side of white rice and broccoli.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients:&nbsp; </strong>Chicken Thigh, White Rice, Broccoli, Scallions, Pineapple, Mirin SweetCooking Honteri, Mirin, Liquid Amino Acids, Ginger, Liquid Amino Acids</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(0.0g), Calories per serving: 360. Total Fat 10g &ndash; 13%, Sat. Fat 2.5g &ndash; 13%, Trans Fat 0g, Cholest. 150Mg &ndash; 50%, Sodium 250mg - 11% . Vitamin D 0.2mcg 0%, Calcium 60mg 4%.&nbsp; Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 4g, Incl.&nbsp; 1 g added Sugars - 2%, Protein 33g.&nbsp;Iron 3.2mg - 20% . Potassium 610mg - 15%.</p>\\r\\n\",\n                    \"_id\": \"5d51296fef08b20e4944d8ca\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee59f2bcc0d074c675c70\",\n                \"title\": \"Chicken Meals\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eeb2d2bcc0d074c675c83\",\n            \"title\": \"Chicken Fajitas\",\n            \"meal_images\": [\n                \"meal_images_1565690765897_IMG_1424_(1).jpg\"\n            ],\n            \"content\": \"<p>Grilled chicken breast marinated in a achiote flavor with a side of rice topped with grilled bell peppers and onions.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Achiote Thigh<strong> (</strong>Chicken , Lime Juice, Olive Oil, Liquid Amino Acids, Water, Annatto Seed, Oregano), Bell Peppers, Onions, Rice</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>&nbsp;<strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 310. Total Fat 8g &ndash; 10%, Sat. Fat 1.5g &ndash; 8%, Trans Fat 0g,&nbsp; Cholest. 95Mg &ndash; 32%, Sodium 370mg - 16% . Vitamin D 0.1mcg 0%, Calcium 60mg 4%. Total Carb 34g - 12%, Fiber 3g - 11%, Total Sugars 3g, Incl. 0 g added Sugars - 0%, Protein 26g.Iron 3.6mg - 20% . Potassium 470mg - 10%.</p>\\r\\n\",\n                    \"_id\": \"5d528b8e3063170c27ee940b\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee59f2bcc0d074c675c70\",\n                \"title\": \"Chicken Meals\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3ee7ea2bcc0d074c675c79\",\n            \"title\": \"Eggs & Potatoes \",\n            \"meal_images\": [\n                \"meal_images_1565691045122_IMG_1429.jpg\"\n            ],\n            \"content\": \"<p>A mixture of slow cooked scrambled eggs with seasoned baked red potatoes.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Eggs, Red Potatoes</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(227g), Calories per serving: 270. Total Fat 13g &ndash; 17%, Sat. Fat 4g &ndash; 20%, Trans Fat 0.5g, Cholest. 315Mg &ndash; 105%, Sodium 180mg - 8% . Vitamin D 2.3mcg 10%, Calcium 90mg 6%.&nbsp; Total Carb 24g, Fiber 2g, Total Sugars 3g, Incl.&nbsp; 0 g added Sugars, Protein 14g. Iron 2.3mg - 15% .</p>\\r\\n\",\n                    \"_id\": \"5d528ca63063170c27ee9410\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5cfe44f5c1891efa1a9123d3\",\n                \"title\": \"Breakfast\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-10T17:39:03.380Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3ee8992bcc0d074c675c7b\",\n            \"title\": \"Egg Salad with Miso Dressing\",\n            \"meal_images\": [\n                \"meal_images_1564403865190_561847875_5141c0e111.jpg\",\n                \"meal_images_1564403865190_Japanese-Salad-Bowls-with-a-Miso-Tahini-Dressing-and-Brown-Rice-700x514.jpg\"\n            ],\n            \"content\": \"<p>A tossed spring mixed salad filled with tomato, carrots and broccoli with a sliced hard boiled egg. Topped with chopped almonds and a house made miso dressing.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"\",\n                    \"desc\": \"\",\n                    \"_id\": \"5d47e05121a88e3295bafaa2\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5962bcc0d074c675c6f\",\n                \"title\": \"Cold Items\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eea7f2bcc0d074c675c7f\",\n            \"title\": \"Chicken Wrap\",\n            \"meal_images\": [\n                \"meal_images_1565690829519_IMG_1425_(1).jpg\"\n            ],\n            \"content\": \"<p>A spinach wrapped burrito filled with spring mix, carrots, tomatoes and broccoli drizzled with a house made miso dressing.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Chicken, Organic Spring Green Mix, Tomatoes, Spinach Tortilla, Carrot, Miso Dressing, Seasonal Fruit</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 270. Total Fat 6g &ndash; 8%, Sat. Fat 1.5g &ndash; 8%, Trans Fat 0g, Cholest. 95Mg &ndash; 32%, Sodium 260mg - 11% . Vitamin D 0.1mcg 0%, Calcium 80mg 6%. Total Carb 14g - 5%, Fiber 3g - 11%,&nbsp; Total Sugars 3g, Incl.&nbsp; 0 g added Sugars, Protein 38g. Iron 3.2mg - 20% . Potassium 420mg - 8%.</p>\\r\\n\",\n                    \"_id\": \"5d528bdc3063170c27ee940d\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5962bcc0d074c675c6f\",\n                \"title\": \"Cold Items\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d5a90afde10d058d2f5cc00\",\n            \"title\": \"mojito\",\n            \"meal_images\": [\n                \"meal_images_1566216367088_mojito-cocktails.jpg\"\n            ],\n            \"content\": \"<p>Superb</p>\\r\\n\",\n            \"contains_text\": [\n                \"all good ingredents\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"its yum\",\n                    \"desc\": \"<p>Interesting taste</p>\\r\\n\",\n                    \"_id\": \"5d5a90afde10d058d2f5cc02\"\n                },\n                {\n                    \"heading\": \"\",\n                    \"desc\": \"\",\n                    \"_id\": \"5d5a90afde10d058d2f5cc01\"\n                }\n            ],\n            \"price\": \"12\",\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-08-19T09:06:41.616Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5ea2bcc0d074c675c76\",\n                \"title\": \"No Carb\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [\n                {\n                    \"_id\": \"5d00f5f0b89eb6111e86261a\",\n                    \"title\": \"Test Title123\",\n                    \"price\": \"5.50\",\n                    \"slug\": \"\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-12T12:33:20.607Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [\n                {\n                    \"_id\": \"5d15d612f03a11075ef76756\",\n                    \"title\": \"Test Title\",\n                    \"key_icon\": \"key_icon_1561712146459_dummy1.png\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-28T08:43:06.435Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"like_details\": [\n                {\n                    \"_id\": \"5d14e51e82fa99153846f904\",\n                    \"title\": \"test\",\n                    \"like_icon\": \"like_icon_1561650462430_autumn.jpg\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-27T15:40:08.858Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"dietary_details\": [\n                {\n                    \"_id\": \"5d14e6ad82fa99153846f906\",\n                    \"title\": \"asa asasas\",\n                    \"dietary_icon\": \"dietary_icon_1561650861631_new_dummy.jpeg\",\n                    \"status\": \"Active\",\n                    \"isDeleted\": false,\n                    \"createdAt\": \"2019-06-27T15:40:08.851Z\",\n                    \"__v\": 0\n                }\n            ],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eeca12bcc0d074c675c87\",\n            \"title\": \"Ground Beef, Pasta\",\n            \"meal_images\": [\n                \"meal_images_1564404897875_4524446.jpg\",\n                \"meal_images_1564404897877_ground-beef-pasta.jpg\"\n            ],\n            \"content\": \"<p>Whole wheat spaghetti noodles with a homemade fresh marina sauce topped with seasoned ground beef and spinach.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Ground Beef, Whole Wheat Pasta, Parsley, Onion, Crushed Tomatoes, Sundried Tomato Bits, Garlic</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(283g), Calories per serving: 420. Total Fat 12g &ndash; 15%, Sat. Fat 4g &ndash; 20%, Trans Fat 0g, Cholest. 75Mg &ndash; 25%,&nbsp;Sodium 135mg - 6% . Vitamin D 0mcg 0%, Calcium 110mg - 8%. Total Carb 46g - 17%, Fiber 6g - 21%, Total Sugars 15g,&nbsp; Incl. 0 g added Sugars - 0%, Protein 33g. Iron 7.2mg - 40% . Potassium 530mg - 10%.</p>\\r\\n\",\n                    \"_id\": \"5d512d1aef08b20e4944d8df\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5b12bcc0d074c675c72\",\n                \"title\": \"Beef\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3ee7642bcc0d074c675c77\",\n            \"title\": \"Soyrizo wrap\",\n            \"meal_images\": [\n                \"meal_images_1565690890412_IMG_1427.jpg\"\n            ],\n            \"content\": \"<p>Start your morning off with a Spinach tortilla wrapped burrito filled with soyrizo and eggs. Served with a fresh side of seasonal fruit.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients:</strong> Eggs, Soy Chorizo, Spinach Tortilla, Seasonal Fruit, Onion, Green Bell Pe</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts:</strong> 1 serving, serving size : 1 mass meal(227g), Calories per serving: 370. Total Fat 23g &ndash; 29%, Sat. Fat 6g &ndash; 30%, Trans Fat 1g, Cholest. 355Mg &ndash; 118%, Sodium 640mg - 28% . Vitamin D 2.3mcg 10%, Calcium 150mg 10%, Potassium 210mg 4%. Total Carb 19g, Fiber 3g, Total Sugars 4g, Incl. 0 g added Sugars, Protein 20g.Iron 2.8mg - 15% .</p>\\r\\n\",\n                    \"_id\": \"5d528c0b3063170c27ee940e\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5cfe44f5c1891efa1a9123d3\",\n                \"title\": \"Breakfast\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-10T17:39:03.380Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eec5f2bcc0d074c675c85\",\n            \"title\": \"Al Pastor, Rice and veggies\",\n            \"meal_images\": [\n                \"meal_images_1565710424764_IMG_1436.jpg\"\n            ],\n            \"content\": \"<p>Pork marinated with pineapples and lime juice with a side of rice&nbsp;and a mix of broccoli, carrots and cauliflower.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Al Pastor, White Rice,<strong> </strong>Broccoli, Cauliflower, Carrots</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p><strong>Nutrition Facts: </strong>1 serving, serving size(10g), Calories per serving: 250. Total Fat 5g &ndash; 6%, Sat. Fat 1g &ndash; 5%, Trans Fat 0g,&nbsp; Cholest. 40Mg &ndash; 13%, Sodium 220mg - 10% . Vitamin D 0.1mcg 0%, Calcium 35mg 2%. Total Carb 31g - 11%, Fiber 3g - 11%, Total Sugars 3g,&nbsp; Incl. 0 g added Sugars - 0%, Protein 19g. Iron 2.3mg - 15% . Potassium 460mg - 10%.</p>\\r\\n\",\n                    \"_id\": \"5d52d8593063170c27ee9415\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5aa2bcc0d074c675c71\",\n                \"title\": \"Pork\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": []\n        },\n        {\n            \"_id\": \"5d3eed362bcc0d074c675c89\",\n            \"title\": \"Tri Tip, Brown Rice and Squash\",\n            \"meal_images\": [\n                \"meal_images_1565691302808_IMG_1435.jpg\"\n            ],\n            \"content\": \"<p>Slow cooked marinated tri tip with a side of brown and seasoned squash.</p>\\r\\n\",\n            \"contains_text\": [\n                \"\"\n            ],\n            \"detail_blocks\": [\n                {\n                    \"heading\": \"Ingredients & Nutrition Facts\",\n                    \"desc\": \"<p><strong>Ingredients: </strong>Tri - Tip, Zucchini, Brown Rice<br />\\r\\n<br />\\r\\n<strong>Nutrition Facts: </strong>1 serving, serving size : 1 mass meal(277g), Calories per serving: 410. Total Fat 13g &ndash; 17%, Sat. Fat 4.5g &ndash; 23%, Trans Fat 0g, Cholest. 130Mg &ndash; 40%,&nbsp;Sodium 105mg - 5% . Vitamin D 0mcg 0%, Calcium 60mg - 4%. Total Carb 23g - 8%, Fiber 2g - 7%, Total Sugars 2g, Incl. 0 g added Sugars - 0%, Protein 49g. Iron 3.6mg - 20% . Potassium 900mg - 20%.</p>\\r\\n\",\n                    \"_id\": \"5d528da93063170c27ee9412\"\n                }\n            ],\n            \"price\": 30,\n            \"slug\": \"\",\n            \"status\": \"Active\",\n            \"isDeleted\": false,\n            \"createdAt\": \"2019-07-29T11:23:22.610Z\",\n            \"mealtype_details\": {\n                \"_id\": \"5d3ee5b12bcc0d074c675c72\",\n                \"title\": \"Beef\",\n                \"slug\": \"\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-07-29T11:23:22.628Z\",\n                \"__v\": 0\n            },\n            \"topping_details\": [],\n            \"category_details\": {\n                \"_id\": \"5d039fdac1891efa1ae98e41\",\n                \"title\": \"Sample\",\n                \"slug\": \"sample\",\n                \"status\": \"Active\",\n                \"isDeleted\": false,\n                \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n                \"__v\": 0\n            },\n            \"key_details\": [],\n            \"like_details\": [],\n            \"dietary_details\": [],\n            \"review_details\": [\n                {\n                    \"_id\": \"5d5bcfb349afef01433af84d\",\n                    \"user_id\": \"5d4ab2d748a8fe34c9b98ef7\",\n                    \"meal_id\": \"5d3eed362bcc0d074c675c89\",\n                    \"title\": \"Delicious\",\n                    \"comment\": \"Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. \",\n                    \"rating\": 5,\n                    \"createdAt\": \"2019-08-19T08:13:49.497Z\",\n                    \"isDeleted\": false,\n                    \"__v\": 0\n                },\n                {\n                    \"_id\": \"5d5a5bc349156e065ea391e1\",\n                    \"user_id\": \"5d4ab2d748a8fe34c9b98ef7\",\n                    \"meal_id\": \"5d3eed362bcc0d074c675c89\",\n                    \"title\": \"Delicious\",\n                    \"comment\": \"Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. \",\n                    \"rating\": 4,\n                    \"createdAt\": \"2019-08-19T08:13:49.497Z\",\n                    \"isDeleted\": false,\n                    \"__v\": 0\n                }\n            ]\n        }\n    ],\n    \"message\": \"Product fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 201,\n    \"data\": {},\n    \"message\": \"Sorry, No record found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/product.routes.js",
    "groupTitle": "Product",
    "name": "PostProductList"
  },
  {
    "type": "post",
    "url": "/products/review",
    "title": "Post Product Review",
    "version": "1.0.0",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "meal_id",
            "description": "<p>Meal Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Review Short Title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Review Content</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating Number[ex: 1/2/3/4/5]</p>"
          }
        ]
      }
    },
    "group": "Product",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"user_id\": \"5d4ab2d748a8fe34c9b98ef7\",\n        \"meal_id\": \"5d3eed362bcc0d074c675c89\",\n        \"title\": \"Delicious\",\n        \"comment\": \"Proin sed pretium risus. Sed egestas tellus orci, vel hendrerit libero pulvinar non. Donec blandit ex massa, a tristique turpis placerat vel. \",\n        \"rating\": 4,\n        \"createdAt\": \"2019-08-19T08:13:49.497Z\",\n        \"isDeleted\": false,\n        \"_id\": \"5d5a5bc349156e065ea391e1\",\n        \"__v\": 0\n    },\n    \"message\": \"Thank you, We have received your review on this Meal.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/product.routes.js",
    "groupTitle": "Product",
    "name": "PostProductsReview"
  },
  {
    "type": "get",
    "url": "/user/logout/:id",
    "title": "Logout",
    "version": "1.0.0",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {},\n    \"message\": \"You have successfully logout.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "GetUserLogoutId"
  },
  {
    "type": "get",
    "url": "/user/profile/remove",
    "title": "Remove Profile",
    "version": "1.0.0",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {},\n    \"message\": \"Your profile has been removed successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "GetUserProfileRemove"
  },
  {
    "type": "post",
    "url": "/user/changePassword",
    "title": "User Change Password",
    "version": "1.0.0",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "old_password",
            "description": "<p>Old password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "new_password",
            "description": "<p>New password</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"test\",\n        \"last_name\": \"test\",\n        \"email\": \"test046@test.com\",\n        \"phone\": \"1230123013\",\n        \"password\": \"$2a$08$WAH3M8RxUzufNZnCQxgoJ.7mAaPilKkRRNpteBsdUeiyWzbZMjMxe\",\n        \"isNewUser\": false,\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5d5419e727fd1004ab79a35b\",\n        \"role\": \"5cfe19dec1891efa1a8e2193\",\n        \"createdAt\": \"2019-08-14T14:25:43.906Z\",\n        \"updatedAt\": \"2019-08-20T11:15:58.434Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Password Changed Successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserChangepassword"
  },
  {
    "type": "post",
    "url": "/user/checkzipcode",
    "title": "Check Zip Code",
    "version": "1.0.0",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "zip",
            "description": "<p>Zipcode</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"title\": 78945,\n        \"status\": \"Active\",\n        \"isDeleted\": false,\n        \"createdAt\": \"2019-06-14T09:31:26.317Z\",\n        \"_id\": \"5d037fcac1891efa1ae74604\",\n        \"__v\": 0\n    },\n    \"message\": \"Zipcode fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserCheckzipcode"
  },
  {
    "type": "post",
    "url": "/user/login",
    "title": "Login",
    "version": "1.0.0",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZmZhNTgwZDI0Mjc4MTFkN2VmOGUyZiIsImlhdCI6MTU2MDc2NzAzOCwiZXhwIjoxNTYwODUzNDM4fQ.zJlAXvAj0DeK5ktYQ9T6JjyY6eFZbvraIRb8e_kCpvI\",\n    \"data\": {\n        \"first_name\": \"Sougatadsdsds\",\n        \"last_name\": \"Royttyry\",\n        \"email\": \"vendor10@mahk.eu\",\n        \"phone\": \"9932155101\",\n        \"password\": \"$2a$08$.ungCsiSf9xBIAIydlHqxON4h.5twDUQyasua9/4Sie6f6wSdzz1W\",\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5cffa580d2427811d7ef8e2f\",\n        \"role\": {\n            \"desc\": \"User of the application.\",\n            \"_id\": \"5cfe19dec1891efa1a8e2193\",\n            \"roleDisplayName\": \"User\",\n            \"role\": \"user\",\n            \"id\": \"5cfe19dec1891efa1a8e2193\"\n        },\n        \"__v\": 0\n    },\n    \"message\": \"You have successfully logged in\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 200,\n    \"data\":{},\n    \"message\": \"Authentication failed, Wrong Password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserLogin"
  },
  {
    "type": "post",
    "url": "/user/profileUpdate",
    "title": "Update Profile",
    "version": "1.0.0",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"first_name\": \"Ram1\",\n        \"last_name\": \"Singh1\",\n        \"email\": \"test046@test.com\",\n        \"phone\": \"7890321456\",\n        \"password\": \"$2a$08$g94fMRqms0pygwbI1.rhQ.U4Lv5RpSkBBHjSXKIXWfNHiEMQtgKhq\",\n        \"isNewUser\": false,\n        \"isVerified\": \"Yes\",\n        \"verifyToken\": null,\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5d5419e727fd1004ab79a35b\",\n        \"role\": \"5cfe19dec1891efa1a8e2193\",\n        \"createdAt\": \"2019-08-14T14:25:43.906Z\",\n        \"updatedAt\": \"2019-08-20T16:04:56.941Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Profile updated successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserProfileupdate"
  },
  {
    "type": "post",
    "url": "/user/reset-password",
    "title": "Forgot Password",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          }
        ]
      }
    },
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {},\n    \"message\": \"A email with new password has been sent to your email address.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "{\n    \"status\": 200,\n    \"data\": {},\n    \"message\": \"No user found.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/api/user.routes.js",
    "groupTitle": "User",
    "name": "PostUserResetPassword"
  }
] });
